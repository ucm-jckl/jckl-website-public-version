# Set custom time formats
Time::DATE_FORMATS[:long_12hr] = '%B %d, %Y %l:%M %p'

# Set Chronic to use local time zone as default
Chronic.time_class = Time.zone

# Set Chronic to use 00:00 (midnight) as the default time for a date with no time component rather than 12 PM
Chronic::Parser::DEFAULT_OPTIONS[:guess] = :begin
