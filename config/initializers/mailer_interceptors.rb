# Send all emails on dev server to welker@ucmo.edu and
# append a list of original recipients to footer of email body
class DevMailInterceptor
  def self.delivering_email(message)
    original_recipients = message.to
    message.to = ['xxxxx@ucmo.edu']
    message.body = String(message.body) + "<br>----Original Recipients----<br>#{original_recipients.join('<br>')}".html_safe
  end
end

if Rails.env.development?
  ActionMailer::Base.register_interceptor(DevMailInterceptor)
end
