require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module JcklWeb
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Autoload the lib directory
    config.autoload_paths += %W(#{config.root}/lib)
    config.action_dispatch.default_headers = {
      'X-Frame-Options' => 'ALLOWALL'
    }

    # Set custom time zone
    config.time_zone = 'America/Chicago'
    config.active_record.default_timezone = :utc
    config.beginning_of_week = :sunday

    # Enable React addons
    config.react.addons = true

    # Set SMTP for emails
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.default_options = { from: 'library@ucmo.edu', reply_to: 'jckl-technology-group@ucmo.edu' }
    config.action_mailer.smtp_settings = {
      address: 'smtp.gmail.com',
      port: 587,
      user_name: 'library@ucmo.edu',
      password: Rails.application.secrets.library_gmail_password,
      authentication: 'plain',
      enable_starttls_auto: true
    }

    # Use custom routes for error handling
    config.exceptions_app = routes

    # Enable CORS to allow cross-domain access to content on this site
    config.middleware.use Rack::Cors do
      allow do
        origins /https?:\/\/.*\.ucmo\.edu/,
                /https?:\/\/ucmo\.libapps\.com/,
                /https?:\/\/ucmo\.libcal\.com/,
                /https?:\/\/ucmo\.libsurveys\.com/,
                /https?:\/\/ucmo\.beta\.libguides\.com/,
                /https?:\/\/wd8cd4tk5m\.search\.serialssolutions\.com/,
                /https?:\/\/ae\.preview\.serialssolutions\.com/
        resource '*', headers: :any, methods: [:get, :post, :options]
      end
    end
  end
end
