Rails.application.routes.draw do
  # Home page and sitemap routes
  root to: 'index#index'
  get 'index', to: 'index#index'
  get 'sitemap', to: 'index#sitemap'

  # Hours routes
  resources :library_hours, only: [:index], path: 'hours'

  # News routes
  resources :news_items, path: 'news', only: [:index, :show], param: :friendly_url

  # Search routes
  resources :search, only: [:index]

  # Employee routes
  resources :employees, param: :name do
    collection do
      get 'subject-specialists', to: 'employees#subject_specialists'
      get 'portal'
      get 'student-workers', to: 'employees#student_workers'
    end

    resources :absences do
      patch 'approve', on: :member
      patch 'reject', on: :member
      patch 'receive_note', on: :member
    end
  end
  get 'absences', to: 'absences#by_date', as: 'absences_by_date'
  get 'absences/:date', to: 'absences#by_date'

  resources :absences, only: [:new, :create, :index]
  # get 'employees/absence', :to => redirect( "absences/new" ) #redirect old absence form route

  # Contact routes
  resources :website_contacts, path: 'contact', only: [:create] do
    collection do
      get 'general'
      get 'employee/:name', action: 'employee', as: 'employee'
      get 'office/:name', action: 'office', as: 'office'
      get 'info'
    end
  end
  get 'contact', to: 'website_contacts#general'

  # Map routes
  get 'map', to: 'map#index', as: 'map'
  get 'map/index', to: 'map#index'

  # Computer routes
  get 'computers', to: 'computers#index', as: 'computers'
  get 'computers/index', to: 'computers#index'
  post 'computers/ping', to: 'computers#ping', as: 'computers_ping'
  get 'computers/availability', to: 'computers#availability', as: 'computers_availability'

  # Room booking routes
  resources :meeting_rooms, path: 'room-bookings', param: :room_name, only: [:index] do
    get 'available-rooms', to: 'available_rooms', on: :collection
    get 'book', to: 'meeting_rooms#new', as: 'new_booking_for', on: :member
    post 'create', as: 'create_booking_for', on: :member
  end
  get 'roomschedules', to: 'meeting_room_bookings#index' # old url redirect

  # New books routes
  resources :new_books, path: 'new-books', only: [:index] do
    get 'mappings', on: :collection
  end
  get 'newbooks', to: 'new_books#index'

  # Purchase request routes
  resources :new_book_requests, path: 'purchase-request', only: [:new, :create]
  get 'purchase-request', to: 'new_book_requests#new'

  # DVD routes
  get 'dvds', to: 'dvds#index', as: 'dvds'
  get 'dvds/index', to: 'dvds#index'

  # Stats routes
  get 'stats', to: 'stats#dashboard'
  get 'stats/dashboard', to: 'stats#dashboard', as: 'stats_dashboard'
  get 'stats/headcount_form', to: 'stats#headcount_form', as: 'stats_headcount_form'
  post 'stats/headcount_form', to: 'stats#headcount_form'
  get 'stats/headcounts', to: 'stats#headcounts', as: 'stats_headcounts'
  get 'stats/studyrooms', to: 'stats#studyrooms', as: 'stats_studyrooms'
  get 'stats/computers', to: 'stats#computers', as: 'stats_computers'

  # Inventory routes
  resources :barcode_files, path: 'inventory', only: [:new, :create, :index], path_names: { new: 'upload' }

  # File upload routes
  resources :files

  # Horses in Culture routes
  get 'horses_in_culture/data', to: 'horses_in_culture#data', as: 'horses_in_culture_data'

  # Error handling
  match '/401', to: 'errors#unauthorized', via: :all, as: 'error_unauthorized'
  match '/404', to: 'errors#not_found', via: :all, as: 'error_not_found'
  match '/422', to: 'errors#unprocessable', via: :all, as: 'error_unprocessable'
  match '/500', to: 'errors#internal_server_error', via: :all, as: 'error_internal_server_error'

  # Devise and ActiveAdmin authentication
  devise_config = ActiveAdmin::Devise.config
  devise_config[:controllers][:omniauth_callbacks] = 'omniauth_callbacks'
  devise_config[:controllers][:registrations] = 'devise/registrations'
  devise_config[:controllers][:sessions] = 'sessions'
  devise_for :users, devise_config
  ActiveAdmin.routes(self)
  get 'omniauth_callbacks/all'

  # Jasmine specs page
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
end
