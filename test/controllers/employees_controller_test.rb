require 'test_helper'
require 'devise'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  test 'get index' do
    get employees_path
    assert_response :success
  end

  test 'get subject_specialists' do
    get subject_specialists_employees_path
    assert_response :success
  end

  test 'get portal' do
    sign_in User.find(2)
    get portal_employees_path
    assert_response :success
  end

  test 'get edit' do
    sign_in User.find(2)
    get edit_employee_path(name: User.find(2).employee.url_slug)
    assert_response :success
  end

  test 'update employee info if logged in as employee' do
    sign_in User.find(2)
    patch employee_path(name: User.find(2).employee.url_slug), params: { employee: { first_name: 'TestFirstName', last_name: 'TestLastName' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'portal'
  end

  test 'get student_workers' do
    get student_workers_employees_path
    assert_response :success
  end

  test 'wrong user cannot do anything' do
    # Signed out user gets redirected to login page
    get portal_employees_path
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    get edit_employee_path(name: User.find(2).employee.url_slug)
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    patch employee_path(name: User.find(2).employee.url_slug), params: { employee: { first_name: 'TestFirstName', last_name: 'TestLastName' } }
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    # Signed in user gets unauthorized page
    sign_in User.find(3)

    get portal_employees_path
    assert_response :success

    get edit_employee_path(name: User.find(2).employee.url_slug)
    assert_response :redirect
    follow_redirect!
    assert_response 401

    patch employee_path(name: User.find(2).employee.url_slug), params: { employee: { first_name: 'TestFirstName', last_name: 'TestLastName' } }
    assert_response :redirect
    follow_redirect!
    assert_response 401
  end
end
