require 'test_helper'

class NewBookRequestsControllerTest < ActionDispatch::IntegrationTest
  TEST_BOOK_REQUEST = {
    requestor_name: 'Test McTesterson',
    requestor_email: 'fake-email@fake.com',
    format: 'Book',
    title: 'My Awesome Book',
    copies_ordered: 2,
    subject_area_id: 1
  }.freeze

  test 'get new' do
    # unauthenticated user gets redirected to log in
    get new_new_book_request_path
    assert_response :redirect
    follow_redirect!
    assert @controller.login_page?

    # authenticated user can view the page
    sign_in(User.find(2))
    get new_new_book_request_path
    assert_response :success
  end

  test 'post create' do
    # unauthenticated user gets redirected to log in
    post new_book_requests_path, params: { new_book_request: TEST_BOOK_REQUEST }
    assert_response :redirect
    follow_redirect!
    assert @controller.login_page?

    # authenticated user can post a request
    sign_in(User.find(2))
    post new_book_requests_path, params: { new_book_request: TEST_BOOK_REQUEST }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not @controller.login_page?
  end
end
