require 'test_helper'

class NewsItemsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get news_items_path
    assert_response :success
  end

  test 'should get item' do
    news_item = NewsItem.current_news.first
    get news_items_path(friendly_url: news_item.friendly_url)
    assert_response :success
  end
end
