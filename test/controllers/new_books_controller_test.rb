require 'test_helper'

class NewBooksControllerTest < ActionDispatch::IntegrationTest
  test 'get index' do
    travel_to(Chronic.parse('2014-10-01 8:00 AM'))

    # Test with no filters
    get new_books_path
    assert_response :success

    # Test with date filter
    get new_books_path(timeframe: '2014-10')
    assert_response :success

    # Test with subject filters
    get new_books_path(subject: 'history')
    assert_response :success

    # Test with call number filters
    get new_books_path(range: 124)
    assert_response :success

    # Test pagination
    get new_books_path(page: 5)
    assert_response :success

    travel_back
  end

  test 'get mappings' do
    get mappings_new_books_path
    assert_response :success
  end
end
