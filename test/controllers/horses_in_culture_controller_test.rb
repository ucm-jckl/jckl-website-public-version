require 'test_helper'

class HorsesInCultureControllerTest < ActionDispatch::IntegrationTest
  test 'should get data' do
    get horses_in_culture_data_path, as: :json
    assert_response :success
  end
end
