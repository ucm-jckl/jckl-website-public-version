require 'test_helper'

TEST_BOOKING_FORM = {
  requestor_name: 'Fakey McFakerson',
  requestor_email: 'FakeEmail@FakeEmail.com',
  contact_name: 'Fakey McFakerson',
  contact_email: 'FakeEmail@FakeEmail.com',
  contact_phone: '1234567',
  contact_address: 'my house',
  contact_dept: 'library',
  meeting_room_id: MeetingRoom.first.id,
  event_name: 'my event',
  start_time: '2016-09-01 08:00 AM',
  end_time: '2016-09-01 09:00 AM',
  setup_time: nil,
  takedown_time: nil,
  attendance: 10,
  recurrence: nil,
  notes: 'this is an important meeting'
}.freeze

class MeetingRoomsControllerTest < ActionDispatch::IntegrationTest
  test 'get index' do
    get meeting_rooms_path
    assert_response :success
  end

  test 'get new' do
    room = MeetingRoom.first
    get new_booking_for_meeting_room_path(room_name: room.room_name)
    assert_response :success
  end

  test 'post create booking' do
    room = MeetingRoom.first
    post create_booking_for_meeting_room_path(room_name: room.room_name), params: { meeting_room_booking: TEST_BOOKING_FORM }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end
end
