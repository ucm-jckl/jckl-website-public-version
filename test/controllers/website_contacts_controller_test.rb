require 'test_helper'

TEST_CONTACT_FORM = {
  contact_name: 'Fakey McFakerson',
  contact_email: 'FakeEmail@FakeEmail.com',
  topic: 'comments',
  subject: 'My Fake Subject',
  message: 'This is the fake message.'
}.freeze

class WebsiteContactsControllerTest < ActionDispatch::IntegrationTest
  test 'get general' do
    get general_website_contacts_path
    assert_response :success
  end

  test 'get employee' do
    get employee_website_contacts_path(name: Employee.first.url_slug)
    assert_response :success
  end

  test 'get office' do
    get office_website_contacts_path(name: Office.first.office_name)
    assert_response :success
  end

  test 'send email' do
    post website_contacts_path, params: { website_contact: TEST_CONTACT_FORM }
    assert_response :redirect
    assert_equal flash[:notice], WebsiteContactsController::CONFIRMATION_MESSAGE
  end

  test 'get contact info' do
    get info_website_contacts_path
    assert_response :success
  end
end
