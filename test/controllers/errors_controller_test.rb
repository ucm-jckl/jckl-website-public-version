require 'test_helper'

class ErrorsControllerTest < ActionDispatch::IntegrationTest
  test 'get not_found' do
    get error_not_found_path
    assert_response :success
  end

  test 'get unprocessable' do
    get error_unprocessable_path
    assert_response :success
  end

  test 'get internal_server_error' do
    get error_internal_server_error_path
    assert_response :success
  end
end
