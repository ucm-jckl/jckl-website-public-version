require 'test_helper'

class LibraryHoursControllerTest < ActionDispatch::IntegrationTest
  setup do
    travel_to(Chronic.parse('2016-09-15 8:00 AM'))
  end

  teardown do
    travel_back
  end

  test 'get index' do
    get library_hours_path
    assert_response :success
  end
end
