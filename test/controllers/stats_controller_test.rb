require 'test_helper'

class StatsControllerTest < ActionDispatch::IntegrationTest
  test 'get dashboard' do
    get stats_dashboard_url
    assert_response :success
  end

  test 'get headcount_form' do
    get stats_headcount_form_url
    assert_response :success
  end

  test 'get headcounts' do
    get stats_headcounts_url
    assert_response :success
  end

  test 'should get studyrooms' do
    get stats_studyrooms_url
    assert_response :success
  end

  test 'should get computers' do
    get stats_computers_url
    assert_response :success
  end
end
