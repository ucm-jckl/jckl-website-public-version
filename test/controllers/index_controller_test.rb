require 'test_helper'
require 'chronic'

class IndexControllerTest < ActionDispatch::IntegrationTest
  test 'get index' do
    get root_path
    assert_response :success
  end

  test 'get sitemap' do
    get root_path
    assert_response :success
  end
end
