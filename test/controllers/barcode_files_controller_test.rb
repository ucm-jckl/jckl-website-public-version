require 'test_helper'

class BarcodeFilesControllerTest < ActionDispatch::IntegrationTest
  TEST_BARCODE_FILE = {
    notes: 'testing 123',
    barcodes: '123,456,789'
  }.freeze

  setup do
    @user = User.find(2)
    sign_in @user
  end

  test 'get new' do
    get new_barcode_file_path
    assert_response :success
  end

  test 'create' do
    post barcode_files_path(params: { barcode_file: TEST_BARCODE_FILE })
    assert_response :success
  end

  test 'index' do
    get barcode_files_path
    assert_response :success
  end

  test 'unauthenticated user' do
    sign_out(@user)

    get new_barcode_file_path
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    post barcode_files_path(params: { barcode_file: TEST_BARCODE_FILE })
    assert :redirect
    follow_redirect!
    assert @controller.login_page?
  end
end
