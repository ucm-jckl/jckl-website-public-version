require 'test_helper'

class ComputersControllerTest < ActionDispatch::IntegrationTest
  setup do
    travel_to(Chronic.parse('2016-09-01 5:01 PM'))
  end

  teardown do
    travel_back
  end

  test 'index' do
    get computers_path
    assert_response :success
    assert_select '.computer', count: Computer.where(active: true).count
  end

  test 'ping' do
    travel_back

    require 'resolv'

    # find no computer for this host (test server)
    post computers_ping_path
    assert_response 404

    # find a valid computer and create a new session
    computer = Computer.where(active: true).first
    assert computer.current_session.new_record?
    post computers_ping_path, params: { ip: computer.ip_address }
    assert_response :success
    assert_not computer.current_session.new_record?
    assert_equal 1, computer.current_session.ping_count

    # update the session again after 2 seconds
    sleep 2
    post computers_ping_path, params: { ip: computer.ip_address }
    assert_response :success
    assert_equal 2, computer.current_session.ping_count
    assert_not_equal computer.current_session.created_at, computer.current_session.updated_at
  end

  test 'availability' do
    get computers_availability_path, as: :json, xhr: true
    assert_response :success
    computers = JSON.parse(@response.body)

    availability_per_floor = {
      'all' => 0,
      '1' => 0,
      '2' => 0,
      '3' => 0
    }

    computers.each do |computer|
      if computer['available?']
        availability_per_floor['all'] += 1
        availability_per_floor[computer['floor'].to_s] += 1
      end
    end

    assert_equal 4, availability_per_floor['all']
    assert_equal 0,  availability_per_floor['1']
    assert_equal 2,  availability_per_floor['2']
    assert_equal 2,  availability_per_floor['3']
  end
end
