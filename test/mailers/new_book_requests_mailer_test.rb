require 'test_helper'

class NewBookRequestsMailerTest < ActionMailer::TestCase
  TEST_BOOK_REQUEST = {
    requestor_name: 'Test McTesterson',
    requestor_email: 'fake-email@fake.com',
    format: 'Book',
    title: 'My Awesome Book',
    copies_ordered: 2,
    subject_area_id: 1
  }.freeze

  test 'send book request email' do
    user = User.find(2)
    request = NewBookRequest.new(TEST_BOOK_REQUEST)
    request.save
    mail = NewBookRequestsMailer.send_email(user, request)
    assert_equal mail.subject, "Online Purchase Request: #{request.title}"
    assert_equal mail.to.sort, ([user.email, request.subject_area.andand.subject_librarian.andand.email] + ApplicationMailer::BOOK_REQUESTS_EMAIL).sort

    assert_emails 1 do
      mail.deliver_now
    end
  end
end
