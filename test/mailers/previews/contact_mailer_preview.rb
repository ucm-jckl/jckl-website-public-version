# Preview all emails at http://localhost:3000/rails/mailers/contact_mailer
class ContactMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contact_mailer/general
  def general
    ContactMailer.general
  end

  # Preview this email at http://localhost:3000/rails/mailers/contact_mailer/employee
  def employee
    ContactMailer.employee
  end

  # Preview this email at http://localhost:3000/rails/mailers/contact_mailer/office
  def office
    ContactMailer.office
  end

end
