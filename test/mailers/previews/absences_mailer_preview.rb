# Preview all emails at http://localhost:3000/rails/mailers/absences_mailer
class AbsencesMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/absences_mailer/send_email
  def send_email
    AbsencesMailer.send_email
  end

end
