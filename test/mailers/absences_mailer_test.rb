require 'test_helper'

class AbsencesMailerTest < ActionMailer::TestCase
  TEST_ABSENCE_REQUEST = {
    employee_id: 38,
    submitter_id: 38,
    start_date: '2017-03-01',
    end_date: '2017-03-02',
    absence_type_id: 1,
    duration_minutes: 960,
    schedule: 'Out all day'
  }.freeze

  test 'send absence email' do
    absence = Absence.new(TEST_ABSENCE_REQUEST)
    absence.save
    verb = 'Submitted'
    mail = AbsencesMailer.send_email(absence, verb)
    assert_equal mail.subject, "Absence Request #{verb}: #{absence.employee.first_last} - #{absence.date_range}"
    assert_equal mail.to.sort, ([absence.employee.email, absence.submitter.email, absence.employee.supervisor.email] + ApplicationMailer::ADMIN_OFFICE_EMAIL).sort

    assert_emails 1 do
      mail.deliver_now
    end
  end
end
