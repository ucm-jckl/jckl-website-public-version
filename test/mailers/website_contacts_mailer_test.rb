require 'test_helper'

class WebsiteContactsMailerTest < ActionMailer::TestCase
  TEST_CONTACT_FORM = {
    contact_name: 'Fakey McFakerson',
    contact_email: 'FakeEmail@FakeEmail.com',
    subject: 'My Fake Subject',
    message: 'This is the fake message.'
  }.freeze

  test 'general' do
    contact_form = WebsiteContact.new(TEST_CONTACT_FORM)
    contact_form.topic = 'comments'
    employee = nil
    office = nil

    mail = WebsiteContactsMailer.send_email(contact_form, employee, office)
    assert_equal mail.subject, "Online Contact Form Received: #{contact_form.subject}"
    assert_equal mail.to.sort, ([contact_form.contact_email] + ApplicationMailer::ADMIN_OFFICE_EMAIL).sort

    assert_emails 1 do
      mail.deliver_now
    end
  end

  test 'employee' do
    contact_form = WebsiteContact.new(TEST_CONTACT_FORM)
    employee = Employee.first
    office = nil

    mail = WebsiteContactsMailer.send_email(contact_form, employee, office)
    assert_equal mail.subject, "Online Contact Form Received: #{contact_form.subject}"
    assert_equal mail.to, [contact_form.contact_email, employee.email]

    assert_emails 1 do
      mail.deliver_now
    end
  end

  test 'office' do
    contact_form = WebsiteContact.new(TEST_CONTACT_FORM)
    employee = nil
    office = Office.first

    mail = WebsiteContactsMailer.send_email(contact_form, employee, office)
    assert_equal mail.subject, "Online Contact Form Received: #{contact_form.subject}"
    assert_equal mail.to, [contact_form.contact_email, office.email, office.primary_employee.andand.email]

    assert_emails 1 do
      mail.deliver_now
    end
  end
end
