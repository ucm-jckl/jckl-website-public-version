require 'test_helper'

class MeetingRoomsMailerTest < ActionMailer::TestCase
  TEST_BOOKING_FORM = {
    requestor_name: 'Fakey McFakerson',
    requestor_email: 'FakeEmail@FakeEmail.com',
    contact_name: 'Fakey McFakerson',
    contact_email: 'FakeEmail@FakeEmail.com',
    contact_phone: '1234567',
    contact_address: 'my house',
    contact_dept: 'library',
    meeting_room_id: MeetingRoom.first.id,
    event_name: 'my event',
    start_time: '2016-09-01 08:00 AM',
    end_time: '2016-09-01 09:00 AM',
    setup_time: nil,
    takedown_time: nil,
    attendance: 10,
    recurrence: nil,
    notes: 'this is an important meeting'
  }.freeze

  test 'send booking email' do
    booking = MeetingRoomBooking.new(TEST_BOOKING_FORM)

    mail = MeetingRoomsMailer.send_email(booking)
    assert_equal mail.subject, "Room Booking Request - #{booking.event_name} - #{booking.start_time.to_formatted_s(:long_12hr)} to #{booking.end_time.to_formatted_s(:long_12hr)}".gsub('  ', ' ')
    assert_equal mail.to.sort, ([booking.requestor_email, booking.contact_email] + ApplicationMailer::ROOM_BOOKINGS_EMAIL).sort

    assert_emails 1 do
      mail.deliver_now
    end
  end
end
