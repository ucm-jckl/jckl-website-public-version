require 'test_helper'

class MapTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 5
  end

  test 'load the map' do
    visit(map_path)
    assert page.assert_selector('#viewport .floor', count: 3, visible: :all)
    assert page.assert_selector('#viewport .floor.active', count: 1)
  end

  test 'change floors' do
    visit(map_path)
    click_link('2')
    assert page.assert_selector('#viewport #floor-2.active', count: 1)
  end

  test 'find a room' do
    visit(map_path)
    fill_in(class: 'room-search-number', with: 'room 3252')
    click_button('Search')
    assert page.assert_selector('#viewport #floor-3.active', count: 1)
    assert page.assert_selector('#viewport #room-3252.animating', count: 1)
  end

  test 'show computer availability' do
    visit(map_path)
    assert page.assert_selector('#viewport')
    assert page.assert_selector('.computer.available')
    page.find('a[href="#computers"]').click
    assert page.assert_text('2 of 2', count: 3)
  end
end
