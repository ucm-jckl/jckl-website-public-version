require 'test_helper'

class NewBooksTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 5
  end

  test 'index' do
    travel_to(Chronic.parse('2014-10-01 8:00 AM'))

    visit(new_books_path)
    assert page.assert_text('Showing 1 - 25 of 536 new items.')
    assert page.assert_selector('.new-book', count: 25)

    visit(new_books_path(page: 5))
    assert page.assert_text('Showing 101 - 125 of 536 new items.')
    assert page.assert_selector('.new-book', count: 25)

    visit(new_books_path(timeframe: '2014-10'))
    assert page.assert_text('Showing 1 - 25 of 119 new items.')
    assert page.assert_selector('.new-book', count: 25)

    visit(new_books_path(subject: 'history'))
    assert page.assert_text('Showing 1 - 17 of 17 new items.')
    assert page.assert_selector('.new-book', count: 17)

    visit(new_books_path(range: 124))
    assert page.assert_text('Showing 1 - 12 of 12 new items.')
    assert page.assert_selector('.new-book', count: 12)

    travel_back
  end

  test 'mappings' do
    visit(mappings_new_books_path)
    assert page.find_all('.subject').length >= 10
    assert page.find_all('.range').length >= 10
  end
end
