require 'test_helper'

class IndexTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 10
  end

  # View time-sensitive special messages
  test 'view special messages' do
    travel_to Chronic.parse('2014-09-13 12:00 PM') do
      visit(root_path)
      assert page.assert_selector('.special-message', count: 1)
      assert page.find('.special-message').assert_text('ILLiad downtime')
    end
  end

  test 'do searches' do
    # by default searches summon
    visit(root_path)
    fill_in(id: 'search-terms', with: 'test')
    page.find('.search-submit-button').click
    assert_equal page.current_host, 'http://ucmo.summon.serialssolutions.com'
    assert_equal page.find('#searchBox_00W')['value'], 'test'

    # searches catalog
    visit(root_path)
    page.find('.search-type-button').click
    page.find('#catalog-search-selector').click
    fill_in(id: 'search-terms', with: 'test')
    page.find('.search-submit-button').click
    assert_equal page.current_host, 'https://quest.searchmobius.org'
    assert_equal page.find('#searcharg')['value'], 'test'

    # searches articles
    visit(root_path)
    page.find('.search-type-button').click
    page.find('#articles-search-selector').click
    fill_in(id: 'search-terms', with: 'test')
    page.find('.search-submit-button').click
    assert_equal page.current_host, 'http://ucmo.summon.serialssolutions.com'
    assert_equal page.find('#searchBox_00W')['value'], 'test'
    assert page.find('.ng-scope.applied').assert_text('Scholarly')

    # searches journals
    visit(root_path)
    page.find('.search-type-button').click
    page.find('#journals-search-selector').click
    fill_in(id: 'search-terms', with: 'test')
    page.find('.search-submit-button').click
    assert_equal page.current_host, 'http://wd8cd4tk5m.search.serialssolutions.com'
    assert page.assert_text('Title begins with "test"')

    # searches website
    visit(root_path)
    page.find('.search-type-button').click
    page.find('#website-search-selector').click
    fill_in(id: 'search-terms', with: 'test')
    page.find('.search-submit-button').click
    assert_equal page.current_host, 'https://www.google.com'
    assert_equal page.find('input')['value'], 'site:library.ucmo.edu test'
  end

  test 'show hours' do
    travel_to Chronic.parse('2016-08-23 12:00 PM') do
      visit(root_path)
      assert page.find('.library-hours-display').assert_text('7:30 AM to Midnight')
    end

    # make sure page doesn't break when hours are nil
    travel_to Chronic.parse('2000-01-01 12:00 AM') do
      visit(root_path)
      assert_equal page.status_code, 200
    end
  end

  test 'show news' do
    travel_to Chronic.parse('2016-09-01 12:00 PM') do
      visit(root_path)
      NewsItem.current_news.each do |news_item|
        next unless news_item.friendly_url.present?
        assert page.assert_selector("#news-item-#{news_item.id}")
      end
    end
  end

  test 'show new books slideshow' do
    visit(root_path)
    assert page.assert_selector('.slick-list')
    assert page.assert_selector('.slick-slide', count: 42)
    active_slide = page.find_all('.slick-active').first
    page.find('.slick-prev').trigger('click')
    assert active_slide != page.find_all('.slick-active').first
  end
end
