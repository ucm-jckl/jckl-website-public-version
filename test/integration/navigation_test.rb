require 'test_helper'

class NavigationTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 10
  end

  test 'show header and footer nav menu items' do
    visit(root_path)

    # make sure each nav menu item is present
    NavigationMenuItem.all.each do |item|
      assert page.assert_selector("#jckl-header .nav-menu-item-#{item.id}", visible: :all)
      assert page.assert_selector("#jckl-footer .nav-menu-item-#{item.id}") if item.show_in_footer && item.url.present?
    end

    # make sure dropdown menu works
    assert page.assert_no_selector('.header-navigation-menu .dropdown.open')
    page.first('.header-navigation-menu .dropdown-toggle').click
    assert page.assert_selector('.header-navigation-menu .dropdown.open')
  end
end
