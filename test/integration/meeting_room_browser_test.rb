require 'test_helper'

class MeetingRoomBrowserTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 5
  end

  test 'load the meeting room browse page' do
    visit(meeting_rooms_path)
    assert page.assert_selector('.room-bookings-room-list .room', count: 15)
  end

  test 'view calendar' do
    visit(meeting_rooms_path)
    room = MeetingRoom.all.sample
    page.find("#room-#{room.room_name}-calendar").click
    assert page.assert_selector('.room-bookings-calendar-modal', count: 1)
    assert page.assert_selector("#room-bookings-iframe-#{room.room_name}", count: 1)
  end

  test 'filter rooms' do
    visit(meeting_rooms_path)
    page.execute_script('$("#availability-filter-start-time").val("2016-09-01 08:00 AM")')
    page.execute_script('$("#availability-filter-end-time").val("2016-09-01 05:00 PM")')
    click_button('Submit')
    assert page.assert_selector('.room-bookings-availability-message', count: 1)
  end
end
