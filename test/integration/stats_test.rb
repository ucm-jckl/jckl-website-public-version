require 'test_helper'

class StatsTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 5
  end

  test 'headcounts' do
    visit(stats_headcounts_path)
    assert page.assert_selector('#headcounts-chart .highcharts-container svg')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'days',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('#headcounts-chart .highcharts-container svg')
    assert page.assert_selector('tr', text: '2014-07-01 (Tue) 30 83 45 53')
    assert page.assert_selector('tr', text: '2014-07-09 (Wed) 42 89 38 52')
    assert page.assert_selector('tr', text: '2014-08-01 (Fri) 20 50 31 22')
    assert page.assert_selector('tr', text: '2014-08-25 (Mon) 282 708 398 239')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'hours',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2014-07-01 (Tue) 11:00 1 8 5 5')
    assert page.assert_selector('tr', text: '2014-07-05 (Sat) 18:00 0 0 0 0')
    assert page.assert_selector('tr', text: '2014-07-14 (Mon) 15:00 4 3 6 2')
    assert page.assert_selector('tr', text: '2014-07-21 (Mon) 08:00 2 1 0 0')
    assert page.assert_selector('tr', text: '2014-07-27 (Sun) 06:00 0 0 0 0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'weeks',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2014, Week 26 (Jun 29) 76 184 90 86')
    assert page.assert_selector('tr', text: '2014, Week 29 (Jul 20) 211 300 201 225')
    assert page.assert_selector('tr', text: '2014, Week 30 (Jul 27) 162 315 134 178')
    assert page.assert_selector('tr', text: '2014, Week 32 (Aug 10) 216 340 241 147')
    assert page.assert_selector('tr', text: '2014, Week 34 (Aug 24) 1417 3439 2345 1162')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'months',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2014-07 844 1526 793 921')
    assert page.assert_selector('tr', text: '2014-08 2767 5602 3759 2302')
    assert page.assert_selector('tr', text: '2014-09 0 0 0 0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'years',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2014 3611 7128 4552 3223')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'hour-averages',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '00:00 4.57 4.88 4.63 3.0')
    assert page.assert_selector('tr', text: '12:00 7.7 13.34 9.48 7.63')
    assert page.assert_selector('tr', text: '14:00 8.14 15.16 9.56 7.23')
    assert page.assert_selector('tr', text: '18:00 6.2 12.9 9.37 6.9')
    assert page.assert_selector('tr', text: '22:00 12.86 26.38 16.88 10.13')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'weekday-averages',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: 'Sun 33.5 85.33 71.5 179.0')
    assert page.assert_selector('tr', text: 'Tue 86.67 168.89 101.0 78.56')
    assert page.assert_selector('tr', text: 'Wed 89.67 175.33 98.56 83.0')
    assert page.assert_selector('tr', text: 'Fri 48.75 91.38 53.13 47.13')
    assert page.assert_selector('tr', text: 'Sat 36.5 160.0 144.5 0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'weekday-hour-averages',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: 'Mon 08:00 0.63 2.13 0.75 1.25')
    assert page.assert_selector('tr', text: 'Tue 21:00 8.0 30.0 3.0 12.0')
    assert page.assert_selector('tr', text: 'Wed 20:00 15.0 40.0 14.5 20.5')
    assert page.assert_selector('tr', text: 'Fri 08:00 1.5 5.38 1.38 1.25')
    assert page.assert_selector('tr', text: 'Sat 14:00 6.5 28.0 34.5 0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'days',
              grouping: 'all'
            }
    ))

    assert page.assert_selector('tr', text: '2014-07-02 (Wed) 225')
    assert page.assert_selector('tr', text: '2014-07-23 (Wed) 210')
    assert page.assert_selector('tr', text: '2014-08-01 (Fri) 123')
    assert page.assert_selector('tr', text: '2014-08-08 (Fri) 100')
    assert page.assert_selector('tr', text: '2014-08-25 (Mon) 1627')

    visit(stats_headcounts_path(
            params: {
              start_time: 'July 1, 2014 12:00 AM',
              end_time: 'August 31, 2014 11:59 PM',
              interval: 'days',
              grouping: 'location'
            }
    ))

    assert page.assert_selector('tr', text: '2014-07-04 (Fri) 0 0 0 0 0 0 0 0 0 0 0 0 0')
    assert page.assert_selector('tr', text: '2014-07-31 (Thu) 4 35 0 2 1 11 4 30 27 10 2 0 12')
    assert page.assert_selector('tr', text: '2014-08-19 (Tue) 34 148 74 14 7 19 75 121 80 41 36 8 161')
    assert page.assert_selector('tr', text: '2014-08-24 (Sun) 11 179 13 0 2 95 11 156 148 21 48 5 346')
    assert page.assert_selector('tr', text: '2014-08-30 (Sat) 9 0 11 9 0 16 6 81 25 15 38 3 142')
  end

  test 'study rooms' do
    visit(stats_studyrooms_path)
    assert page.assert_selector('#headcounts-chart .highcharts-container svg')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'hours',
              grouping: 'all',
              metric: 'total'
            }
    ))

    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 08:00 120')
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 09:00 120')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'hours',
              grouping: 'rooms',
              metric: 'total'
            }
    ))

    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 08:00 60 60')
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 09:00 60 60')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'days',
              grouping: 'all',
              metric: 'percent'
            }
    ))

    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 08:00 60 60')
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 09:00 60 60')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'days',
              grouping: 'rooms',
              metric: 'percent'
            }
    ))

    assert page.assert_selector('tr', text: '2015-08-24 (Mon) 54.0 54.0')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'days',
              grouping: 'all',
              metric: 'percent'
            }
    ))

    assert page.assert_selector('tr', text: '2015-08-24 (Mon) 4.0')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'weeks',
              grouping: 'all',
              metric: 'total'
            }
    ))

    assert page.assert_selector('tr', text: '2015, Week 32 (Aug 09) 120')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'months',
              grouping: 'all',
              metric: 'total'
            }
    ))

    assert page.assert_selector('tr', text: '2015-08 10668')
    assert page.assert_selector('tr', text: '2015-09 29799')

    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
              interval: 'years',
              grouping: 'all',
              metric: 'total'
            }
    ))

    assert page.assert_selector('tr', text: '2015 106783')
  end

  test 'headcount form' do
    visit(stats_headcount_form_path(params: { date: '2016-01-07' }))
    assert page.assert_selector('tr', text: 'HCC Floor 0 1 1 3 2 3 2 1')
  end
end
