require 'test_helper'

class EmployeesTest < ActionDispatch::IntegrationTest
  setup do
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 5
  end

  test 'show index' do
    # test with no department filter
    visit(employees_path)
    employees = Employee.joins(:department).where('hide_from_public_views is null or hide_from_public_views = false')
    offices = Office.joins(:department).order('display_name asc')
    assert page.assert_selector('.employee-name', count: employees.length)
    assert page.assert_selector('.office-name', count: offices.length)

    # test with department filter
    department = Department.all.first
    visit(employees_path(list: department.department_name))
    employees = employees.where(department_id: department.id)
    offices = offices.where(department_id: department.id)
    assert page.assert_selector('.employee-name', count: employees.length)
    assert page.assert_selector('.office-name', count: offices.length)
  end

  test 'show subject specialists' do
    visit(subject_specialists_employees_path)
    subjects = SubjectArea.where.not(subject_librarian_id: nil)
    assert page.has_selector?('.employee-subject', count: subjects.length)
  end
end
