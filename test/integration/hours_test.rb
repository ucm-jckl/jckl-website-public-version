require 'test_helper'

class HoursTest < ActionDispatch::IntegrationTest
  setup do
    travel_to(Chronic.parse('2016-09-15 8:00 AM'))
    Capybara.current_driver = Capybara.javascript_driver
    Capybara.default_max_wait_time = 5
  end

  teardown do
    travel_back
  end

  test 'show index' do
    visit(library_hours_path)

    # Sunday is first day of week
    page.find_all('.weekday-0').each do |weekday|
      assert weekday.find('.calendar-date').assert_text('Sunday')
    end

    # Show correct number of dates
    assert page.assert_selector('.calendar-date', count: 28)

    # Highlight past and present dates
    assert page.assert_selector('.past-date', count: 4)
    assert page.assert_selector('.today', count: 1)
    assert page.find('.today').assert_text('Thursday, September 15')
  end
end
