class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :title
      t.string :teaser
      t.text :article
      t.text :expiration_date
      t.text :friendly_url

      t.timestamps
    end

    add_attachment :news_items, :news_image
  end
end
