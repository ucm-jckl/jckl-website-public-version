class CreateSubjectAreas < ActiveRecord::Migration
  def change
    create_table :subject_areas do |t|
      t.string :subject_name
      t.string :display_name
      t.integer :subject_librarian_id
      t.integer :parent_subject_id
      t.string :fund_code
    end
  end
end
