class CreateHeadcountEntries < ActiveRecord::Migration
  def change
    create_table :headcount_entries do |t|
      t.integer :location_id
      t.integer :headcount
      t.datetime :count_time

    end
  end
end
