class CreateHorsesInCultureTitles < ActiveRecord::Migration
  def change
    create_table :horses_in_culture_titles do |t|
      t.string :title
      t.integer :publication_date
      t.string :publisher
      t.string :publication_place
      t.integer :number_pages
      t.string :illustrations
      t.string :equestrian_discipline
      t.string :breed
      t.string :ethnicity
      t.text :notes
      t.string :subjects
      t.string :author

      t.timestamps
    end
  end
end
