class CreateNewBooks < ActiveRecord::Migration
  def change
    create_table :new_books do |t|
      t.text :title
      t.string :author
      t.string :call_number
      t.text :isns
      t.string :year
      t.text :summary
      t.integer :location_code_id
      t.string :image_url
      t.string :info_page_url
      t.integer :record_number, limit: 8
      t.integer :item_record_id, limit: 8
      t.integer :bib_record_id, limit: 8
      t.date :cdate

      t.timestamps
    end

    create_table :location_codes do |t|
      t.string :location_code
      t.string :location_name
    end

    create_table :new_book_requests do |t|
      t.string :requestor_name
      t.string :requestor_email
      t.string :format
      t.string :title
      t.string :edition
      t.string :author
      t.string :isbn
      t.string :publisher
      t.string :year
      t.string :series
      t.string :volumes
      t.decimal :list_price
      t.integer :copies_ordered
      t.string :recommender
      t.string :approved_by
      t.integer :subject_area_id
      t.text :notes
      t.string :submitted_by

      t.timestamps
    end
  end
end
