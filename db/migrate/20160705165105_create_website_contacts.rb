class CreateWebsiteContacts < ActiveRecord::Migration
  def change
    create_table :website_contacts do |t|
      t.string :contact_name
      t.string :contact_email
      t.string :topic
      t.string :recipients
      t.string :subject
      t.text :message

      t.timestamps
    end
  end
end
