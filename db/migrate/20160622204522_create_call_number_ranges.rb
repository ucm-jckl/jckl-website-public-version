class CreateCallNumberRanges < ActiveRecord::Migration
  def change
    create_table :call_number_ranges do |t|
      t.string :range_start
      t.string :range_end
      t.string :range_name
      t.integer :subject_area_id
    end
  end
end
