class CreateStudyRooms < ActiveRecord::Migration
  def change
    create_table :study_rooms do |t|
      t.string :room_name
      t.string :display_name
      t.integer :location_id
      t.boolean :active
    end

    create_table :study_room_bookings do |t|
      t.integer :study_room_id
      t.string :email
      t.datetime :booking_time
      t.integer :booking_duration_minutes
    end
  end
end
