class CreateSpecialMessages < ActiveRecord::Migration
  def change
    create_table :special_messages do |t|
      t.string :title
      t.text :content
      t.datetime :expiration_date

      t.timestamps
    end
  end
end
