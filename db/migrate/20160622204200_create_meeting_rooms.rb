class CreateMeetingRooms < ActiveRecord::Migration
  def change
    create_table :meeting_rooms do |t|
      t.string :room_name
      t.string :display_name
      t.string :description
      t.string :google_calendar_id
      t.integer :floor
      t.boolean :library_only
      t.boolean :faculty_staff_only
      t.boolean :classroom
      t.boolean :meetings
      t.integer :seating
      t.integer :desktops
      t.integer :laptops
      t.boolean :projector
      t.boolean :podium
      t.boolean :whiteboard
    end

    create_table :meeting_room_bookings do |t|
      t.string :requestor_name
      t.string :requestor_email
      t.string :contact_name
      t.string :contact_email
      t.string :contact_phone
      t.string :contact_dept
      t.string :contact_address
      t.integer :meeting_room_id
      t.datetime :start_time
      t.datetime :end_time
      t.string :event_name
      t.string :setup_time
      t.string :takedown_time
      t.string :recurrence
      t.integer :attendance
      t.string :notes

      t.timestamps
    end

    add_attachment :meeting_rooms, :photo
  end
end
