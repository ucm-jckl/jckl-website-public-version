class CreateCirculationTransactions < ActiveRecord::Migration
  def change
    create_table :circulation_transactions do |t|
      t.integer :transaction_id, limit: 8
      t.datetime :transaction_gmt
      t.string :application_name
      t.string :source_code
      t.string :op_code
      t.integer :item_record_id, limit: 8
      t.integer :bib_record_id, limit: 8
      t.integer :stat_group_code_num
      t.text :best_title_norm
      t.string :best_author_norm
      t.string :item_location_code
      t.string :call_number
      t.integer :itype_code
      t.string :itype_name
      t.integer :ptype_code
      t.string :ptype_name
      t.integer :patron_checkout_total
      t.string :patron_home_library_code
    end
  end
end
