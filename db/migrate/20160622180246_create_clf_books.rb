class CreateClfBooks < ActiveRecord::Migration
  def change
    create_table :clf_books do |t|
      t.string :title
      t.string :primary_author
      t.string :secondary_author
      t.string :illustrator
      t.string :isbn
      t.decimal :price
      t.integer :festival_year
      t.text :summary
      t.string :image_url
      t.string :info_page_url

      t.timestamps
    end

    create_table :clf_orders do |t|
      t.string :school_name
      t.string :contact_name
      t.string :contact_phone
      t.boolean :tax_exempt
      t.string :tax_exempt_number
      t.string :comment
      t.string :session_id
      t.boolean :is_submitted

      t.timestamps
    end

    create_table :clf_book_order_links do |t|
      t.integer :book_id
      t.integer :order_id
      t.integer :quantity

      t.timestamps
    end
  end
end
