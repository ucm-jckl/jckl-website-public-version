class CreateComputers < ActiveRecord::Migration
  def change
    create_table :computers do |t|
      t.string :map_tag
      t.string :ip_address
      t.string :computer_name
      t.integer :floor
      t.integer :computer_build_id
      t.boolean :active
    end

    create_table :computer_builds do |t|
      t.string :build_name
      t.string :description
      t.integer :computer_operating_system_id
    end

    create_table :computer_operating_systems do |t|
      t.string :os_family_name
      t.string :os_version_name
    end

    create_table :computer_softwares do |t|
      t.string :display_name
      t.string :description
      t.string :version
    end

    create_table :computer_software_links do |t|
      t.integer :computer_id
      t.integer :computer_software_id
    end

    create_table :computer_build_software_links do |t|
      t.integer :computer_build_id
      t.integer :computer_software_id
    end

    create_table :computer_sessions do |t|
      t.integer :computer_id
      t.integer :ping_count

      t.timestamps
    end
  end
end
