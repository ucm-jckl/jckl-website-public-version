class CreateBarcodeFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :barcode_files do |t|
      t.text :barcodes
      t.integer :user_id
      t.text :notes
      t.boolean :processed

      t.timestamps
    end
  end
end
