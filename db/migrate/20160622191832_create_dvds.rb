class CreateDvds < ActiveRecord::Migration
  def change
    create_table :dvds do |t|
      t.string :title
      t.string :subtitle
      t.string :title_sort
      t.string :copyright_date
      t.text :summary
      t.string :image_url
      t.string :imdb_id
      t.string :tmdb_id
      t.string :call_number
      t.string :volume
      t.integer :location_code_id
      t.decimal :rating
      t.string :runtime
      t.string :language
      t.integer :checkouts
      t.integer :ytd_checkouts
      t.integer :last_ytd_checkouts
      t.boolean :available
      t.date :cdate
      t.integer :item_record_id, limit: 8
      t.integer :bib_record_id, limit: 8
      t.integer :item_record_num, limit: 8
      t.integer :bib_record_num, limit: 8
      t.boolean :is_marc_data_harvested
      t.boolean :is_tmdb_data_harvested
      t.timestamps
    end

    create_table :dvd_genres do |t|
      t.string :display_name
      t.integer :tmdb_id
    end

    create_table :dvd_genre_links do |t|
      t.integer :dvd_id
      t.integer :dvd_genre_id
    end
  end
end
