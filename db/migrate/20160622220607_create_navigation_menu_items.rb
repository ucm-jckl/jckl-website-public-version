class CreateNavigationMenuItems < ActiveRecord::Migration
  def change
    create_table :navigation_menu_items do |t|
      t.string :url
      t.string :label
      t.string :element_class
      t.string :element_id
      t.integer :position
      t.boolean :show_in_footer
      t.integer :navigation_menu_item_id

      t.timestamps
    end
  end
end
