class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :location_name
      t.string :display_name
      t.integer :floor
      t.integer :sort
      t.date :stat_form_start_date
      t.date :stat_form_end_date
    end
  end
end
