class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.string :display_name
      t.string :office_name
      t.integer :department_id
      t.string :email
      t.integer :primary_employee_id
      t.string :phone_number
      t.string :location
      t.string :webpage_url
    end
  end
end
