class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :office
      t.string :title
      t.string :rank
      t.boolean :is_faculty
      t.integer :supervisor_id
      t.integer :department_id
      t.string :image_name
      t.string :libguides_profile_url
      t.boolean :hide_from_public_views
    end

    create_table :departments do |t|
      t.string :department_name
      t.string :display_name
      t.integer :department_head_id
      t.string :parent_department_id
    end
  end
end
