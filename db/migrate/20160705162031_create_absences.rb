class CreateAbsences < ActiveRecord::Migration
  def change
    create_table :absences do |t|
      t.integer :employee_id
      t.integer :submitter_id
      t.date :start_date
      t.date :end_date
      t.integer :absence_type_id
      t.integer :duration_minutes
      t.text :schedule
      t.text :background_information
      t.string :professional_funding
      t.boolean :approved
      t.boolean :note_required
      t.boolean :note_received
      t.text :google_calendar_event_id

      t.timestamps
    end

    create_table :absence_types do |t|
      t.string :absence_type_name
      t.string :display_name
      t.integer :sort
    end
  end
end
