class CreateLibraryHours < ActiveRecord::Migration
  def change
    create_table :library_hours do |t|
      t.date :calendar_date
      t.datetime :open_time
      t.datetime :close_time
      t.string :hours_display_text
      t.integer :total_calendar_date_minutes
      t.integer :total_operating_date_minutes
      t.integer :hour00minutes
      t.integer :hour01minutes
      t.integer :hour02minutes
      t.integer :hour03minutes
      t.integer :hour04minutes
      t.integer :hour05minutes
      t.integer :hour06minutes
      t.integer :hour07minutes
      t.integer :hour08minutes
      t.integer :hour09minutes
      t.integer :hour10minutes
      t.integer :hour11minutes
      t.integer :hour12minutes
      t.integer :hour13minutes
      t.integer :hour14minutes
      t.integer :hour15minutes
      t.integer :hour16minutes
      t.integer :hour17minutes
      t.integer :hour18minutes
      t.integer :hour19minutes
      t.integer :hour20minutes
      t.integer :hour21minutes
      t.integer :hour22minutes
      t.integer :hour23minutes
    end
  end
end
