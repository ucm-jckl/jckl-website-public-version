class CreateShelfColumns < ActiveRecord::Migration[5.0]
  def change
    create_table :shelf_columns do |t|
      t.string :code
    end

    create_table :shelf_column_inventory_dates do |t|
      t.integer :shelf_column_id
      t.datetime :date_inventoried
      t.integer :user_id
    end
  end
end
