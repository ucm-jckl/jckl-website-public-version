# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161209202321) do

  create_table "absence_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "absence_type_name"
    t.string  "display_name"
    t.integer "sort"
  end

  create_table "absences", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "employee_id"
    t.integer  "submitter_id"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "absence_type_id"
    t.integer  "duration_minutes"
    t.text     "schedule",                 limit: 65535
    t.text     "background_information",   limit: 65535
    t.string   "professional_funding"
    t.boolean  "approved"
    t.boolean  "note_required"
    t.boolean  "note_received"
    t.text     "google_calendar_event_id", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "barcode_files", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "barcodes",   limit: 65535
    t.integer  "user_id"
    t.text     "notes",      limit: 65535
    t.boolean  "processed"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "call_number_ranges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "range_start"
    t.string  "range_end"
    t.string  "range_name"
    t.integer "subject_area_id"
  end

  create_table "circulation_transactions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint   "transaction_id"
    t.datetime "transaction_gmt"
    t.string   "application_name"
    t.string   "source_code"
    t.string   "op_code"
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.integer  "stat_group_code_num"
    t.text     "best_title_norm",          limit: 65535
    t.string   "best_author_norm"
    t.string   "item_location_code"
    t.string   "call_number"
    t.integer  "itype_code"
    t.string   "itype_name"
    t.integer  "ptype_code"
    t.string   "ptype_name"
    t.integer  "patron_checkout_total"
    t.string   "patron_home_library_code"
  end

  create_table "clf_book_order_links", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "book_id"
    t.integer  "order_id"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clf_books", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "primary_author"
    t.string   "secondary_author"
    t.string   "illustrator"
    t.string   "isbn"
    t.decimal  "price",                          precision: 10
    t.integer  "festival_year"
    t.text     "summary",          limit: 65535
    t.string   "image_url"
    t.string   "info_page_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clf_orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "school_name"
    t.string   "contact_name"
    t.string   "contact_phone"
    t.boolean  "tax_exempt"
    t.string   "tax_exempt_number"
    t.string   "comment"
    t.string   "session_id"
    t.boolean  "is_submitted"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "computer_build_software_links", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "computer_build_id"
    t.integer "computer_software_id"
  end

  create_table "computer_builds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "build_name"
    t.string  "description"
    t.integer "computer_operating_system_id"
  end

  create_table "computer_operating_systems", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "os_family_name"
    t.string "os_version_name"
  end

  create_table "computer_sessions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "computer_id"
    t.integer  "ping_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "computer_software_links", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "computer_id"
    t.integer "computer_software_id"
  end

  create_table "computer_softwares", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "display_name"
    t.string "description"
    t.string "version"
  end

  create_table "computers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "map_tag"
    t.string  "ip_address"
    t.string  "computer_name"
    t.integer "floor"
    t.integer "computer_build_id"
    t.boolean "active"
  end

  create_table "departments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "department_name"
    t.string  "display_name"
    t.integer "department_head_id"
    t.string  "parent_department_id"
  end

  create_table "dvd_genre_links", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "dvd_id"
    t.integer "dvd_genre_id"
  end

  create_table "dvd_genres", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "display_name"
    t.integer "tmdb_id"
  end

  create_table "dvds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "subtitle"
    t.string   "title_sort"
    t.string   "copyright_date"
    t.text     "summary",                limit: 65535
    t.string   "image_url"
    t.string   "imdb_id"
    t.string   "tmdb_id"
    t.string   "call_number"
    t.string   "volume"
    t.integer  "location_code_id"
    t.decimal  "rating",                               precision: 10
    t.string   "runtime"
    t.string   "language"
    t.integer  "checkouts"
    t.integer  "ytd_checkouts"
    t.integer  "last_ytd_checkouts"
    t.boolean  "available"
    t.date     "cdate"
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.bigint   "item_record_num"
    t.bigint   "bib_record_num"
    t.boolean  "is_marc_data_harvested"
    t.boolean  "is_tmdb_data_harvested"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "first_name"
    t.string  "last_name"
    t.string  "email"
    t.string  "phone"
    t.string  "office"
    t.string  "title"
    t.string  "rank"
    t.boolean "is_faculty"
    t.integer "supervisor_id"
    t.integer "department_id"
    t.string  "image_name"
    t.string  "libguides_profile_url"
    t.boolean "hide_from_public_views"
  end

  create_table "headcount_entries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "location_id"
    t.integer  "headcount"
    t.datetime "count_time"
  end

  create_table "horses_in_culture_titles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.integer  "publication_date"
    t.string   "publisher"
    t.string   "publication_place"
    t.integer  "number_pages"
    t.string   "illustrations"
    t.string   "equestrian_discipline"
    t.string   "breed"
    t.string   "ethnicity"
    t.text     "notes",                 limit: 65535
    t.string   "subjects"
    t.string   "author"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "library_hours", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date     "calendar_date"
    t.datetime "open_time"
    t.datetime "close_time"
    t.string   "hours_display_text"
    t.integer  "total_calendar_date_minutes"
    t.integer  "total_operating_date_minutes"
    t.integer  "hour00minutes"
    t.integer  "hour01minutes"
    t.integer  "hour02minutes"
    t.integer  "hour03minutes"
    t.integer  "hour04minutes"
    t.integer  "hour05minutes"
    t.integer  "hour06minutes"
    t.integer  "hour07minutes"
    t.integer  "hour08minutes"
    t.integer  "hour09minutes"
    t.integer  "hour10minutes"
    t.integer  "hour11minutes"
    t.integer  "hour12minutes"
    t.integer  "hour13minutes"
    t.integer  "hour14minutes"
    t.integer  "hour15minutes"
    t.integer  "hour16minutes"
    t.integer  "hour17minutes"
    t.integer  "hour18minutes"
    t.integer  "hour19minutes"
    t.integer  "hour20minutes"
    t.integer  "hour21minutes"
    t.integer  "hour22minutes"
    t.integer  "hour23minutes"
  end

  create_table "location_codes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "location_code"
    t.string "location_name"
  end

  create_table "locations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "location_name"
    t.string  "display_name"
    t.integer "floor"
    t.integer "sort"
    t.date    "stat_form_start_date"
    t.date    "stat_form_end_date"
  end

  create_table "meeting_room_bookings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "requestor_name"
    t.string   "requestor_email"
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "contact_phone"
    t.string   "contact_dept"
    t.string   "contact_address"
    t.integer  "meeting_room_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "event_name"
    t.string   "setup_time"
    t.string   "takedown_time"
    t.string   "recurrence"
    t.integer  "attendance"
    t.string   "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "meeting_rooms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "room_name"
    t.string   "display_name"
    t.string   "description"
    t.string   "google_calendar_id"
    t.integer  "floor"
    t.boolean  "library_only"
    t.boolean  "faculty_staff_only"
    t.boolean  "classroom"
    t.boolean  "meetings"
    t.integer  "seating"
    t.integer  "desktops"
    t.integer  "laptops"
    t.boolean  "projector"
    t.boolean  "podium"
    t.boolean  "whiteboard"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "navigation_menu_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "url"
    t.string   "label"
    t.string   "element_class"
    t.string   "element_id"
    t.integer  "position"
    t.boolean  "show_in_footer"
    t.integer  "navigation_menu_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "new_book_requests", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "requestor_name"
    t.string   "requestor_email"
    t.string   "format"
    t.string   "title"
    t.string   "edition"
    t.string   "author"
    t.string   "isbn"
    t.string   "publisher"
    t.string   "year"
    t.string   "series"
    t.string   "volumes"
    t.decimal  "list_price",                    precision: 10
    t.integer  "copies_ordered"
    t.string   "recommender"
    t.string   "approved_by"
    t.integer  "subject_area_id"
    t.text     "notes",           limit: 65535
    t.string   "submitted_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "new_books", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "title",            limit: 65535
    t.string   "author"
    t.string   "call_number"
    t.text     "isns",             limit: 65535
    t.string   "year"
    t.text     "summary",          limit: 65535
    t.integer  "location_code_id"
    t.string   "image_url"
    t.string   "info_page_url"
    t.bigint   "record_number"
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.date     "cdate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.string   "teaser"
    t.text     "article",                 limit: 65535
    t.text     "expiration_date",         limit: 65535
    t.text     "friendly_url",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "news_image_file_name"
    t.string   "news_image_content_type"
    t.integer  "news_image_file_size"
    t.datetime "news_image_updated_at"
  end

  create_table "offices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "display_name"
    t.string  "office_name"
    t.integer "department_id"
    t.string  "email"
    t.integer "primary_employee_id"
    t.string  "phone_number"
    t.string  "location"
    t.string  "webpage_url"
  end

  create_table "shelf_column_inventory_dates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "shelf_column_id"
    t.datetime "date_inventoried"
    t.integer  "user_id"
  end

  create_table "shelf_columns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "code"
  end

  create_table "special_messages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.text     "content",         limit: 65535
    t.datetime "expiration_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "study_room_bookings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "study_room_id"
    t.string   "email"
    t.datetime "booking_time"
    t.integer  "booking_duration_minutes"
  end

  create_table "study_rooms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "room_name"
    t.string  "display_name"
    t.integer "location_id"
    t.boolean "active"
  end

  create_table "subject_areas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "subject_name"
    t.string  "display_name"
    t.integer "subject_librarian_id"
    t.integer "parent_subject_id"
    t.string  "fund_code"
  end

  create_table "user_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "name"
    t.integer "user_id"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "employee_id"
    t.integer  "id700"
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "uid"
    t.string   "provider"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  create_table "website_contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "topic"
    t.string   "recipients"
    t.string   "subject"
    t.text     "message",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
