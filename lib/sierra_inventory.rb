require 'capybara/dsl'
require 'capybara/poltergeist'

class SierraInventory
  extend Capybara::DSL

  def self.barcodes_to_circa(barcodes)
    Capybara.javascript_driver = :poltergeist
    Capybara.default_driver = :poltergeist
    Capybara.run_server = false
    visit('https://quest.searchmobius.org/iii/airwkst/')
    fill_in('login', with: 'ucmscripts')
    fill_in('loginpassword', with: Rails.application.secrets.sierra_password[0,11]) # Circa randomly uses just the first 12 characters and breaks if more are used. Weird but whatever.
    click_button('submit')
    click_link('Inventory Control')

    barcodes.each do |barcode|
      #fill in the barcode and submit
      fill_in('searchstring', with: barcode)
      click_button('submit')

      #wait for results page to load before doing next barcode
      page.find('h4', text: barcode, wait: 30)
    end
  end

  # Run the batch inventory process in Sierra via telnet
  # Note: won't work on Windows because of lack of PTY support
  def self.process_inventory
    # Open a PTY (pseudoterminal) SSH session to quest.searchmobius.org
    PTY.spawn('ssh sadmin@quest.searchmobius.org') do |reader, writer, _pid|
      # Navigate the Sierra character-based interface

      reader.sync = true

      # Log in
      writer.puts('saiii') if reader.expect(/password:/i).present?
      writer.puts('ucmscripts') if reader.expect(/login:/i).present?
      # Sierra telnet app randomly uses just the first 12 characters and breaks if more are used. Weird but whatever.
      writer.puts(Rails.application.secrets.sierra_password[0, 11]) if reader.expect(/password:/i).present?

      # Navigate to Inventory Control
      if reader.expect(/C > CIRCULATION subsystem/i, 5).present?
        writer.puts('c')
      else
        puts 'could not find circulation'
      end

      if reader.expect(/P > PROCESS PC transactions/i, 5).present?
        writer.print('p')
      else
        puts 'could not find pc transactions'
      end

      if reader.expect(/login:/i).present?
        writer.puts('ucmscripts')
      else
        puts 'could not find login'
      end

      if reader.expect(/password:/i).present?
        writer.puts(Rails.application.secrets.sierra_password[0, 11]) # Sierra telnet app randomly uses just the first 12 characters and breaks if more are used. Weird but whatever.
      else
        puts 'could not find password field'
      end

      if reader.expect(/C > COMPARE inventory to shelf list/i, 10).present?
        writer.puts('c')
        puts 'found compare inventory to shelf list'
      else
        puts 'could not find compare inventory'
      end

      # Run inventory and shelf list application for each file
      if reader.expect(/C > COMPARE file of barcodes to shelflist/i, 5).present?
        writer.print('c')
      else
        puts 'could not find compare file menu'
      end

      more_files_exist = true
      while more_files_exist
        puts 'looping through barcode files'
        if reader.expect(/1 > rdi/i, 5).present?
          writer.print('1')
          puts 'found a barcode file'
        else
          more_files_exist = false
          puts 'could not find a compare file'
        end

        if reader.expect(/E > List ERRORS only/i, 5).present?
          writer.print('e')
          puts 'listing errors'
        else
          puts 'could not find list errors'
        end

        if reader.expect(/List items recorded as checked out?/i, 5).present?
          writer.print('n')
          puts 'not listing as checked out'
        else
          puts 'could not find not listing as checked out'
        end

        skipping_barcode_errors = true
        i = 0
        while skipping_barcode_errors
          writer.print(' ')
          writer.print('n')
          if reader.expect(/P > PRINT/i, 1).present?
            skipping_barcode_errors = false
            writer.print('p')
            puts 'printing report'
          else
            i + +
            skipping_barcode_errors = false if i > 1200
          end
          sleep(1)
        end

        if reader.expect(/3 > E-mail Printer/i, 5).present?
          writer.print('3')
        else
          puts 'could not find email'
        end

        if reader.expect(/Enter full email address/i, 5).present?
          writer.puts('tlheater@ucmo.edu')
        else
          puts 'could not find email address field'
        end

        if reader.expect(/Email Note/i, 5).present?
          writer.puts('')
        else
          puts 'could not find email note field'
        end

        if reader.expect(/Q > QUIT/i, 5).present?
          writer.puts('q')
        else
          puts 'could not find quit report'
        end

        if reader.expect(/Update inventory date/i, 5).present?
          writer.print('y')
        else
          puts 'could not find update inventory'
        end

        if reader.expect(/Press <SPACE> to continue/i, 60).present?
          writer.puts(' ')
        else
          puts 'could not find continue'
        end

        if reader.expect(/Remove inventory file/i, 5).present?
          writer.print('y')
        else
          puts 'could not find remove file'
        end
      end

      # Exit the module and telnet app
      writer.puts('q') if reader.expect(/Q > QUIT/i, 15).present?

      writer.puts('q') if reader.expect(/Q > QUIT/i, 15).present?

      writer.puts('q') if reader.expect(/Q > QUIT/i, 15).present?

      writer.puts('q') if reader.expect(/Q > QUIT/i, 15).present?

      writer.puts('q') if reader.expect(/Q > QUIT/i, 15).present?
    end
  end
end
