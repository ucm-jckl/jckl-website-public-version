class SierraBursar
  # Class variables
  @@bursar_directory = Rails.env.development? ? "#{Rails.root}/bursar" : '/home/ucm-bursar'

  def self.run_all_tasks
    bursar_out
    reformat_bursar_file
    bursar_in
  end

  def self.reformat_bursar_file
    # Create a backup of the bursar file
    filename = "#{@@bursar_directory}/in/bursar.send"
    if File.exist?(filename)
      FileUtils.cp(filename, "#{@@bursar_directory}/in/bursar.send.#{Time.now.to_s(:number)}")
    end

    # open bursar out file and read to array
    invoices = {}
    to_sierra = []
    to_banner = []

    input_file = CSV.read(filename, col_sep: '|', quote_char: "\x00")

    # consolidate duplicated invoices
    input_file.each do |row|
      invoices[row[0]] ||= []
      invoices[row[0]] << row
    end

    header_date = nil
    invoices.each do |_invoice_number, data|
      # skip non-ucm patrons
      next unless data[0][2].start_with?('ck') && data[0][4].start_with?('b700')

      # skip special patrons
      next if data[0][4] == 'b777777777' # ILL
      next if data[0][4] == 'b444444441' # displays

      # check if header row and save the header date
      header_date = data[0][8] if data[0][0] == 'HEADER'

      total_fine = 0
      data.each do |row|
        total_fine += row[15].to_i + row[16].to_i + row[17].to_i
      end
      sierra_fine = total_fine
      sierra_fine = 0 if total_fine < 0

      # reformat each line into the fixed-length 63-character format required by bursar in (http://csdirect.iii.com/sierrahelp/Default.shtml#sadmin/sadmin_admin_corner_bursarin_viewfile.html)
      # For string formating info see http://ruby-doc.org/core-2.2.3/String.html#method-i-25
      sierra_row = {
        payment_date: header_date || Time.zone.now.strftime('%y%m%d'), # 6 char, yyddmm
        invoice_number: '%-6s' % data[0][0], # 6 char
        bursar_initials: '%-12s' % 'jckl-bursar', # 12 char
        patron_record: '%-10s' % data[0][3], # 10 char, can omit if using patron_barcode
        patron_barcode: '%-20s' % '', # data[0][4], # 20 char, can omit if using patron record
        payment_amount: ('%8s' % sierra_fine).tr(' ', '0'), # 8 char, 00004500 = $45
        payment_type_code: ' ' # 1 char, " " = paid in full, "2" = partial payment, "3" = waived, we always want paid in full
      }

      # Put the row together as a non-delimited string terminated by /r
      to_sierra << sierra_row[:payment_date] + sierra_row[:invoice_number] + sierra_row[:bursar_initials] + sierra_row[:patron_record] + sierra_row[:patron_barcode] + sierra_row[:payment_amount] + sierra_row[:payment_type_code] + "\r\n"

      # Format the row from bursar_out into a Banner format
      semester_code = case Time.zone.now.strftime('%m')
                      when '00', '01', '02', '03', '04', '05'
                        "#{Time.zone.now.strftime('%Y')}20"
                      when '06', '07'
                        "#{Time.zone.now.strftime('%Y')}30"
                      when '08', '09', '10', '11', '12'
                        "#{Time.zone.now.strftime('%Y')}10"
      end

      charge_type = case data[0][14]
                    when 'OVERDUE', 'OVERDUE_REN', 'MAN_CHARGE', 'MANUAL'
                      'JCK1'
                    when 'OVERDUEX', 'REPLACEMENT'
                      'JCK2'
      end

      banner_row = {
        invoice_number: data[0][0],
        invoice_date: Time.zone.now.strftime('%Y-%m-%d'),
        term_code: semester_code,
        patron_700: data[0][4].delete('b'),
        charge_type: charge_type,
        total: total_fine / 100.to_f
      }

      # Put the banner row into the banner array
      to_banner << banner_row.values
    end

    # backup existing bursar in file and save new one
    if File.exist?("#{@@bursar_directory}/out/sierra/bursar.in")
      FileUtils.cp("#{@@bursar_directory}/out/sierra/bursar.in", "#{@@bursar_directory}/out/sierra/bursar.in.#{Time.now.to_s(:number)}")
    end
    File.open("#{@@bursar_directory}/out/sierra/bursar.in", 'w') do |sierra_file|
      sierra_file.printf(to_sierra.join(''))
    end

    # save banner data to a csv file
    CSV.open("#{@@bursar_directory}/out/banner/bursar.banner.#{Time.now.to_s(:number)}.csv", 'w') do |csv|
      csv << ['INVOICE #', 'INVOICE DATE', 'TERM CODE', '700#', 'CHARGE TYPE', 'TOTAL']
      to_banner.each do |row|
        csv << row
      end
    end
  end

  # Initiate bursar out and send file via FTP.
  # Note: won't work on Windows because of lack of PTY support
  def self.bursar_out
    errors = []
    start_time = Time.zone.now

    # Open a PTY (pseudoterminal) SSH session to quest.searchmobius.org
    PTY.spawn('ssh xxxxx@quest.searchmobius.org') do |reader, writer, _pid|
      # Navigate the Sierra character-based interface
      reader.sync = true

      # Log in
      if reader.expect(/password:/i, 5).present?
        writer.puts('xxxxx')
      else
        errors << '1: Could not log in.'
      end

      if reader.expect(/login:/i, 5).present?
        writer.puts('xxxxx')
      else
        errors << '2: Could not log in.'
      end

      if reader.expect(/password:/i, 5).present?
        writer.puts(Rails.application.secrets.sierra_password[0, 11]) # Sierra telnet app randomly uses just the first 12 characters and breaks if more are used. Weird but whatever.
      else
        errors << '3: Could not log in.'
      end

      # Navigate to Bursar Out
      if reader.expect(/C > CIRCULATION subsystem/i, 15).present?
        writer.puts('c')
      else
        errors << '4: Could not find C > CIRCULATION subsystem.'
      end

      if reader.expect(/F > FINANCIAL functions/i, 15).present?
        writer.puts('f')
      else
        errors << '5: Could not find F > FINANCIAL functions.'
      end

      if reader.expect(/O > OUTPUT to bursar's office/i, 15).present?
        writer.puts('o')
      else
        errors << "6: Could not find O > OUTPUT to bursar's office."
      end

      # Create new file
      if reader.expect(/C > CREATE bursar output file/i, 15).present?
        writer.puts('c')
      else
        errors << '7: Could not find C > CREATE bursar output file.'
      end

      if reader.expect(/Do you want to clear/i, 15).present?
        writer.puts('y')
      else
        errors << '8: Could not find clear file prompt.'
      end

      if reader.expect(/Press <space> to continue/i, 600).present?
        writer.puts(' ')
      else
        errors << '9: Could not clear file.'
      end

      # Sort file to generate bursar.send
      if reader.expect(/S > SORT bursar output file/i, 15).present?
        writer.puts('s')
      else
        errors << '10: Could not find S > SORT bursar output file.'
      end

      if reader.expect(/Press <space> to continue/i, 600).present?
        writer.puts(' ')
      else
        errors << '11: Could not sort file.'
      end

      # Transmit file
      if reader.expect(/T > TRANSMIT bursar file/i, 15).present?
        writer.puts('t')
      else
        errors << '12: Could not find T > TRANSMIT bursar file.'
      end

      if reader.expect(/T > Transfer files/i, 15).present?
        writer.puts('t')
      else
        errors << '13: Could not find T > Transfer files.'
      end

      if reader.expect(/Enter name of remote file/i, 15).present?
        writer.puts(' ')
      else
        errors << '14: Could not find enter remote file name prompt.'
      end

      if reader.expect(/C > CONTINUE/i, 15).present?
        writer.puts('c')
      else
        errors << '15: Could not find C > CONTINUE.'
      end

      if reader.expect(/Q > QUIT/i, 15).present?
        writer.puts('q')
      else
        errors << '16: Could not find Q > QUIT.'
      end

      if reader.expect(/Press <space> to continue/i, 600).present?
        writer.puts(' ')
      else
        errors << '17: Could not quit file transfer.'
      end

      # Remove files
      if reader.expect(/R > REMOVE files/i, 15).present?
        writer.print('r')
      else
        errors << '18: Could not find R > REMOVE files.'
      end

      if reader.expect(/Input numbers of files to be removed/i, 15).present?
        writer.puts("1-2\r\n")
      else
        errors << '19: Could not find file number removal prompt.'
      end

      if reader.expect(/Remove file bursar.out/i, 15).present?
        writer.print('y')
      else
        errors << '20: Could not find file remove confirmation prompt.'
      end

      if reader.expect(/Remove file bursar.send/i, 15).present?
        writer.print('y')
      else
        errors << '21: Could not find file remove confirmation prompt.'
      end

      if reader.expect(/Q > QUIT/i, 15).present?
        writer.puts('q')
      else
        errors << '22: Could not find Q > QUIT.'
      end
    end

    if Time.zone.now - start_time > 15.minutes
      errors << '23: Warning - Bursar Out took unusually long to complete (> 15 minutes)'
    end

    BursarMailer.error_notification('Bursar Out', errors).deliver_later if errors.present?
  end

  def self.bursar_in
    errors = []
    start_time = Time.zone.now

    # Open a PTY (pseudoterminal) SSH session to quest.searchmobius.org
    PTY.spawn('ssh xxxxx@quest.searchmobius.org') do |reader, writer, _pid|
      # Navigate the Sierra character-based interface

      # Log in
      if reader.expect(/password:/i, 5).present?
        writer.puts('xxxxx')
      else
        errors << '1: Could not log in.'
      end

      if reader.expect(/login:/i, 5).present?
        writer.puts('xxxxx')
      else
        errors << '2: Could not log in.'
      end

      if reader.expect(/password:/i, 5).present?
        writer.puts(Rails.application.secrets.sierra_password[0, 11]) # Sierra telnet app randomly uses just the first 12 characters and breaks if more are used. Weird but whatever.
      else
        errors << '3: Could not log in.'
      end

      # Navigate to Bursar In
      if reader.expect(/C > CIRCULATION subsystem/i, 15).present?
        writer.puts('c')
      else
        errors << '4: Could not find C > CIRCULATION subsystem.'
      end

      if reader.expect(/F > FINANCIAL functions/i, 15).present?
        writer.puts('f')
      else
        errors << '5: Could not find F > FINANCIAL functions.'
      end

      if reader.expect(/I > Bursar IN/i, 15).present?
        writer.puts('i')
      else
        errors << '6: Could not find I > Bursar IN'
      end

      if reader.expect(/G > GET payment data from bursar\'s office/i, 15).present?
        writer.puts('g')
      else
        errors << "7: Could not find G > GET payment data from bursar's office"
      end

      if reader.expect(/Assign what name/i, 15).present?
        writer.puts('')
      else
        errors << '8: Could not find assign name prompt.'
      end

      if reader.expect(/Choose one/i, 15).present?
        writer.puts('22')
      else
        errors << '9: Could not find choose prompt.'
      end

      if reader.expect(/Username/i, 15).present?
        writer.puts('ucm-bursar')
      else
        errors << '10: Could not find FTP username prompt.'
      end

      if reader.expect(/Password/i, 15).present?
        writer.puts(Rails.application.secrets.bursar_ftp_password)
      else
        errors << '11: Could not find FTP password prompt.'
      end

      if reader.expect(/C > CHANGE directory/i, 15).present?
        writer.puts('c')
      else
        errors << '12: Could not find C > CHANGE directory.'
      end

      if reader.expect(/2 > out/i, 15).present?
        writer.puts('2')
      else
        errors << '13: Could not find 2 > out.'
      end

      if reader.expect(/2 > sierra/i, 15).present?
        writer.puts('2')
      else
        errors << '14: Could not find 2 > sierra'
      end

      if reader.expect(/Choose one \(V,U,E\)/i, 15).present?
        writer.puts('V')
      else
        errors << '15: Could not find Choose one.'
      end

      if reader.expect(/1 > /i, 15).present?
        writer.puts('1')
      else
        errors << '16: Could not find 1 >'
      end

      if reader.expect(/T > TRANSFER files/i, 15).present?
        writer.puts('t')
      else
        errors << '17: Could not find T > TRANSFER files.'
      end

      if reader.expect(/Enter name of local file/i, 15).present?
        writer.puts('')
      else
        errors << '18: Could not find file name entry prompt.'
      end

      if reader.expect(/C > CONTINUE/i, 15).present?
        writer.puts('c')
      else
        errors << '19: Could not find C > CONTINUE.'
      end

      if reader.expect(/Q > QUIT/i, 15).present?
        writer.puts('q')
      else
        errors << '20: Could not find Q > QUIT.'
      end

      if reader.expect(/Press <SPACE> to continue/i, 15).present?
        writer.puts(' ')
      else
        errors << '21: Could not quit file transfer.'
      end

      if reader.expect(/P > POST payment data/i, 15).present?
        writer.puts('p')
      else
        errors << '22: Could not find P > POST payment data.'
      end

      if reader.expect(/Post which file/i, 15).present?
        writer.puts('1')
      else
        errors << '23: Could not find choose post file prompt.'
      end

      if reader.expect(/C > CHECK validity of each entry/i, 60).present?
        writer.puts('c')
      else
        errors << '24: Could not find C > CHECK validity of each entry.'
      end

      if reader.expect(/Press <SPACE> to continue/i, 120).present?
        writer.puts(' ')
      else
        errors << '25: Could not check the validity.'
      end

      if reader.expect(/U > UPDATE patron records/i, 600).present?
        writer.puts('u')
      else
        errors << '26: Could not find U > UPDATE patron records.'
      end

      if reader.expect(/Press <SPACE> to continue/i, 600).present?
        writer.puts(' ')
      else
        errors << '27: Could not update patron records.'
      end

      if reader.expect(/Q > QUIT/i, 15).present?
        writer.puts('q')
      else
        errors << '28: Could not find Q > QUIT.'
      end

      if reader.expect(/Press <SPACE> to continue/i, 15).present?
        writer.puts(' ')
      else
        errors << '29: Could not quit patron update.'
      end

      if reader.expect(/R > REMOVE files/i, 15).present?
        writer.print('r')
      else
        errors << '30: Could not find R > REMOVE files.'
      end

      if reader.expect(/Input numbers of files to be removed/i, 15).present?
        writer.puts('1')
      else
        errors << '31: Could not find remove file number prompt.'
      end

      if reader.expect(/Remove file bursar.in.bux?/i, 15).present?
        writer.print('y')
      else
        errors << '32: Could not remove file.'
      end

      if reader.expect(/Q > QUIT/i, 15).present?
        writer.puts('q')
      else
        errors << '33: Could not find Q > QUIT.'
      end
    end

    if Time.zone.now - start_time > 15.minutes
      errors << '34: Warning - Bursar In took unusually long to complete (> 15 minutes)'
    end

    BursarMailer.error_notification('Bursar In', errors).deliver_later if errors.present?
  end
end
