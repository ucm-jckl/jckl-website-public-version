class SierraConnection
  # Class variables
  HOST      = 'quest-db.searchmobius.org'.freeze
  PORT      = 1032
  SSLMODE   = 'require'.freeze
  DBNAME    = 'xxxxx'.freeze
  USER      = 'xxxxx'.freeze
  PASSWORD  = Rails.application.secrets.sierra_password

  # Instance variables
  @connection

  def initialize
    connect
  end

  # Create the connection
  def connect
    @connection = PG.connect(
      host: HOST,
      port: PORT,
      sslmode: SSLMODE,
      dbname: DBNAME,
      user: USER,
      password: PASSWORD
    )
  rescue PG::ConnectionBad => e
    SierraMailer.connection_error_notification(e)
  end

  # Close the connection
  def disconnect
    @connection.close
  end

  # Execute a SELECT query
  def select(query)
    result = @connection.exec(query)
    result
  end
end
