class GoogleApiService
  require 'googleauth'
  require 'google/apis'
  require 'google/apis/calendar_v3'

  APPLICATION_NAME = 'JCKL Web Google Calendar API Service'.freeze

  def get_gcal_events(calendar_id)
    response = gcal_client.list_events(calendar_id,
                                       max_results: 1000,
                                       single_events: true,
                                       order_by: 'startTime',
                                       time_min: Time.now.iso8601)
    response.items
  end

  def add_gcal_event(calendar_id, summary, start_datetime, end_datetime)
    event = Google::Apis::CalendarV3::Event.new(summary: summary,
                                                start: {
                                                  time_zone: 'America/Chicago'
                                                },
                                                end: {
                                                  time_zone: 'America/Chicago'
                                                })

    if start_datetime.class.name == 'Time' && end_datetime.class.name == 'Time'
      puts event.start
      event.start[:date_time] = start_datetime.iso8601
      event.end[:date_time] = end_datetime.iso8601
    elsif start_datetime.class.name == 'Date' && end_datetime.class.name == 'Date'
      event.start[:date] = start_datetime.iso8601
      event.end[:date] = (end_datetime + 1.day).iso8601
    else
      return false
    end

    gcal_client.insert_event(calendar_id, event)
  end

  def edit_gcal_event(calendar_id, event_id, summary, start_datetime, end_datetime)
    event = gcal_client.get_event(calendar_id, event_id)
    event.summary = summary
    if start_datetime.class.name == 'Time' && end_datetime.class.name == 'Time'
      event.start.date_time = start_datetime.iso8601
      event.end.date_time = end_datetime.iso8601
    elsif start_datetime.class.name == 'Date' && end_datetime.class.name == 'Date'
      event.start.date = start_datetime.iso8601
      event.end.date = (end_datetime + 1.day).iso8601
    else
      return false
    end

    result = gcal_client.update_event(calendar_id, event_id, event)
  end

  def delete_gcal_event(calendar_id, event_id)
    gcal_client.delete_event(calendar_id, event_id)
  end

  private

  # Set up authorization
  def authorize
    if @authorization.blank?
      file = Tempfile.new('google_application_credentials')
      begin
        # Set up a temp .json file containing credentials since Googleauth requires it in a file rather than secrets.yml
        file.write(Rails.application.secrets.google_application_credentials.to_json.to_s)
        ENV['GOOGLE_APPLICATION_CREDENTIALS'] = file.path
        file.close
        # Create authorization
        scopes = ['https://www.googleapis.com/auth/cloud-platform', 'https://www.googleapis.com/auth/devstorage.read_only', Google::Apis::CalendarV3::AUTH_CALENDAR]
        @authorization = Google::Auth.get_application_default(scopes)
      ensure
        file.close
        file.unlink
      end
    end
    @authorization
  end

  # Create/get the Google Calendar API Service
  def gcal_client
    if @gcal_client.blank?
      @gcal_client = Google::Apis::CalendarV3::CalendarService.new
      @gcal_client.client_options.application_name = APPLICATION_NAME
      @gcal_client.authorization = authorize
    end
    @gcal_client
  end
end
