<% if @absence.errors.any? %>
  <div class="form-errors alert alert-danger">
    <h2>Please correct the following errors:</h2>
    <ul>
      <% @absence.errors.full_messages.each do |error| %>
        <li><%= error %></li>
      <% end %>
    </ul>
  </div>
<% end %>
<%= semantic_form_for(
      @absence,
      url: if @absence.new_record? then employee_absences_url else employee_absence_url( employee_name: current_user.employee.url_slug, id: @absence.id ) end,
      method: if @absence.new_record? then :post else :patch end
    ) do |f| %>
  <%= f.inputs do %>
    <% if current_user.employee.library_administrator? %>
      <%= f.input :employee, as: :select, collection: LibraryEmployee.where( "hide_from_public_views = false or hide_from_public_views is null" ).order( "last_name ASC, first_name ASC" ).pluck( "concat(last_name,\", \", first_name )", :id ) %>
    <%  end %>
    <%= f.input :start_date, as: :string, input_html: { class: "anytime-date" } %>
    <%= f.input :end_date, as: :string, input_html: { class: "anytime-date" } %>
    <%= f.input :schedule, as: :string, label: "Schedule", input_html: { placeholder: "e.g. \"Out all day\" or \"Working 1-5\"" } %>
    <%= f.input :leave_hours_used, as: :number, label: "Leave Time Used - Hours" %>
    <%= f.input :leave_minutes_used, as: :number, label: "Leave Time Used - Minutes" %>
    <%= f.input :absence_type, as: :select, label: "Absence Type" %>
    <%= f.input :background_information, as: :text, label: "Additional information",input_html: { rows: 4 } %>
    <%= f.input :professional_funding, as: :string %>
  <% end %>
  <%= f.actions do %>
    <%= f.action :submit, as: :button,
        label: if @absence.new_record? then "Submit" else "Save" end,
        button_html: {class: 'btn btn-primary'} %>
    <% if !@absence.new_record? %>
      <%= link_to "Delete", employee_absence_url( employee_name: @absence.employee.url_slug, id: @absence.id ),
            class: "btn btn-secondary confirmation-dialog",
            method: "delete" %>
      <% if current_user.employee.can_approve_absences_of?(@absence.employee) %>
        <%= if @absence.approved != true then
            link_to "Approve",
              approve_employee_absence_url( employee_name: @absence.employee.url_slug, id: @absence.id ),
              method: :pach, class: "btn btn-secondary"
            end  %>
        <%= if @absence.approved != false then
            link_to "Reject",
              reject_employee_absence_url( employee_name: @absence.employee.url_slug, id: @absence.id ),
              method: :patch, class: "btn btn-secondary"
            end %>
      <% end %>
      <% if @absence.note_required and @absence.note_received != true and current_user.employee.can_approve_absences_of?( @employee ) and @absence.new_record? == false %>
        <%= link_to "Mark Note As Received",
              receive_note_employee_absence_url( employee_name: @absence.employee.url_slug, id: @absence.id ),
              method: :patch, class: "btn btn-default"  %>
      <% end %>
    <% end %>
  <% end %>
<% end %>
