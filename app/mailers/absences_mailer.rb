class AbsencesMailer < ApplicationMailer
  def send_email(absence, verb)
    @absence = absence
    @verb = verb
    recipients = ADMIN_OFFICE_EMAIL
    recipients += [
      @absence.employee.email,
      @absence.submitter.email,
      @absence.employee.supervisor.andand.email
    ]
    subject = "Absence Request #{verb}: #{@absence.employee.first_last} - #{@absence.date_range}"
    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    ).deliver
  end
end
