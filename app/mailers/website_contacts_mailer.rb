class WebsiteContactsMailer < ApplicationMailer
  def send_email(contact_form, employee, office)
    @contact_form = contact_form
    @employee = employee
    @office = office

    recipients = [@contact_form.contact_email]
    if @contact_form.topic
      recipients += case @contact_form.topic
                    when 'general'
                      REFERENCE_EMAIL
                    when 'reference'
                      REFERENCE_EMAIL
                    when 'comments'
                      ADMIN_OFFICE_EMAIL
                    else
                      ADMIN_OFFICE_EMAIL
                    end
    end
    recipients << @employee.andand.email if @employee.present?
    recipients << @office.andand.email if @office.present?
    recipients << @office.andand.primary_employee.andand.email if @office.present?

    @contact_form.recipients = recipients.join(',')
    @contact_form.save

    @include_unmonitored_note = false if @contact_form.topic == 'reference'

    mail(
      to: recipients,
      reply_to: recipients,
      subject: "Online Contact Form Received: #{@contact_form.subject}"
    ).deliver
  end
end
