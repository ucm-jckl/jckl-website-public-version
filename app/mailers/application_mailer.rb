class ApplicationMailer < ActionMailer::Base
  default from: 'library@ucmo.edu'
  layout 'mailer'
  @include_unmonitored_note = true

  ADMIN_OFFICE_EMAIL = ['xxxxx@ucmo.edu'].freeze
  REFERENCE_EMAIL = ['xxxxx@ucmo.edu'].freeze
  ROOM_BOOKINGS_EMAIL = ['xxxxx@ucmo.edu'].freeze
  BOOK_REQUESTS_EMAIL = ['xxxxx@ucmo.edu'].freeze
end
