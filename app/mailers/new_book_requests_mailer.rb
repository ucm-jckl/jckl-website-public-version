class NewBookRequestsMailer < ApplicationMailer
  def send_email(user, request)
    @request = request
    @user = user

    recipients = BOOK_REQUESTS_EMAIL
    recipients += [@user.email, @request.subject_area.andand.subject_librarian.andand.email]
    subject = "Online Purchase Request: #{@request.title}"

    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    ).deliver
  end
end
