class BursarMailer < ActionMailer::Base
  def error_notification(action, error_messages)
    recipients = ['welker@ucmo.edu', 'fiegenbaum@ucmo.edu', 'agueros@ucmo.edu', 'horne-popp@ucmo.edu']
    @action = action
    @error_messages = error_messages

    mail(
      to: recipients,
      reply_to: recipients,
      subject: "#{@action} (#{Time.now.strftime('%Y-%m-%d')})"
    ).deliver
  end
end
