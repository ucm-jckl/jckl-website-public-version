class MeetingRoomsMailer < ApplicationMailer
  def send_email(booking)
    @booking = booking

    recipients = ROOM_BOOKINGS_EMAIL
    recipients += [@booking.contact_email, @booking.requestor_email]

    subject = "Room Booking Request - #{@booking.event_name} - #{@booking.start_time.to_formatted_s(:long_12hr)} to #{@booking.end_time.to_formatted_s(:long_12hr)}"
    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    ).deliver
  end
end
