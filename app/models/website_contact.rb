class WebsiteContact < ApplicationRecord
  validates :contact_name, :contact_email, :subject, :message, presence: true

  def email_friendly_created_at
    created_at.to_formatted_s(:long_12hr)
  end
end
