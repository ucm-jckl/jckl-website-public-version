# This is an abstract class to facilitate sharing methods and data between the various statistics models

class Statistic < ApplicationRecord
  self.abstract_class = true

  def self.date_format(time_interval)
    case time_interval
    when 'hours', 'hour_averages', 'weekday_hour_averages'
      group = '%Y-%m-%d (%a) %H:00'
    when 'days', 'weekday_averages'
      group = '%Y-%m-%d (%a)'
    when 'weeks'
      group = '%Y, Week %U'
    when 'months'
      group = '%Y-%m'
    when 'years'
      group = '%Y'
    end
    group
  end

  def self.year_and_week_to_date(week_string)
    year = week_string.split(',')[0].to_i
    week_number = week_string.split(',')[1].gsub(/[^0-9]/i, '').to_i
    (Chronic.parse("#{year}-01-01").beginning_of_year.beginning_of_week + week_number.weeks).beginning_of_week
  end
end
