class ComputerOperatingSystem < ApplicationRecord
  has_many :computer_builds, dependent: :nullify
end
