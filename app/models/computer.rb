class Computer < ApplicationRecord
  belongs_to :computer_build
  belongs_to :location
  has_many :computer_sessions
  has_many :computer_softwares, through: :computer_software_links
  has_many :computer_software_links, dependent: :destroy

  def last_session
    computer_sessions.order(:updated_at).last
  end

  def current_session
    if last_session.present?
      if last_session.current?
        return last_session
      else
        return ComputerSession.new(computer_id: id, ping_count: 0)
      end
    else
      return ComputerSession.new(computer_id: id, ping_count: 0)
    end
  end

  def available?
    current_session.new_record?
  end
end
