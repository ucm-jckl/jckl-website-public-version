class SpecialMessage < ApplicationRecord
  def self.unexpired
    #The created_at parameter is to help in test cases when changing the current time
    SpecialMessage.where('expiration_date >= ? and created_at <= ?', Time.now, Time.now)
  end
end
