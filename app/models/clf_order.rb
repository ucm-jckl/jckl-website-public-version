class ClfOrder < ApplicationRecord
  has_many :clf_books, through: :clf_book_order_links
  has_many :clf_book_order_links, dependent: :destroy
end
