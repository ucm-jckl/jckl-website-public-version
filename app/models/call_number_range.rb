class CallNumberRange < ApplicationRecord
  belongs_to :subject_area

  def range_summary
    "#{range_name + ': ' if range_name.present?}#{range_start} - #{range_end}"
  end
end
