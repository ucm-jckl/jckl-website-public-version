class Department < ApplicationRecord
  belongs_to :parent_department, class_name: 'Department', foreign_key: 'parent_department_id'
  belongs_to :department_head, class_name: 'Employee', foreign_key: 'department_head_id'
  has_many :employees, dependent: :nullify
  has_many :child_departments, class_name: 'Department', foreign_key: 'parent_department_id', dependent: :nullify
  has_many :offices, dependent: :nullify
end
