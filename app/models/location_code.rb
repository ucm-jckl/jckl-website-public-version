class LocationCode < ApplicationRecord
  has_many :new_books, dependent: :nullify
  has_many :dvds, dependent: :nullify
end
