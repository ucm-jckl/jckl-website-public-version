class ComputerSoftwareLink < ApplicationRecord
  belongs_to :computer
  belongs_to :computer_software
end
