class ClfBook < ApplicationRecord
  has_many :clf_orders, through: :clf_book_order_links
  has_many :clf_book_order_links, dependent: :destroy
end
