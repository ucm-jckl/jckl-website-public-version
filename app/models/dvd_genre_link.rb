class DvdGenreLink < ApplicationRecord
  belongs_to :dvd
  belongs_to :dvd_genre
end
