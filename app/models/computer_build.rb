class ComputerBuild < ApplicationRecord
  belongs_to :computer_operating_system
  has_many :computers, dependent: :nullify
  has_many :computer_softwares, through: :computer_build_software_links
  has_many :computer_build_software_links, dependent: :destroy
end
