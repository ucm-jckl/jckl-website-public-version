class StudyRoom < ApplicationRecord
  has_many :study_room_bookings, dependent: :nullify
end
