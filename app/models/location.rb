class Location < ApplicationRecord
  has_many :computers, dependent: :nullify
  has_many :headcount_entries, dependent: :nullify
end
