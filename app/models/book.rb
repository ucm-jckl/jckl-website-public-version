# This is an abstract class to facilitate sharing methods and data between the New Books, DVDs, and CLF Books models

class Book < ApplicationRecord
  self.abstract_class = true

  @@image_not_available_url   = 'images/image-not-available.png'

  @@library_catalog_base_url  = 'https://quest.searchmobius.org'
  @@library_catalog_cluster   = 'S2'
  @@location_code_like = 'c%'

  @@syndetics_base_url        = 'http://www.syndetics.com'
  @@syndetics_client_id       = 'ucmmob'
  @@google_books_base_url_1     = /^http:\/\/bks[0-9]\.books\.google\.com/
  @@google_books_base_url_2     = /^http:\/\/books\.google\.com/
  @@google_books_api_base_url = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'
  @@google_api_key = Rails.application.secrets.google_api_key

  @@rss_channel_title = 'New Arrivals'
  @@rss_channel_author = 'James C. Kirkpatrick Library'
  @@rss_channel_about_url = 'https://library.ucmo.edu'

  def title=(val)
    write_attribute(:title, val.gsub('[electronic resource]', '').squish)
  end

  def author=(val)
    write_attribute(:author, val.gsub('by ', '').squish)
  end

  def call_number=(val)
    write_attribute(:call_number, val.squish)
  end

  def quest_url
    @@library_catalog_base_url + '/record=' + read_attribute(:record_number).to_s + '~' + @@library_catalog_cluster
  end

  # get the absolute url of a cover image or a filler image
  def full_image_url(host = '', force_https = false)
    # if there is no URL, use the local "image not available" image
    if image_url.blank?
      if force_https
        return "https://#{host}/assets/#{@@image_not_available_url}"
      else
        return "//#{host}/assets/#{@@image_not_available_url}"
      end
    else
      # if the image URL is from Syndetics and this is the production server, replace Syndetics with the URL of the local reverse proxy
      if image_url.start_with?(@@syndetics_base_url) && Rails.env.production?
        if force_https
          return image_url.sub(@@syndetics_base_url, "https://#{host}/syndetics")
        else
          return image_url.sub(@@syndetics_base_url, "//#{host}/syndetics")
        end
      # if the image URL is from Google Books and this is the production server, replace Google Books with the URL of the local reverse proxy
      elsif image_url =~ @@google_books_base_url_1
        google_books_subdomain_number = image_url.sub('http://bks', '').sub(/\.books\.google\.com\/.*/, '')
        if force_https
          return image_url.sub(@@google_books_base_url_1, "https://#{host}/google-books#{google_books_subdomain_number}")
        else
          return image_url.sub(@@google_books_base_url_1, "//#{host}/google-books#{google_books_subdomain_number}")
        end
      elsif image_url =~ @@google_books_base_url_2
        if force_https
          return image_url.sub('http://', 'https://')
        else
          return image_url
        end
      # otherwise, return the regular image URL (Syndetics in dev environment and Google Books in all environments)
      else
        return image_url
      end
    end
  end

  # Get metadata from the SQL result record set and mash it up with metadata from Syndetics or Google Books
  def fetch_metadata(isbn)
    metadata = scrape_syndetics(isbn)
    metadata_is_missing = [:image, :summary, :info_page].any? { |key| metadata[key].blank? }
    metadata = query_google_books_api(isbn, metadata) if metadata_is_missing

    self.image_url ||= metadata[:image]
    self.summary ||= metadata[:summary]
    self.info_page_url ||= metadata[:info_page]
    save
  end

  # Screen scrape Syndetics for metadata
  def scrape_syndetics(isn, metadata = {})
    require 'nokogiri'
    require 'open-uri'

    book_url  = @@syndetics_base_url + '?isbn=' + isn + '/summary.html&client=' + @@syndetics_client_id
    av_url    = @@syndetics_base_url + '?isbn=' + isn + '/avsummary.html&client=' + @@syndetics_client_id

    # Open the book and AV summary pages.
    book_page = Nokogiri::HTML(open(book_url))
    av_page = Nokogiri::HTML(open(av_url))

    # Search the book and AV pages for DOM elements.
    if metadata[:title].blank?
      if book_page.at_css('.header_title').present?
        metadata[:info_page] = book_url
      elsif book_page.at_css('.header_title_r1').present?
        metadata[:info_page] = book_url
      elsif av_page.at_css('.header_title').present?
        metadata[:info_page] = av_url
      elsif av_page.at_css('.header_title_r1').present?
        metadata[:info_page] = book_url
      end
      end

    if metadata[:summary].blank?
      if book_page.at_css('.element_body').present?
        book_page.at_css('.element_body').children.each do |child|
          metadata[:summary] = child.text.strip if child.text?
        end
      elsif av_page.at_css('.element_body').present?
        av_page.at_css('.element_body').children.each do |child|
          metadata[:summary] = child.text.strip if child.text?
        end
      end
    end

    if metadata[:author].blank?
      if book_page.at_css('.author_heading').present?
        metadata[:author] = book_page.at_css('.author_heading').text.strip
      elsif av_page.at_css('.author_heading').present?
        metadata[:author] = av_page.at_css('.author_heading').text.strip
      end
    end

    if metadata[:image].blank?
      image_url = nil
      if book_page.at_css('.book_cover img').present?
        image_url = @@syndetics_base_url + '/' + book_page.at_css('.book_cover img')[:src]
      elsif av_page.at_css('.book_cover img').present?
        image_url = @@syndetics_base_url + '/' + av_page.at_css('.book_cover img')[:src]
      end

      if image_url
        image_dimensions = FastImage.size(image_url)
        if image_dimensions.present?
          if image_dimensions[0] > 1 && image_dimensions[1] > 1
            metadata[:image] = image_url
          end
        end
      end
    end

    metadata
  end

  # Query Google Books API for metadata
  def query_google_books_api(isn, metadata = {})
    require 'net/http'

    # Fetch data from Google Books API
    google_books_url = @@google_books_api_base_url + isn + '&key=' + @@google_api_key
    json_data = Net::HTTP.get_response(URI.parse(google_books_url)).body
    data = JSON.parse(json_data)

    # Loop through the resulting data and try to fill our missing metadata
    if data['items'].present?
      data['items'].each do |item|
        if metadata[:info_page].blank? && item.key?('volumeInfo')
          metadata[:info_page] = item['volumeInfo']['infoLink']
        end

        if metadata[:author].blank? && item.key?('volumeInfo')
          if item['volumeInfo'].key?('authors')
            metadata[:author] = item['volumeInfo']['authors'].join(', ')
          end
        end

        if metadata[:summary].blank? && item.key?('volumeInfo')
          if item['volumeInfo'].key?('description')
            metadata[:summary] = item['volumeInfo']['description']
          end
        end

        next unless metadata[:image].blank? && item.key?('volumeInfo')
        next unless item['volumeInfo'].key?('imageLinks')
        if item['volumeInfo']['imageLinks'].key?('thumbnail')
          metadata[:image] = item['volumeInfo']['imageLinks']['thumbnail']
        end
      end
    end

    metadata
  end

  def normalize_call_number
    if call_number
      original_call_number = call_number.gsub(/\|[a-zA-Z]/, '').tr('-', ' ')
      self.call_number = call_number.gsub(/\|[a-zA-Z]/, '').tr('-', ' ')
                                    .gsub(/JE/i, '').gsub(/Ref/i, '')
                                    .gsub(/Big Books/i, '').gsub(/Curr/i, '')
      normalized_call_number = Lcsort.normalize(call_number)

      self.call_number = normalized_call_number if normalized_call_number

      if original_call_number.upcase.start_with?('JE')
        self.call_number = 'JE ' + call_number
      end

      if original_call_number.upcase.start_with?('REF')
        self.call_number = 'REF ' + call_number
      end

      if original_call_number.upcase.start_with?('CURR')
        self.call_number = 'CURR ' + call_number
      end

      if original_call_number.upcase.start_with?('BIG BOOKS')
        self.call_number = 'BIG BOOKS ' + call_number
      end

      save
    end
  end

  def plain_call_number
    call_number.andand.sub('je', '').sub('ref', '').sub('curr', '').sub('big books', '')
  end
end
