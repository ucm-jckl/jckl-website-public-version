class DvdGenre < ApplicationRecord
  has_many :dvds, through: :dvd_genre_links
  has_many :dvd_genre_links, dependent: :destroy
end
