class HeadcountForm
  include ActiveModel::Model

  attr_accessor :headcount_date, :headcounts
  validates :headcount_date, presence: true

  # get the headcounts for a certain date for all locations
  def headcounts_on_date
    date = Chronic.parse(headcount_date).to_date
    if date.blank?
      return nil
    else
      hours = {
        '8am' => {},
        '9am' => {},
        '10am' => {},
        '11am' => {},
        '12pm' => {},
        '1pm' => {},
        '2pm' => {},
        '3pm' => {},
        '4pm' => {},
        '5pm' => {},
        '6pm' => {},
        '7pm' => {},
        '8pm' => {},
        '9pm' => {},
        '10pm' => {},
        '11pm' => {},
        '12am' => {},
        '1am' => {},
        '1:30am' => {}
      }

      locations = Location
                  .where('(stat_form_start_date is null or ? > stat_form_start_date) and (stat_form_end_date is null or ? < stat_form_end_date)', date, date)
                  .order(:floor, :sort)

      locations.each do |location|
        hours.each do |hour, _locations_hash|
          hours[hour][location.location_name] = 0
        end
      end

      start_time = Chronic.parse(date.to_s).beginning_of_day + LibraryHour::DATE_CUTOVER_INTERVAL
      end_time = start_time.end_of_day + LibraryHour::DATE_CUTOVER_INTERVAL
      headcounts = HeadcountEntry.includes(:location).where('count_time >= ? and count_time < ?',
                                                            start_time, end_time)

      headcounts.each do |headcount|
        hour = headcount.count_time.strftime('%l').strip
        hour += ':30' if headcount.count_time.strftime('%M') == '30'
        hour += headcount.count_time.strftime('%P')

        if hours[hour].andand[headcount.location.location_name]
          hours[hour][headcount.location.location_name] = headcount.headcount
        end
      end

      return hours
    end
  end

  # save the headcount form as a bunch of individual headcount records
  def save(clear = false)
    start_time = headcount_date.to_time.beginning_of_day + LibraryHour::DATE_CUTOVER_INTERVAL
    end_time = start_time.end_of_day + LibraryHour::DATE_CUTOVER_INTERVAL

    # delete existing headcount records
    HeadcountEntry.where('count_time >= ? and count_time < ?',
                         start_time, end_time).destroy_all

    # create new headcount records and bulk insert
    new_headcounts = []
    headcounts.each do |hour_name, location_counts|
      location_counts.each do |location_name, count|
        time = Chronic.parse(headcount_date.to_s + ' ' + hour_name)
        time += 1.day if time < start_time

        location = Location.find_by(location_name: location_name)
        headcount = HeadcountEntry.new
        headcount.location_id = location.id
        headcount.headcount = count
        headcount.headcount = 0 if clear
        headcount.count_time = time
        new_headcounts << headcount
      end
    end
    HeadcountEntry.import(new_headcounts)
  end
end
