class StudyRoomBooking < HighchartsDataSource
  belongs_to :study_room

  def self.to_highcharts(start_time, end_time, interval, grouping, aggregation, metric)
    sql = nil

    # Get all study room stats grouped by room
    if grouping == 'rooms'
      sql = "select r.display_name as series, DATE_FORMAT(CONVERT_TZ(b.booking_time, \'+0:00\', \'-5:00\'), '#{date_format(interval)}') as time_period, sum(b.booking_duration_minutes) as count from study_room_bookings b "
      sql += 'left join study_rooms r on b.study_room_id=r.id '

    else
      # OR Get all study room stats as a whole
      sql = "select 'JCKL' as series, DATE_FORMAT(CONVERT_TZ(booking_time, \'+0:00\', \'-5:00\'), '#{date_format(interval)}') as time_period, sum(booking_duration_minutes) as count from study_room_bookings "
    end

    sql += "where (booking_time >= '#{start_time}' and booking_time <= '#{end_time}') "
    sql += 'group by series, time_period '
    sql += 'order by series, time_period asc'

    data = ActiveRecord::Base.connection.execute(sql)

    if metric == 'percent'
      percent_data = []
      data.each_with_index do |row, _i|
        time = row[1]
        minutes_used = row[2]
        case interval
        when 'hours', 'hour_averages', 'weekday_hour_averages'
          start_time = Chronic.parse(time).beginning_of_hour
          end_time = Chronic.parse(time).end_of_hour
        when 'days', 'weekday_averages'
          start_time = Chronic.parse(time).beginning_of_day
          end_time = Chronic.parse(time).end_of_day
        when 'weeks'
          start_time = year_and_week_to_date(time).beginning_of_week
          end_time = year_and_week_to_date(time).end_of_week
        when 'months'
          start_time = Chronic.parse(time).beginning_of_month
          end_time = Chronic.parse(time).end_of_month
        when 'years'
          start_time = Chronic.parse("#{time}-01-01").beginning_of_year
          end_time = Chronic.parse("#{time}-01-01").end_of_year
        end

        minutes_open = LibraryHour.minutes_open_between(start_time, end_time)
        minutes_open *= StudyRoom.where(active: true).length if grouping == 'all'

        percentage = (minutes_used.to_f / minutes_open).round(2) * 100
        percentage = 0 unless percentage.finite?

        percent_data << [row[0], row[1], percentage]
      end
      data = percent_data
    end

    transform_to_highcharts_series(start_time, end_time, interval, data, aggregation)
  end
end
