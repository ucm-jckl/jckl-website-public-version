class ComputerSoftware < ApplicationRecord
  has_many :computers, through: :computer_software_links
  has_many :computer_software_links, dependent: :destroy
  has_many :computer_builds, through: :computer_build_software_links
  has_many :computer_build_software_links, dependent: :destroy
end
