class Absence < ApplicationRecord
  belongs_to :employee
  belongs_to :submitter, foreign_key: 'submitter_id', class_name: 'Employee'
  belongs_to :absence_type

  validates :start_date, :end_date, :schedule, :absence_type, :schedule, presence: true

  validates_each :start_date, :end_date do |record, field, value|
    date_check = Chronic.parse(value.to_s)
    record.errors.add(field, 'must be a valid date.') if date_check.blank?
  end

  SICK_HOURS_BEFORE_DOCTOR_NOTE = 8
  FAMILY_HOURS_BEFORE_DOCTOR_NOTE = 12
  GOOGLE_CALENDAR_ID = 'ucmo.edu_g3v7r53k657u2ajn6d2v2j0j94@group.calendar.google.com'.freeze

  attr_accessor :leave_hours_used
  attr_accessor :leave_minutes_used

  before_save :format_date_fields,
              :set_absence_duration,
              :determine_if_note_needed

  before_destroy :delete_from_google_calendar, if: proc { |_absence| !Rails.env.test? }
  before_create :add_to_google_calendar, if: proc { |_absence| !Rails.env.test? }
  after_update :update_google_calendar, if: proc { |_absence| !Rails.env.test? }
  after_find :set_leave_hours_and_minutes

  def duration
    ChronicDuration.output((duration_minutes || 0) * 60, limit_to_hours: true)
  end

  def date_range
    if start_date == end_date
      start_date.to_formatted_s(:long)
    else
      "#{start_date.to_formatted_s(:long)} to #{end_date.to_formatted_s(:long)}"
    end
  end

  private

  def format_date_fields
    if start_date.class.name == 'String'
      self.start_date = Chronic.parse(start_date)
    end

    self.end_date = Chronic.parse(end_date) if end_date.class.name == 'String'
  end

  def set_leave_hours_and_minutes
    self.leave_hours_used = (duration_minutes / 60 if duration_minutes.present?)

    self.leave_minutes_used = (duration_minutes % 60 if duration_minutes.present?)
  end

  def set_absence_duration
    self.duration_minutes = (@leave_hours_used.to_i * 60) + @leave_minutes_used.to_i
  end

  def determine_if_note_needed
    past_minutes_used = employee.andand.leave_time_used(absence_type.absence_type_name, false)
    new_minutes_used = leave_minutes_used.to_i + (leave_hours_used.to_i * 60)
    total_minutes_used = past_minutes_used + new_minutes_used

    if absence_type.absence_type_name == 'sickself' \
      && total_minutes_used > SICK_HOURS_BEFORE_DOCTOR_NOTE * 60

      self.note_required = true

    elsif absence_type.absence_type_name == 'sickfamily' \
      && total_minutes_used > FAMILY_HOURS_BEFORE_DOCTOR_NOTE * 60

      self.note_required = true

    else
      self.note_required = false
    end

    true
  end

  def add_to_google_calendar
    s = GoogleApiService.new
    event = s.add_gcal_event(GOOGLE_CALENDAR_ID, schedule, start_date, end_date)
    self.google_calendar_event_id = event.id
  end

  def update_google_calendar
    s = GoogleApiService.new
    s.edit_gcal_event(GOOGLE_CALENDAR_ID, google_calendar_event_id, schedule, start_date, end_date)
  end

  def delete_from_google_calendar
    s = GoogleApiService.new
    s.delete_gcal_event(GOOGLE_CALENDAR_ID, google_calendar_event_id)
  end
end
