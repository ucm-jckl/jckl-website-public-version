class ComputerSession < ApplicationRecord
  belongs_to :computer

  SESSION_TIMEOUT = 3.minutes

  def current?
    Time.zone.now - updated_at <= SESSION_TIMEOUT
  end
end
