class ShelfColumn < ApplicationRecord
  has_many :shelf_column_inventory_dates, dependent: :nullify
end
