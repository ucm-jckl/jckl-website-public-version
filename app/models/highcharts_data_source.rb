
class HighchartsDataSource < Statistic
  self.abstract_class = true

  INTERVALS = {
    days: 'Days',
    weeks: 'Weeks',
    months: 'Months',
    years: 'Years',
    hour_averages: 'Hourly Averages',
    weekday_averages: 'Weekday Averages',
    weekday_hour_averages: 'Weekday and Hourly Averages'
  }.freeze

  def self.transform_to_highcharts_series(start_time, end_time, interval, data, aggregation)
    highchart = create_empty_highchart
    highchart[:series] = serialize_data(data, start_time, end_time, interval)

    if aggregation == 'avg'
      highchart[:xAxis][:type] = 'category'
      highchart[:xAxis][:categories] = get_x_axis_categories(start_time, end_time, interval)
      highchart[:series] = aggregate_time_series(highchart, interval)
    end

    highchart
  end

  def self.create_empty_highchart
    {
      xAxis: {
        type: 'datetime'
      },
      series: []
    }
  end

  # Create a hash structure all_series[series_name][time][count]
  # Note: for this to work, the data var must be an array of arrays in
  # the format [series_name, time, count]
  def self.serialize_data(data, start_time, end_time, interval)
    all_series = {}
    highcharts_series = []
    data.each do |record|
      all_series[record[0]] = {} if all_series.key?(record[0]) == false
      all_series[record[0]][record[1]] = record[2]
    end

    time_points = get_time_points(start_time, end_time, interval, false)

    all_series.each do |series_name, series|
      data = {}

      # Fill in zero values as placeholders first
      time_points.each do |time_point|
        time_string = time_point.strftime(date_format(interval))
        data[time_string] = {
          x: time_string_to_milliseconds(time_string, interval),
          y: 0
        }
      end

      # Add actual values
      series.each do |time_string, value|
        x_value = time_string_to_milliseconds(time_string, interval)
        data[time_string] = { x: x_value, y: value }
      end

      highcharts_series << {
        name: series_name,
        data: data.values
      }
    end

    highcharts_series
  end

  def self.get_x_axis_categories(_start_time, _end_time, interval)
    x_categories = []

    if interval == 'hour_averages'
      x_categories = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00',
                      '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00',
                      '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00']
    elsif interval == 'weekday_averages'
      x_categories = %w(Sun Mon Tue Wed Thu Fri Sat)
    elsif interval == 'weekday_hour_averages'
      x_categories = [
        'Sun 00:00', 'Sun 01:00', 'Sun 02:00', 'Sun 03:00', 'Sun 04:00', 'Sun 05:00',
        'Sun 06:00', 'Sun 07:00', 'Sun 08:00', 'Sun 09:00', 'Sun 10:00', 'Sun 11:00',
        'Sun 12:00', 'Sun 13:00', 'Sun 14:00', 'Sun 15:00', 'Sun 16:00', 'Sun 17:00',
        'Sun 18:00', 'Sun 19:00', 'Sun 20:00', 'Sun 21:00', 'Sun 22:00', 'Sun 23:00',

        'Mon 00:00', 'Mon 01:00', 'Mon 02:00', 'Mon 03:00', 'Mon 04:00', 'Mon 05:00',
        'Mon 06:00', 'Mon 07:00', 'Mon 08:00', 'Mon 09:00', 'Mon 10:00', 'Mon 11:00',
        'Mon 12:00', 'Mon 13:00', 'Mon 14:00', 'Mon 15:00', 'Mon 16:00', 'Mon 17:00',
        'Mon 18:00', 'Mon 19:00', 'Mon 20:00', 'Mon 21:00', 'Mon 22:00', 'Mon 23:00',

        'Tue 00:00', 'Tue 01:00', 'Tue 02:00', 'Tue 03:00', 'Tue 04:00', 'Tue 05:00',
        'Tue 06:00', 'Tue 07:00', 'Tue 08:00', 'Tue 09:00', 'Tue 10:00', 'Tue 11:00',
        'Tue 12:00', 'Tue 13:00', 'Tue 14:00', 'Tue 15:00', 'Tue 16:00', 'Tue 17:00',
        'Tue 18:00', 'Tue 19:00', 'Tue 20:00', 'Tue 21:00', 'Tue 22:00', 'Tue 23:00',

        'Wed 00:00', 'Wed 01:00', 'Wed 02:00', 'Wed 03:00', 'Wed 04:00', 'Wed 05:00',
        'Wed 06:00', 'Wed 07:00', 'Wed 08:00', 'Wed 09:00', 'Wed 10:00', 'Wed 11:00',
        'Wed 12:00', 'Wed 13:00', 'Wed 14:00', 'Wed 15:00', 'Wed 16:00', 'Wed 17:00',
        'Wed 18:00', 'Wed 19:00', 'Wed 20:00', 'Wed 21:00', 'Wed 22:00', 'Wed 23:00',

        'Thu 00:00', 'Thu 01:00', 'Thu 02:00', 'Thu 03:00', 'Thu 04:00', 'Thu 05:00',
        'Thu 06:00', 'Thu 07:00', 'Thu 08:00', 'Thu 09:00', 'Thu 10:00', 'Thu 11:00',
        'Thu 12:00', 'Thu 13:00', 'Thu 14:00', 'Thu 15:00', 'Thu 16:00', 'Thu 17:00',
        'Thu 18:00', 'Thu 19:00', 'Thu 20:00', 'Thu 21:00', 'Thu 22:00', 'Thu 23:00',

        'Fri 00:00', 'Fri 01:00', 'Fri 02:00', 'Fri 03:00', 'Fri 04:00', 'Fri 05:00',
        'Fri 06:00', 'Fri 07:00', 'Fri 08:00', 'Fri 09:00', 'Fri 10:00', 'Fri 11:00',
        'Fri 12:00', 'Fri 13:00', 'Fri 14:00', 'Fri 15:00', 'Fri 16:00', 'Fri 17:00',
        'Fri 18:00', 'Fri 19:00', 'Fri 20:00', 'Fri 21:00', 'Fri 22:00', 'Fri 23:00',

        'Sat 00:00', 'Sat 01:00', 'Sat 02:00', 'Sat 03:00', 'Sat 04:00', 'Sat 05:00',
        'Sat 06:00', 'Sat 07:00', 'Sat 08:00', 'Sat 09:00', 'Sat 10:00', 'Sat 11:00',
        'Sat 12:00', 'Sat 13:00', 'Sat 14:00', 'Sat 15:00', 'Sat 16:00', 'Sat 17:00',
        'Sat 18:00', 'Sat 19:00', 'Sat 20:00', 'Sat 21:00', 'Sat 22:00', 'Sat 23:00'
      ]
    end

    x_categories
  end

  def self.aggregate_time_series(highchart, interval)
    highchart[:series].each_with_index do |series, i|
      aggregated_data = {}
      highchart[:xAxis][:categories].each do |category|
        aggregated_data[category] = []
      end
      series[:data].each do |data|
        time = Time.at(data[:x] / 1000)
        aggregate_time = case interval
                         when 'hour_averages'
                           time.strftime('%H:%M')
                         when 'weekday_averages'
                           time.strftime('%a')
                         when 'weekday_hour_averages'
                           time.strftime('%a %H:%M')
        end

        aggregated_data[aggregate_time] << data[:y]
      end

      highchart[:series][i] = {
        name: series[:name],
        data: []
      }

      aggregated_data.each do |_time, values|
        average = (values.reduce(:+).to_f / values.length).round(2)
        average = 0 if average.nan?
        highchart[:series][i][:data] << average
      end
    end
  end

  def self.to_data_table(highchart, start_time, end_time, interval, aggregation)
    categories = highchart[:xAxis][:categories]
    categories = get_time_points(start_time, end_time, interval, true) if categories.blank?

    table = {}
    table[:headings] = []
    table[:rows] = {}

    categories.each do |category|
      table[:rows][category] = [0] * highchart[:series].length
    end

    highchart[:series].each_with_index do |series, i|
      table[:headings] << series[:name]

      series[:data].each_with_index do |data, j|
        if aggregation == 'avg'
          table[:rows][table[:rows].keys[j]][i] = data
        else
          time_label = Time.at(data[:x] / 1000).strftime(date_format(interval))
          if interval == 'weeks'
            time_label += Time.at(data[:x] / 1000).strftime(' (%b %d)')
          end
          table[:rows][time_label][i] = data[:y]
        end
      end
    end

    table
  end

  # Get a list of all dates/time x-axis points within a given period
  def self.get_time_points(start_time, end_time, interval, format_time)
    time_points = []
    time_marker = start_time
    time_marker = start_time.beginning_of_week if interval == 'weeks'

    if interval == 'hours'
      time_interval = 1.hour
      time_format = '%Y-%m-%d (%a) %H:00'
    elsif interval == 'days'
      time_interval = 1.day
      time_format = '%Y-%m-%d (%a)'
    elsif interval == 'weeks'
      time_interval = 1.week
      time_format = '%Y, Week %U (%b %d)'
    elsif interval == 'months'
      time_interval = 1.month
      time_format = '%Y-%m'
    elsif interval == 'years'
      time_interval = 1.year
      time_format = '%Y'
    end

    while time_marker <= end_time
      time_points << if format_time
                       time_marker.strftime(time_format)
                     else
                       time_marker
                     end
      time_marker += time_interval
    end

    time_points
  end

  def self.time_string_to_milliseconds(time_string, interval)
    if interval == 'weeks'
      year_and_week_to_date(time_string).to_i * 1000
    elsif interval == 'years'
      Chronic.parse("#{time_string}-01-01").to_i * 1000
    else
      Chronic.parse(time_string).to_i * 1000
    end
  end
end
