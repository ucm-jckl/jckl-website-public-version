class LibraryHour < ApplicationRecord
  # Set the # of hours after 12 AM that a new date for our operating hours starts
  DATE_CUTOVER_INTERVAL = 6.hours

  # Set LibCal API URLs
  LIBCAL_TODAY_URL = 'https://api3.libcal.com/api_hours_today.php?iid=3245&lid=1921&format=json'.freeze
  LIBCAL_WEEK_URL = 'https://api3.libcal.com/api_hours_grid.php?iid=3245&format=json&weeks=52'.freeze

  # Determine whether a given time is part of the previous day's overnight hours
  def self.is_overnight(time)
    time < (time.beginning_of_day + DATE_CUTOVER_INTERVAL)
  end

  # Get the date record for today
  def self.hours_today
    if is_overnight(Time.zone.now)
      return LibraryHour.find_by(calendar_date: Time.zone.now.to_date - 1.day)
    else
      return LibraryHour.find_by(calendar_date: Time.zone.now.to_date)
    end
  end

  # Get the number of weeks a given calendar date is from now
  def weeks_from_now
    this_week_start = Time.zone.now.beginning_of_week
    if self.class.is_overnight(Time.zone.now)
      this_week_start = (Time.zone.now.to_date - 1.day).beginning_of_week
    end

    ((calendar_date.to_time - this_week_start) / (60 * 60 * 24 * 7)).floor
  end

  # Get hours from LibCal
  def self.fetch_hours
    raw_data = Net::HTTP.get_response(URI.parse(self::LIBCAL_WEEK_URL)).body
    json_data = JSON.parse(raw_data)

    json_data['locations'][0]['weeks'].each do |week|
      week.each do |_weekday, date_data|
        hours_text = date_data['rendered']
        if hours_text.include?(' to ')

          open_time = Chronic.parse(date_data['date'] + ' ' + hours_text.split(' to ')[0])
          close_time = Chronic.parse(date_data['date'] + ' ' + hours_text.split(' to ')[1])

        elsif hours_text.include?('-')
          open_time = Chronic.parse(date_data['date'] + ' ' + hours_text.split('-')[0])
          close_time = Chronic.parse(date_data['date'] + ' ' + hours_text.split('-')[1])
        else

          open_time = nil
          close_time = nil
        end

        if close_time && open_time
          close_time += 1.day if close_time < open_time
        end

        hour = LibraryHour.find_by(calendar_date: Chronic.parse(date_data['date']).to_date)
        hour ||= LibraryHour.new
        hour.calendar_date = Chronic.parse(date_data['date']).to_date
        hour.open_time = open_time
        hour.close_time = close_time
        hour.hours_display_text = hours_text
        hour.save

        hour.set_minutes_each_hour
      end
    end
    # set minutes again to account for date crossover hours
    LibraryHour.all.each(&:set_minutes_each_hour)
  end

  # set the number of minutes the library is open on a given date
  def set_minutes_each_hour
    hour_interval = LibraryHour::DATE_CUTOVER_INTERVAL / (60 * 60)
    minutes_open_each_hour = []

    for i in 0..23
      today_hour_index = calendar_date.beginning_of_day + i.hours
      yesterday_hour_index = calendar_date.beginning_of_day + 1.day + i.hours
      minutes_open = 0

      hour_index = if i >= hour_interval
                     today_hour_index
                   else
                     yesterday_hour_index
      end

      minutes_open = if open_time.nil? || close_time.nil?
                       0
                     elsif hour_index.strftime('%Y-%m-%d %H') == open_time.strftime('%Y-%m-%d %H')
                       # partially open
                       ((hour_index + 1.hour) - open_time) / 60
                     elsif hour_index.strftime('%Y-%m-%d %H') == close_time.strftime('%Y-%m-%d %H')
                       # partially closed
                       (close_time - hour_index) / 60
                     elsif hour_index.strftime('%Y-%m-%d %H') > open_time.strftime('%Y-%m-%d %H') && hour_index.strftime('%Y-%m-%d %H') < close_time.strftime('%Y-%m-%d %H')
                       # full hour
                       60
                     else
                       0
      end
      minutes_open_each_hour << minutes_open || 0

    end

    minutes_open_each_hour.each_with_index do |hour, i|
      padding = i < 10 ? '0' : ''
      write_attribute("hour#{padding + i.to_s}minutes", hour)
    end

    self.total_calendar_date_minutes = minutes_open_each_hour.inject(:+)
    self.total_operating_date_minutes = minutes_open_each_hour
                                        .drop(hour_interval)
                                        .inject(:+)
    next_day = LibraryHour.find_by(calendar_date: (calendar_date + 1.day))
    if next_day
      self.total_operating_date_minutes += next_day.hours_array.first(hour_interval).inject(:+)
    end

    save
  end

  def hours_array
    [
      hour00minutes, hour01minutes, hour02minutes, hour03minutes, hour04minutes, hour05minutes,
      hour06minutes, hour07minutes, hour08minutes, hour09minutes, hour10minutes, hour11minutes,
      hour12minutes, hour13minutes, hour14minutes, hour15minutes, hour16minutes, hour17minutes,
      hour18minutes, hour19minutes, hour20minutes, hour21minutes, hour22minutes, hour23minutes
    ]
  end

  # get raw weekly hours from LibCal
  def self.hours_weekly
    Rails.cache.fetch('hours_weekly', expires_in: 6.hours) do
      json_data = Net::HTTP.get_response(URI.parse(self::LIBCAL_WEEK_URL)).body
      data = JSON.parse(json_data)
      return data['locations'][0]['weeks']
    end
  end

  # get the number of minutes the library is open over a given time period
  def self.minutes_open_between(start_time, end_time)
    total_minutes = 0

    dates = []
    date_marker = start_time.to_date
    while date_marker <= end_time.to_date
      dates << date_marker
      date_marker += 1.day
    end

    hours_records = LibraryHour.where(calendar_date: dates)
    hours_records.each do |record|
      %w(00 01 02 03 04 05 06 07 08 09 10 11
         12 13 14 15 16 17 18 19 20 21 22 23).each do |hour|
        minutes_open_this_hour = record.instance_eval("hour#{hour}minutes")
        hour_start = Chronic.parse("#{record.calendar_date} #{hour}:00:00")
        hour_end = Chronic.parse("#{record.calendar_date} #{hour}:59:59")

        if start_time <= hour_start && end_time >= hour_end
          total_minutes += minutes_open_this_hour
        end
      end
    end
    total_minutes
  end
end
