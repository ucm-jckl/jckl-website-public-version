class MeetingRoom < ApplicationRecord
  has_many :meeting_room_bookings, dependent: :nullify

  has_attached_file :photo,
                    styles: {
                      medium: '400x400'
                    }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: :photo, less_than: 10.megabytes

  GOOGLE_CALENDAR_EMBED_BASE_URL = 'https://www.google.com/calendar/embed?showCurrentTime=0showSubscribeButton=0&showTz=0&showPrint=0&showTabs=0&showCalendars=0&wkst=1&bgcolor=%23e2e2e2&ctz=America%2FChicago'.freeze
  GOOGLE_CALENDAR_PAGE_BASE_URL = 'http://www.google.com/calendar/embed?mode=WEEK&ctz=America/Chicago'.freeze
  GOOGLE_CALENDAR_AVAILABILITY_BASE_URL = "https://www.googleapis.com/calendar/v3/freeBusy?key=#{Rails.application.secrets.google_api_key}".freeze

  def google_calendar_week_embed
    "#{GOOGLE_CALENDAR_EMBED_BASE_URL}&src=#{google_calendar_id}&mode=WEEK"
  end

  def google_calendar_week_page
    "#{GOOGLE_CALENDAR_PAGE_BASE_URL}&src=#{google_calendar_id}&mode=WEEK"
  end

  def medium_photo_url
    photo.andand.url(:medium)
  end

  def self.check_availability(start_time, end_time)
    params = {
      timeMin: start_time,
      timeMax: end_time,
      timeZone: 'US/Chicago',
      items: []
    }
    MeetingRoom.all.each do |room|
      params[:items] << {
        id: room.google_calendar_id
      }
    end

    response = RestClient.post(GOOGLE_CALENDAR_AVAILABILITY_BASE_URL, params.to_json, content_type: :json, accept: :json)
    data = JSON.parse(response.body)

    available_calendars = []
    data['calendars'].each do |calendar_id, data|
      available_calendars << calendar_id if data['busy'].blank?
    end

    available_rooms = MeetingRoom.where(google_calendar_id: available_calendars)
  end
end
