class HeadcountEntry < HighchartsDataSource
  belongs_to :location

  def self.to_highcharts(start_time, end_time, interval, grouping, aggregation)
    sql = nil

    # Get all headcounts grouped by floor
    # NOTE: Any location whose name includes "hcc" is grouped into an HCC "floor" as requested by
    # by Horne-Popp and Tessone on 2016-02-04
    if grouping == 'floor'
      sql = "select if(l.location_name like '%hcc%','HCC',CONCAT('Floor ', l.floor)) as series, DATE_FORMAT(CONVERT_TZ(count_time, \'+0:00\', \'-5:00\'), '#{date_format(interval)}') as time_period, sum(headcount) as count from headcount_entries h "
      sql += 'left join locations l on h.location_id = l.id '
      sql += "where (count_time >= '#{start_time}' and count_time <= '#{end_time}') "

    # OR Get all headcounts grouped by location
    elsif grouping == 'location'
      sql = "select CONCAT('Floor ', l.floor, ': ', l.display_name) as series, DATE_FORMAT(CONVERT_TZ(count_time, \'+0:00\', \'-5:00\'), '#{date_format(interval)}') as time_period, sum(headcount) as count from headcount_entries h "
      sql += 'left join locations l on h.location_id = l.id '
      sql += "where (count_time >= '#{start_time}' and count_time <= '#{end_time}') "

    # OR Get all headcounts for the whole building
    else
      sql = "select 'JCKL' as series, DATE_FORMAT(CONVERT_TZ(count_time, \'+0:00\', \'-5:00\'), '#{date_format(interval)}') as time_period, sum(headcount) as count from headcount_entries "
      sql += "where (count_time >= '#{start_time}' and count_time <= '#{end_time}') "
    end

    sql += 'group by series, time_period '
    sql += 'order by series, time_period asc'

    data = ActiveRecord::Base.connection.execute(sql)
    transform_to_highcharts_series(start_time, end_time, interval, data, aggregation)
  end
end
