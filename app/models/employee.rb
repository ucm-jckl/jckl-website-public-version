class Employee < ApplicationRecord
  belongs_to :department
  belongs_to :supervisor, class_name: 'Employee', foreign_key: 'supervisor_id'
  has_many :headed_departments, class_name: 'Department', foreign_key: 'department_head_id', dependent: :nullify
  has_many :subordinates, class_name: 'Employee', foreign_key: 'supervisor_id', dependent: :nullify
  has_many :offices, foreign_key: 'primary_employee_id', dependent: :nullify
  has_many :absences, foreign_key: 'employee_id', dependent: :nullify

  def first_last
    "#{first_name} #{last_name}"
  end

  def last_first
    "#{last_name}, #{first_name}"
  end

  def title_and_rank
    if rank.blank?
      title
    else
      "#{title} - #{rank}"
    end
  end

  def url_slug
    "#{first_name.downcase.tr('-', '_')}-#{last_name.downcase.tr('-', '_')}"
  end

  def self.find_by_url_slug(slug)
    return nil if slug.blank?
    name = slug.gsub('-', '||').tr('_', '-').split('||')
    return nil unless name.length == 2
    first = name[0]
    last = name[1]
    Employee.find_by(first_name: first, last_name: last)
  end

  def library_administrator?
    department.andand.department_name == 'administration'
  end

  def can_view_absences_of?(employee)
    if employee.id == id || employee.supervisor.id == id || library_administrator?
      return true
    else
      return false
    end
  end

  def can_approve_absences_of?(employee)
    if employee.supervisor.id == id || library_administrator?
      return true
    else
      return false
    end
  end

  def leave_time_used(absence_type, string = true)
    absence_type_id = AbsenceType.find_by(absence_type_name: absence_type).id
    last_july_1 = Chronic.parse('last july 1')
    minutes_used = Absence.where(
      "employee_id = ? and absence_type_id = ? \
      and end_date >= ? and approved=1", id, absence_type_id, last_july_1
    ).sum(:duration_minutes)

    if string
      return ChronicDuration.output(minutes_used * 60,
                                    limit_to_hours: true,
                                    format: :long) || '0 hours'
    else
      return minutes_used
    end
  end
end
