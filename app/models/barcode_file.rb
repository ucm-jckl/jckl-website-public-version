class BarcodeFile < ApplicationRecord
  belongs_to :user

  validates :barcodes, :user_id, presence: true

  def to_array
    barcodes.andand.split(',') || []
  end

  def update_sierra
    SierraInventory.barcodes_to_circa(barcodes.to_array)
    processed = true
    save
  end
end
