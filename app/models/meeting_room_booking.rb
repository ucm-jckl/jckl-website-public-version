class MeetingRoomBooking < ApplicationRecord
  belongs_to :meeting_room
  validates :contact_name, :contact_email, :contact_phone, :contact_address, :contact_dept, :event_name, :attendance, presence: true
  validates :start_time, :end_time, presence: { message: ' can\'t be blank and must be a valid date and time.' }

  def format_date_fields
    if start_time.class.name == 'String'
      self.start_time = Chronic.parse(start_time)
    end

    self.end_time = Chronic.parse(end_time) if end_time.class.name == 'String'
  end
  before_save :format_date_fields
end
