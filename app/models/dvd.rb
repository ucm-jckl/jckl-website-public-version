class Dvd < Book
  has_many :dvd_genres, through: :dvd_genre_links, dependent: :destroy
  has_many :dvd_genre_links
  belongs_to :location_code

  @@tmdb_api_base_url = "http://api.themoviedb.org/3/"
  @@tmdb_api_key = Rails.application.secrets.tmdb_api_key
  @@tmdb_image_base_url = "https://image.tmdb.org/t/p/w185"

  @@imdb_base_url = "http://www.imdb.com/title/"

  @@image_not_available_url   = "images/image-not-available.png"

  def sort
    return @sort
  end


  def sort=(value)
    @sort = value
  end

  def full_title
    return self.title + (if self.volume then ", #{self.volume}" end)
  end


  def full_image_url( host="", force_https=false )
    if self.image_url
      return @@tmdb_image_base_url + self.image_url
    else
      if force_https
        return "https://#{host}/assets/#{@@image_not_available_url}"
      else
        return "//#{host}/assets/#{@@image_not_available_url}"
      end
    end
  end


  def copyright_year
    return Chronic.parse( self.copyright_date ).andand.strftime("%Y")
  end


  def imdb_url
    if self.imdb_id
      return @@imdb_base_url + self.imdb_id
    else
      return nil
    end
  end

  def location_name
    if self.location_code.andand.location_code == "ckcvi" or self.location_code.blank?
      return "3rd Floor DVDs"
    elsif self.location_code.andand.location_code == "ckvlh"
      return "1st Floor Best Sellers"
    end
  end

  def self.find_by_genre(genre_id)
    Dvd.joins(:dvd_genres).where(dvd_genres: {id: genre_id})
  end

  #Fetch metadata from TMDB API and save it to DVD record
  def fetch_tmdb_metadata

    if self.is_tmdb_data_harvested != 1 and self.tmdb_id.present?
      require 'net/http'

      #Get data
      tmdb_url = @@tmdb_api_base_url + self.tmdb_id + '?api_key=' + @@tmdb_api_key
      json_data = Net::HTTP.get_response(URI.parse(tmdb_url)).body
      data = JSON.parse(json_data)

      #Save to record
      self.imdb_id = data['imdb_id']
      self.summary ||= data['overview']
      self.image_url = data['poster_path']
      self.copyright_date = data['release_date']
      self.copyright_date ||= data['first_air_date']
      self.runtime = data['runtime']
      self.rating = data['vote_average']
      self.is_tmdb_data_harvested = 1
      self.save

      #Save genre data
      if data['genres']
        data['genres'].each do |tmdb_genre|
          dvd_genre = DvdGenre.find_by( :tmdb_id => tmdb_genre['id'] )
          if dvd_genre.blank?
            dvd_genre = DvdGenre.new
            dvd_genre.tmdb_id = tmdb_genre['id']
            dvd_genre.display_name = tmdb_genre['name']
            dvd_genre.save
          end

          dvd_genre_link = DvdGenreLink.find_by( :dvd_id => self.id, :genre_id => dvd_genre.id )
          if dvd_genre_link.blank?
            dvd_genre_link = DvdGenreLink.new
            dvd_genre_link.dvd_id = self.id
            dvd_genre_link.genre_id = dvd_genre.id
            dvd_genre_link.save
          end
        end
      end
    end
  end


  #initial query to get item and bib records from Sierra
  def self.fetch_all_dvds

    record_sql =
      "SELECT i.id as item_id, i.record_num as item_record_num,
        checkout_total, last_year_to_date_checkout_total, year_to_date_checkout_total,
        i.record_creation_date_gmt, call_number_norm, location_code,
        b.id as bib_id, b.title,  b.record_num as bib_record_num, b.language_code

      FROM sierra_view.item_view i
      join sierra_view.item_record_property irp on irp.item_record_id = i.id
      join sierra_view.bib_record_item_record_link bil on bil.item_record_id = i.id
      join sierra_view.bib_view b on b.id = bil.bib_record_id
      where (location_code = 'ckcvi' or location_code = 'ckvlh')
      and itype_code_num = 14" #itypecode 14 is dvd, location ckcvi is ucm media, ckvlh is dvd bestseller

    #execute the query
    sierra_connection = SierraConnection.new
    results = sierra_connection.select( record_sql )
    sierra_connection.disconnect

    #loop through results and create dvd records for new dvds
    results.each do |record|
      if Dvd.find_by( :item_record_id => record["item_id"] ).blank?
        dvd = Dvd.new
        dvd.item_record_id = record["item_id"]
        dvd.item_record_num = record["item_record_num"]
        dvd.bib_record_num = record["bib_record_num"]
        dvd.bib_record_id = record["bib_id"]
        dvd.cdate = record["record_creation_date_gmt"]
        dvd.title = record["title"].gsub( "[videorecording]", "" ).strip.chomp(".")
        dvd.summary = record["summary"]
        dvd.language = record["language_code"]
        if record["call_number_norm"]
          dvd.call_number = Lcsort.normalize( record["call_number_norm"] )
        end
        dvd.location_code = record["location_code"]
        dvd.ytd_checkouts = record["year_to_date_checkout_total"]
        dvd.last_ytd_checkouts = record["last_year_to_date_checkout_total"]
        dvd.checkouts = record["checkout_total"]

        if dvd.title.include?( "=" )
          dvd.title = dvd.title.gsub( "=", "(") + ")"
        end

        dvd.title_sort = dvd.title.gsub(/^the /i, "")
          .gsub(/^the\s/i, "")
          .gsub(/^a\s/i, "")
          .gsub(/^an\s/i, "")
          .gsub(/^el\s/i, "")
          .gsub(/^los\s/i, "")
          .gsub(/^la\s/i, "")
          .gsub(/^las\s/i, "")
          .gsub(/^le\s/i, "")
          .gsub(/^les\s/i, "")
        dvd.save
      end
    end

  end

  #Generate an email after a new record is created
  after_create do
    begin
      DvdMailer.new_dvd_notification( self )
    rescue Net::SMTPAuthenticationError => e
      nil
    end
  end


  #Get a list of all the item records and fetch corresponding marc data fields
  #for volume numbers and summaries
  def self.fetch_marc_fields
    new_dvds = Dvd.where( "is_marc_data_harvested = 0 or is_marc_data_harvested is null" )
    record_nums = new_dvds.pluck( :item_record_num, :bib_record_num ).flatten
    record_nums.delete(nil)
    marc_sql =
      "SELECT *
      FROM sierra_view.varfield_view marc
      WHERE marc.record_num in (#{record_nums.join(",")})
      and (marc.varfield_type_code in ('n','v') )
      and (marc.marc_tag is null or marc.marc_tag = '520')"

    #execute the query
    sierra_connection = SierraConnection.new
    results = sierra_connection.select( marc_sql )
    sierra_connection.disconnect

    #loop through results and append to dvd records
    results.each do |record|

      if record["marc_tag"] == "520"
        dvds = Dvd.where( :bib_record_num => record["record_num"])
        dvds.each do |dvd|
          dvd.summary = record["field_content"].gsub("|a", "")
          dvd.save
        end
      elsif record["varfield_type_code"] == "v"
        dvd = Dvd.find_by( :item_record_num => record["record_num"])
        if dvd
           dvd.volume = record["field_content"]
           dvd.save
        end
      end

    end

    new_dvds.each do |dvd|
      if dvd.volume.present? or dvd.summary.present? or (Time.now - dvd.created_at > 2.weeks)
        dvd.is_marc_data_harvested = 1
        dvd.save
      end
    end

  end


  #Check availability, location, withdrawn, use counts
  #Item is available if item_status_code = '-' and last_checkin_gmt >= last_checkout_gmt
  def self.check_availability_and_usage

    record_nums = Dvd.all.pluck( :item_record_id ).flatten
    record_nums.delete(nil)
    marc_sql =
      "SELECT *
      FROM sierra_view.item_view i
      WHERE i.id in (#{record_nums.join(",")})"

    #execute the query
    sierra_connection = SierraConnection.new
    results = sierra_connection.select( marc_sql )
    sierra_connection.disconnect

    #loop through results and append to dvd records
    Dvd.all.each do |dvd|
      results.each do |record|
        if dvd.item_record_id == record["id"]
          if record["item_status_code"] == "-" \
            and record["last_checkout_gmt"].blank? \
            and dvd.available != 1

            dvd.available = 1

          elsif record["item_status_code"] == "-" \
            and record["last_checkout_gmt"].present? \
            and record["last_checkin_gmt"].blank? \
            and dvd.available != 0

            dvd.available = 0

          elsif record["item_status_code"] == "-" \
            and record["last_checkin_gmt"] >= record["last_checkout_gmt"] \
            and dvd.available != 1

            dvd.available = 1

          elsif dvd.available != 0
            dvd.available = 0
          end

          if dvd.checkouts != record["checkouts"] \
            or dvd.ytd_checkouts != record["ytd_checkouts"] \
            or dvd.last_ytd_checkouts != record["last_ytd_checkouts"] \

            dvd.ytd_checkouts = record["year_to_date_checkout_total"]
            dvd.last_ytd_checkouts = record["last_year_to_date_checkout_total"]
            dvd.checkouts = record["checkout_total"]
          end

          if dvd.location_code != record["location_code"]
            dvd.location_code = record["location_code"]
          end

          if dvd.changed?
            dvd.save
          end
        end
      end
    end

  end
end
