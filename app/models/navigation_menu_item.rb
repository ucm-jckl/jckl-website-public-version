class NavigationMenuItem < ApplicationRecord
  # has_ancestry :orphan_strategy => :adopt, :touch => true
  has_many :children, class_name: 'NavigationMenuItem', foreign_key: :navigation_menu_item_id, dependent: :nullify
  belongs_to :parent, class_name: 'NavigationMenuItem', foreign_key: :navigation_menu_item_id

  validates :label, presence: true

  # get root parent items
  def self.roots
    NavigationMenuItem.where(navigation_menu_item_id: nil).order(:position)
  end

  # Generate a cache key based on the last time a menu item was modified
  def self.cache_key
    key = 'NavMenu' + NavigationMenuItem.count.to_s + NavigationMenuItem.maximum(:updated_at).to_s
  end
end
