class NewBookRequest < ApplicationRecord
  belongs_to :subject_area

  validates :requestor_name, :requestor_email, :format, :title, :copies_ordered, :subject_area, presence: true
end
