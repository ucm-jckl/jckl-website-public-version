class UserRole < ApplicationRecord
  belongs_to :user, foreign_key: :user_id

  ROLES = {
    admin: 'administrator',
    news: 'news',
    dvds: 'dvds',
    computers: 'computers',
    stats: 'statistics',
    stacks: 'stacks'
  }.freeze
end
