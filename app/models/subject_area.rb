class SubjectArea < ApplicationRecord
  belongs_to :subject_librarian, class_name: 'Employee', foreign_key: 'subject_librarian_id'
  belongs_to :parent_subject, class_name: 'SubjectArea', foreign_key: 'parent_subject_id'
  has_many :child_subjects, class_name: 'SubjectArea', foreign_key: 'parent_subject_id', dependent: :nullify
  has_many :call_number_ranges, dependent: :nullify

  def self.subjects_with_call_number_ranges
    SubjectArea.includes(:call_number_ranges).where.not(call_number_ranges: { id: nil })
  end

  def new_books
    all_new_books = NewBook.all
    subject_new_books = NewBook.none
    call_number_ranges.each do |range|
      all_new_books.each do |book|
        subject_new_books << book if book.within_call_number_range?(range.id)
      end
    end
    subject_new_books.distinct
  end
end
