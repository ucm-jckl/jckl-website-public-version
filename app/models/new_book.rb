class NewBook < Book
  belongs_to :location_code

  def isn_array
    isns.split(',')
  end

  def self.within_call_number_range(range_id)
    range = CallNumberRange.find_by(id: range_id)
    book_ids = []
    if range.present?
      NewBook.all.each do |book|
        if (Lcsort.normalize(book.plain_call_number) || '') >= (Lcsort.normalize(range.range_start) || '') \
          && (Lcsort.normalize(book.plain_call_number) || '') <= (Lcsort.normalize(range.range_end) || '')
          book_ids << book.id
        end
      end
    end
    NewBook.where(id: book_ids)
  end

  def self.within_subject_area(subject_id)
    subject = SubjectArea.includes(:call_number_ranges).find_by(id: subject_id)
    ranges = subject.andand.call_number_ranges
    book_ids = []
    if subject.present? && ranges.present?
      ranges.each do |range|
        book_ids += NewBook.within_call_number_range(range.id).pluck(:id)
      end
    end
    NewBook.where(id: book_ids)
  end

  # Get a randomized assortment of X of the most recent new books
  def self.randomized_new_books(quantity)
    new_books = NewBook.where("title is not null and image_url is not null and image_url != ''").order('cdate DESC').limit(quantity * 3)
    new_books.sample(quantity)
  end

  # Time ranges must be a number and a time period ("1 day", "2 months" "1 year" etc)
  def self.fetch_new_books(time_range)
    # Connect to the Sierra database
    # puts 'Connecting...'
    sierra_connection = SierraConnection.new

    # Construct the query
    query = generate_sql(time_range)

    # Execute the query
    # puts 'Executing query...'
    results = sierra_connection.select(query)
    if results.present?
      isn_query = create_isn_query(results)
      if isn_query.present?
        isn_data = sierra_connection.select(isn_query)

        # Put the results in a structured hash
        # puts 'Organizing results...'
        record_set = create_item_record_hash(results, isn_data)

        # Save the item records as local NewBook records
        create_new_book_records(record_set)

        # Normalize call numbers
        normalize_call_number
    end
    end
    # Close the Sierra database connection
    sierra_connection.disconnect
  end

  # Build the SQL query
  def self.generate_sql(time_range)
    # puts 'Building SQL query...'
    query = "select distinct on (b.id) b.id as bib_record_id, bil.item_record_id as item_record_id, b.record_num as record_number,
                irp.call_number_norm as call_number, brp.best_title as title, brp.best_author as author, brp.publish_year as publish_year,
                i.location_code, o.catalog_date_gmt as cdate
              from sierra_view.order_view o
              join sierra_view.order_record_cmf orc on o.id = orc.order_record_id
              join sierra_view.bib_record_order_record_link bol on bol.order_record_id = o.id
              join sierra_view.bib_view b on bol.bib_record_id = b.id
              join sierra_view.bib_record_property brp on b.id = brp.bib_record_id
              join sierra_view.bib_record_item_record_link bil on b.id = bil.bib_record_id
              join sierra_view.item_record i on bil.item_record_id = i.id
              join sierra_view.item_record_property irp on i.id = irp.item_record_id
              where orc.location_code like '#{@@location_code_like}'
              and o.catalog_date_gmt > (date '#{Date.today}' - interval '#{time_range}')
              and o.catalog_date_gmt is not null
              and i.location_code = orc.location_code
              order by b.id"

    query
  end

  # Build a secondary query to get ISNs for records from the first query
  def self.create_isn_query(record_set)
    record_numbers = []
    record_set.each do |record|
      record_numbers << record['record_number']
    end
    if record_numbers.present?
      query = "select m020a.content as isn, b.record_num as record_number
              from sierra_view.bib_view b
              join sierra_view.subfield_view m020a on b.record_num = m020a.record_num
              where b.record_num in (#{record_numbers.join(',')})
              and m020a.marc_tag = '020'
              and m020a.tag = 'a'
              and m020a.record_type_code = 'b'"
      return query
    else
      return nil
    end
  end

  # Loop through results and put them into a new hash containing record info and multiple ISNs
  def self.create_item_record_hash(results, isn_data)
    record_set = {}
    # puts results.count.to_s + " results"
    results.each do |row|
      record_set[row['record_number']] = {
        item_record_id: row['item_record_id'],
        bib_record_id: row['bib_record_id'],
        record_number: row['record_number'],
        call_number: row['call_number'],
        title: row['title'],
        author: row['author'],
        year: row['publish_year'],
        cdate: row['cdate'],
        location_code: row['location_code'],
        isn: []
      }
    end

    isn_data.each do |row|
      record_set[row['record_number']][:isn] << row['isn'].split(' ')[0]
    end

    # puts record_set.count.to_s + " unique records"
    record_set
  end

  # Create new books from item record data
  def self.create_new_book_records(item_record_data)
    new_books = []

    item_record_data.each do |_key, record|
      new_book = NewBook.new
      new_book.item_record_id = record[:item_record_id]
      new_book.bib_record_id = record[:bib_record_id]
      new_book.record_number = record[:record_number]
      new_book.call_number = record[:call_number]
      new_book.isns = record[:isn].join(',')
      new_book.year = record[:year]
      new_book.location_code_id = NewBooksLocation.find_by(location_code: record[:location_code]).id
      new_book.cdate = record[:cdate]
      new_book.title = record[:title]
      new_book.author = record[:author]
      new_books << new_book if new_book.save
    end

    new_books
  end

  # When a record is created, automatically fetch metadata from Syndetics/Google
  after_create :fetch_metadata_for_each_isn

  # Fetch metadata from Syndetics/Google, trying each ISN on the record
  def fetch_metadata_for_each_isn
    isn_array.each do |isn|
      if image_url.blank? || info_page_url.blank? || summary.blank?
        fetch_metadata(isn)
      end
    end
  end

  # Delete the book if it is older than 1 year
  def remove_if_old
    destroy if cdate < Time.now - 1.year
  end
end
