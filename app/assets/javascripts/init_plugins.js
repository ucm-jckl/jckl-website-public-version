//Initialize one-off plugins
$( document ).ready( function() {
  $( ".anytime-datetime" ).AnyTime_picker( {
    askEra: false,
    askSecond: false,
    format: "%Y-%m-%d %h:%i %p",
    latest: new Date( new Date().getTime() + ( 1000 * 60 * 60 * 24 * 365 * 2 ) )
  } ).removeAttr( "readonly" );

  $( ".anytime-date" ).AnyTime_picker( {
    askEra: false,
    askSecond: false,
    format: "%Y-%m-%d",
    latest: new Date( new Date().getTime() + ( 1000 * 60 * 60 * 24 * 365 * 2 ) )
  } ).removeAttr( "readonly" );

  $( ".readmore" ).readmore();
} );
