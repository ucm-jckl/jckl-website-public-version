class MeetingRoomBrowser extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      rooms: props.initialRooms,
      activeRoom: null,
      filterActive: false
    }

    this.handleAvailabilitySearchSubmit = this.handleAvailabilitySearchSubmit.bind(this);
    this.handleViewCalendarClick = this.handleViewCalendarClick.bind(this)
  }

  photo(room){
    if (room.photo_file_size) {
      return <div><img className="room-image" src={room.medium_photo_url}/></div>
    } else {
      return <p>(Photo not available.)</p>
    }
  }

  mapUrl(room){
    if(room){
      return this.props.mapUrl + "?room=" + room.room_name + "&floor=" + room.floor
    }else{
      return null;
    }
  }

  bookingUrl(room){
    if(room){
      return this.props.bookingUrl.replace("%7CROOMNAME%7C", room.room_name)
    }else{
      return null
    }
  }

  render() {
    let self = this;
    return(
    <div>
      <form className="room-bookings-availability-filter form-inline" onSubmit={(event) => self.handleAvailabilitySearchSubmit(event)}>
        <p>Looking for a room at a specific time? Enter the time here.</p>
        <div className="form-group">
          <label htmlFor="availability-filter-start-time">Start Time</label>
          <input type="text" className="form-control" id="availability-filter-start-time" ref="availabilityFilterStartTime"></input>
        </div>
        <div className="form-group">
          <label htmlFor="availability-filter-end-time">End Time</label>
          <input type="text" className="form-control" id="availability-filter-end-time" ref="availabilityFilterEndTime"></input>
        </div>
        <button className="btn btn-primary" type="submit">Submit</button>
      </form>

      {self.availabilityFilterMessage()}

      <ul className="room-bookings-room-list list-group">
        {this.state.rooms.map(function(room) {
          return (
            <li key={room.id} id={"room-" + room.room_name} className="list-group-item room container">
              <div className="row">
                <div className="col-md-12">
                  <h4 className="room-name">{room.display_name}</h4>
                  <div className="room-description">{room.description}</div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <table className="table table-sm table-striped">
                    <tbody>
                      <tr>
                        <td>Seats</td>
                        <td>{room.seating}</td>
                      </tr>
                      <tr>
                        <td>Desktops</td>
                        <td>{room.desktops}</td>
                      </tr>
                      <tr>
                        <td>Laptops</td>
                        <td>{room.laptops}</td>
                      </tr>
                      <tr>
                        <td>Projector</td>
                        <td className={room.projector}></td>
                      </tr>
                      <tr>
                        <td>Podium</td>
                        <td className={room.podium}></td>
                      </tr>
                      <tr>
                        <td>Whiteboard</td>
                        <td className={room.whiteboard}></td>
                      </tr>
                      <tr>
                        <td>Classroom Use</td>
                        <td className={room.classroom}></td>
                      </tr>
                      <tr>
                        <td>Meetings</td>
                        <td className={room.meetings}></td>
                      </tr>
                      {self.libraryUseOnly(room)}
                      <tr>
                        <td>{room.faculty_staff_only ? "Faculty and staff only" : "Faculty, staff, and students"}</td>
                        <td className={room.faculty_staff_only ? "notice" : "true"}></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="col-md-4">
                  {self.photo(room)}
                </div>
                <div className="room-booking-buttons col-md-4">
                  <a className="room-calendar-link btn btn-lg btn-default" data-toggle="modal" data-target="#room-bookings-calendar-modal" onClick={(event) => self.handleViewCalendarClick(event, room)} id={"room-" + room.room_name + "-calendar"}>View Calendar</a>
                  <a className="room-map btn btn-lg btn-default" href={self.mapUrl(room)}>View on Map</a>
                  <a className="room-book btn btn-lg btn-default" href={self.bookingUrl(room)}>Reserve This Room</a>
                </div>
              </div>
            </li>
          )
        })}
      </ul>
      <div className="room-bookings-calendar-modal modal" id="room-bookings-calendar-modal" tabIndex="-1" role="dialog" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h2 className="modal-title"></h2>
            </div>
            <div className="modal-body">
              <iframe className="room-bookings-iframe" id={this.state.activeRoom ? "room-bookings-iframe-" + this.state.activeRoom.room_name : ""} src={this.state.activeRoom ? this.state.activeRoom.google_calendar_week_embed : ""} scrolling="no"></iframe>
              <p className="google-calendar-link">Having trouble viewing this calendar?
                <a href="">View the calendar on Google.</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }

  //Set up AnyTime picker for time filters once the component is rendered
  componentDidMount(){
    $([this.refs.availabilityFilterStartTime, this.refs.availabilityFilterEndTime]).AnyTime_picker({
        askEra: false,
        askSecond: false,
        format: "%Y-%m-%d %h:%i %p",
        latest: new Date( new Date().getTime() + (1000*60*60*24*365*2) )
    }).removeAttr( "readonly" );
  }

  //Determine whether to add a library-use-only message
  libraryUseOnly(room){
    if (room.library_only) {
      return <tr>
        <td>Library use only</td >
        <td className="notice"></td>
      </tr>
    }else{
      return null;
    }
  }

  //Determine the class to give to the filter alert message
  availabilityFilterMessage(){
    if(!this.state.filterActive){
      return null
    }else if(this.state.filterActive && this.state.rooms.length == 0){
      return (
        <div className="room-bookings-availability-message alert alert-danger">
          There are no rooms available during this time period.
        </div>
      )
    }else{
      return (
        <div className="room-bookings-availability-message alert alert-success">
          There are {this.state.rooms.length} rooms available during this time period. They are listed below.
        </div>
      )
    }
  }

  //Set the Google Calendar iframe to the src of the room just clicked
  handleViewCalendarClick( event, room ) {
    this.setState({
      activeRoom: room
    });
  }

  //Make an ajax request upon form submit to find rooms available at a given time
  handleAvailabilitySearchSubmit(event) {
    event.preventDefault();

    let params = {
      start: this.refs.availabilityFilterStartTime.value,
      end: this.refs.availabilityFilterEndTime.value
    }

    $.ajax({
      url: this.props.availabilityUrl,
      dataType: 'json',
      data: params,
      cache: false,
      success: function(data) {
        this.setState({
          rooms: data,
          filterActive: true
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(status, err.toString());
      }.bind(this)
    });
  }
}

MeetingRoomBrowser.propTypes = {
  initialRooms: React.PropTypes.array,
  mapUrl: React.PropTypes.string,
  bookingUrl: React.PropTypes.string,
  availabilityUrl: React.PropTypes.string
};
