class Map extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      svgHeight: 0
    }
  }

  render () {
    return <div className="jckl-svg-map" ref="mapRoot" style={{height: this.state.svgHeight}}></div>;
  }

  componentDidMount() {
    let self = this;
    self.snap = {
      viewport: Snap().attr("id", "viewport"),
    }
    Snap.load(self.props.mapUrl, function(svg){
      self.snap.viewport.append(svg.selectAll('.floor'));
      self.snap.activeFloor = self.snap.viewport.select("#floor-" + (self.props.initialFloor || ""));
      self.snap.activeFloorNumber = self.props.initialFloor;
      self.snap.svgHeight = self.svgHeight(self.snap.viewport, self.snap.activeFloor);
      self.setState({svgHeight: self.snap.svgHeight}, function(){
        self.snap.panZoomControls = self.setupPanZoomControls(self.snap.viewport);
        if(self.props.initialHighlightedElementId){
          self.highlightElement(self.props.initialHighlightedElementId, true);
        }else{
          self.goToFloor(self.snap.activeFloor);
        }

        self.props.mapLoadedCallback();
      })
    });

    Snap(this.refs.mapRoot).append(viewport);
  }

  svgHeight(viewport, activeFloor) {
    let tallest_floor = 0;
    let total_height = 0;

    viewport.selectAll( '.floor' ).forEach( function( element, i ) {
      let height = element.getBBox().height;
      total_height += height;
      if ( height > tallest_floor ) {
        tallest_floor = height;
      }
    } );

    if(activeFloor){
      return tallest_floor * 1.25;
    }else{
      return total_height * 1.25;
    }

    return height;
  }

  setupPanZoomControls(viewport) {
    let self = this;
    return svgPanZoom( '#' + viewport.node.id, {
      maxZoom: 6,
      minZoom: 0.3,
      fit: false,
      center: false,
      contain: false,
      beforePan: function(oldPan, newPan){
        let stopHorizontal = false;
        let stopVertical = false;
        let gutterWidth = 100;
        let gutterHeight = 100;
        this.updateBBox();
        let sizes = this.getSizes();
        let leftLimit = -((sizes.viewBox.x + sizes.viewBox.width) * sizes.realZoom) + gutterWidth;
        let rightLimit = sizes.width - gutterWidth - (sizes.viewBox.x * sizes.realZoom);
        let topLimit = -((sizes.viewBox.y + sizes.viewBox.height) * sizes.realZoom) + gutterHeight;
        let bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.y * sizes.realZoom);

        customPan = {}
        customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x))
        customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y))

        return customPan;
      }
    });
  }

  goToFloor(floorElement) {
    if(floorElement){
      //hide other floors
      this.snap.viewport.selectAll( ".floor" ).forEach( function( element, i ) {
        element.removeClass( "active" );
        element.addClass( "hidden" );
      });

      //show this floor
      floorElement.addClass("active");
      floorElement.removeClass( "hidden" );
    }else{
      //remove active class from floors
      this.snap.viewport.selectAll( ".floor" ).forEach( function( element, i ) {
        element.removeClass( "active" );
      });
    }

    this.snap.panZoomControls.reset();
    this.snap.panZoomControls.resize();
    this.snap.panZoomControls.fit();
    this.snap.panZoomControls.fit(); //fit has to be called twice for some reason
    this.snap.panZoomControls.center();
  }

  //Highlight a specific element on the map
  highlightElement( elementSelector, allowFloorChange ) {
    let element = this.snap.viewport.select( "#" + elementSelector );
    if(element){
      if ( allowFloorChange ) {
        let floor = this.findElementParentFloor( element );
        this.snap.activeFloor = floor;
        this.snap.activeFloorNumber = floor.node.id.replace("floor-","");
        this.goToFloor( floor );
      }

      this.snap.viewport.selectAll( ".animating" ).forEach( function( element, i ) {
        element.removeClass( "animating" );
      } );
      element.addClass( "animating" );
      this.fadeInOut( element, 20, 0 );
    }
    return element;
  }

  //Fade an element in and out a set number of times
  fadeInOut( element, repeat, i ) {
    let self = this;
    let animationDuration = 750;
    element.animate( {
        opacity: 1
      },
      animationDuration,
      mina.linear,
      function() {
        element.animate( {
            opacity: 0
          },
          animationDuration,
          mina.linear,
          function() {
            i++;
            if ( i < repeat && element.hasClass( "animating" ) ) {
              self.fadeInOut( element, repeat, i );
            } else {
              element.removeClass( "animating" );
            }
          }
        );
      }
    );
  }

  //Get the parent floor of an element by traversing the tree upward
  findElementParentFloor( element ) {
    let floor = null;
    let currentElement = element;
    while ( floor === null && currentElement ) {
      let parent = currentElement.parent();
      if ( parent.hasClass( "floor" ) ) {
        floor = parent;
      }
      currentElement = parent;
    }

    return floor;
  }

  //Determine whether a given layer matches a given date
  doesLayerMatchDate(layer, date) {
    if(layer.attr('data-start-date') && layer.attr('data-end-date')){
      let startDate = new Date(layer.attr('data-start-date')+' 00:00:00 GMT-0500');
      let endDate = new Date(layer.attr('data-end-date')+' 00:00:00 GMT-0500');
      return startDate <= date && date <= endDate;
    }else{
      return false;
    }
  }
}

Map.propTypes = {
  mapUrl: React.PropTypes.string,
  initialFloor: React.PropTypes.number,
  initialHighlightedElementId: React.PropTypes.string,
  mapLoadedCallback: React.PropTypes.func
};
