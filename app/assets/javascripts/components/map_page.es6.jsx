class MapPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      roomSearchValue: null,
      roomNotFound: false,
      activeFloorNumber: props.initialFloor,
      computers: props.initialComputers
    }

    this.handleRoomSearchSubmit = this.handleRoomSearchSubmit.bind(this);
    this.handleChangeFloorClick = this.handleChangeFloorClick.bind(this);
    this.updateComputers = this.updateComputers.bind(this);
  }

  render () {
    return(
      <div>
        <div className="map-info">
          <ul className="nav nav-tabs" role="tablist">
            <li role="presentation" className="nav-item"><a href="#rooms" className="nav-link active" role="tab" data-toggle="tab">Find a Room</a></li>
            <li role="presentation" className="nav-item"><a href="#computers" className="nav-link" role="tab" data-toggle="tab">Find a Computer</a></li>
          </ul>
          <div className="tab-content">
            <div role="tabpanel" className="tab-pane clearfix active" id="rooms">
              <div className="col-md-6">
                <a href="http://ucmo.libcal.com/booking/jckl-study-rooms" className="reserve-study-room btn btn-secondary">Reserve Study Rooms</a>
                <a href={this.props.meetingRoomsUrl} className="reserve-other-room btn btn-secondary">Reserve Other Rooms</a>
              </div>
              <div className="col-md-6">
                <form className="room-search input-group" onSubmit={(event) => this.handleRoomSearchSubmit(event)}>
                  <input ref="roomSearchNumber" type="text" className="room-search-number form-control" placeholder="Find a Room Number"  />
                  <span className="input-group-btn">
                    <button className="btn btn-secondary" type="submit">Search</button>
                  </span>
                </form>
                {this.roomNotFound()}
              </div>
            </div>
            <div role="tabpanel" className="tab-pane clearfix" id="computers">
              <div className="computer-availability btn-group" role="group">
                <button type="button" className="btn btn-secondary" onClick={(event) => this.handleChangeFloorClick(event, 1)}>1st Floor <span className="tag tag-default">{this.availabilityCount(1)}</span></button>
                <button type="button" className="btn btn-secondary" onClick={(event) => this.handleChangeFloorClick(event, 2)}>2nd Floor <span className="tag tag-default">{this.availabilityCount(2)}</span></button>
                <button type="button" className="btn btn-secondary" onClick={(event) => this.handleChangeFloorClick(event, 3)}>3rd Floor <span className="tag tag-default">{this.availabilityCount(3)}</span></button>
              </div>
            </div>
            <div role="tabpanel" className="tab-pane clearfix" id="books">
              Book info
            </div>
          </div>
        </div>
        <div className="floor-select card">
          <div className="card-block">
            <h2 className="card-title">Floor</h2>
          </div>
          <div className="list-group list-group-flush">
            <a href="#" className={"floor list-group-item list-group-item-action" + (this.state.activeFloorNumber == 1 ? " active" : "")} onClick={(event) => this.handleChangeFloorClick(event, 1)}>{1}</a>
            <a href="#" className={"floor list-group-item list-group-item-action" + (this.state.activeFloorNumber == 2 ? " active" : "")} onClick={(event) => this.handleChangeFloorClick(event, 2)}>{2}</a>
            <a href="#" className={"floor list-group-item list-group-item-action" + (this.state.activeFloorNumber == 3 ? " active" : "")} onClick={(event) => this.handleChangeFloorClick(event, 3)}>{3}</a>
          </div>
        </div>
        <Map ref="map"
          mapUrl={this.props.mapUrl}
          initialFloor={this.props.initialFloor}
          initialHighlightedElementId={this.props.initialHighlightedElementId}
          mapLoadedCallback={this.updateComputers} />
      </div>
    );
  }

  updateComputers() {
    let self = this;
    this.setComputerAvailability();
    setInterval(function(){
      $.ajax({
        url: self.props.availabilityUrl,
        dataType: 'json',
        cache: false,
        success: function(data) {
          self.setState({computer: data});
        },
        error: function(xhr, status, err) {
          console.error(status, err.toString());
        }
      });
    }, 30000)
  }

  setComputerAvailability() {
    let self = this;
    this.refs.map.snap.viewport.selectAll('.computer').forEach(function(computerElement, i){
      //loop through computer data and update element classes
      for(let i = 0; i < self.state.computers.length; i++){
        let computer = self.state.computers[i];
        if(computer.map_tag == computerElement.attr('data-computer-tag')){
          if(computer['available?']){
            computerElement.addClass('available');
            computerElement.removeClass('unavailable');
          }else{
            computerElement.addClass('unavailable');
            computerElement.removeClass('available');
          }
        }
      }
    });
  }

  availabilityCount(floor){
    let available = 0;
    let total = 0;

    for(let i = 0; i < this.state.computers.length; i++){
      computer = this.state.computers[i];
      if(computer.floor == floor){
        total += 1;
        if(computer["available?"]){
          available += 1;
        }
      }
    }

    return available + " of " + total;
  }

  roomNotFound() {
    if(this.state.roomNotFound){
      return(
        <div className="room-not-found alert alert-danger">
          Sorry, we couldn't find a room called "{this.state.roomSearchValue}".
        </div>
      )
    }
  }

  handleRoomSearchSubmit(event) {
    event.preventDefault();

    let roomNumber = this.refs.roomSearchNumber.value.replace( /[^\d]/g, "" );
    let roomId;
    if ( this.refs.roomSearchNumber.value.indexOf( "media" ) != -1 ) {
      roomId = "room-media" + roomNumber;
    } else {
      roomId = "room-" + roomNumber;
    }
    let roomElement = this.refs.map.snap.viewport.select("#" + roomId);
    if( roomElement ){
      this.setState({
        roomNotFound: false,
        roomSearchValue: this.refs.roomSearchNumber.value
      });
      this.refs.map.highlightElement(roomId, true);
    }else{
      this.setState({
        roomNotFound: true,
        roomSearchValue: this.refs.roomSearchNumber.value
      });
    }
  }

  handleChangeFloorClick(event, floorNumber) {
    event.preventDefault();
    console.log("click")
    let floor = this.refs.map.snap.viewport.select("#floor-" + floorNumber);
    if(floor){
      this.refs.map.goToFloor(floor);
      this.setState({activeFloorNumber: floorNumber});
    }
  }
}

MapPage.propTypes = {
  availabilityUrl: React.PropTypes.string,
  meetingRoomsUrl: React.PropTypes.string,
  mapUrl: React.PropTypes.string,
  initialFloor: React.PropTypes.number,
  initialHighlightedElementId: React.PropTypes.string,
  initialComputers: React.PropTypes.array
};
