class NewBooksSlider extends React.Component {
  render() {

    let settings = {
      dots: false,
      arrows: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 5000,
      slidesToShow: 6,
      slidesToScroll: 6,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false
          }
        }
      ]
    };

    return (
      <Slider {...settings}>
        {this.props.books.map(function(book, index) {
          return (
            <div key={book.id}>
              <a href={book.quest_url}><img src={book.image_url} alt={book.title}/></a>
            </div>
          );
        })}
      </Slider>
    );
  }
}

NewBooksSlider.propTypes = {
  books: React.PropTypes.array
};
