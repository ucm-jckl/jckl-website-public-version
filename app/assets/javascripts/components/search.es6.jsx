class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTypes: {
        centralSearch: {
          label: "Central Search",
          placeholder: "Find books, articles, videos, and more",
          url: "http://ucmo.summon.serialssolutions.com/search?s.q=||SEARCHTERMS||"
        },
        catalog: {
          label: "Library Catalog",
          placeholder: "Search the library catalog for books and more",
          url: "https://quest.searchmobius.org/search/?searchtype=X&SORT=D&searcharg=||SEARCHTERMS||&searchscope=2"
        },
        articles: {
          label: "Articles",
          placeholder: "Use Central Search for just articles",
          url: "http://ucmo.summon.serialssolutions.com/search?s.q=||SEARCHTERMS||&s.cmd=addFacetValueFilters(IsScholarly,true)&s.light=t"
        },
        journals: {
          label: "Journal Directory",
          placeholder: "Find a specific journal title",
          url: "http://WD8CD4TK5M.search.serialssolutions.com/?tab=JOURNALS&N=100&V=1.0&L=WD8CD4TK5M&S=AC_T_B&C=||SEARCHTERMS||"
        },
        website: {
          label: "Library Website",
          placeholder: "Search the library website",
          url: "https://www.google.com/#q=site:library.ucmo.edu+||SEARCHTERMS||"
        }
      },
      activeSearchType: "centralSearch"
    };

    this.handleSearchTypeChange = this.handleSearchTypeChange.bind(this);
    this.handleSearchValueChange = this.handleSearchValueChange.bind(this);
  }

  render() {
    let self = this;
    return (
      <form className="search-wrapper search-has-focus input-group input-group-lg" method="GET" onSubmit={(event) => this.handleFormSubmit(event)} onBlur={(event) => this.handleSearchBlur(event)} onFocus={(event) => this.handleSearchFocus(event)}>
        <div className="dropdown-button-wrapper input-group-btn">
          <button type="button" className="search-type-button btn btn-default dropdown-toggle input-lg" data-toggle="dropdown" value="">{this.state.searchTypes[this.state.activeSearchType].label}</button>
          <ul className="search-type-list dropdown-menu">
            {Object.keys(self.state.searchTypes).map(function(searchType) {
              return (
                <li key={searchType} id={searchType + "-search-selector"} className="dropdown-item" onClick={() => self.handleSearchTypeChange(searchType)}>
                  {self.state.searchTypes[searchType].label}
                </li>
              );
            })}
          </ul>
        </div>
        <label className="sr-only" htmlFor="search-terms">Search Terms</label>
        <input type="text" className="search-text-field form-control" id="search-terms" value={this.state.searchValue || ""} onChange={this.handleSearchValueChange} placeholder={this.state.searchTypes[this.state.activeSearchType].placeholder}/>
        <div className="submit-button-wrapper input-group-btn">
          <button className="search-submit-button btn btn-default" type="submit">
            <span className="sr-only">Submit Search</span>
          </button>
        </div>
      </form>
    );
  }

  handleSearchFocus(event) {
    event.target.classList.add("search-has-focus");
    event.target.parentNode.classList.add("search-has-focus");
  }

  handleSearchBlur(event) {
    event.target.classList.remove("search-has-focus");
    event.target.parentNode.classList.remove("search-has-focus");
  }

  handleSearchTypeChange(newSearchType) {
    this.setState({activeSearchType: newSearchType});
  }

  handleSearchValueChange(event) {
    this.setState({searchValue: event.target.value});
  }

  handleFormSubmit(event) {
    event.preventDefault();
    let search_url = this.state.searchTypes[this.state.activeSearchType].url.replace("||SEARCHTERMS||", this.state.searchValue);
    window.location.href = decodeURIComponent(search_url);
  }

}
