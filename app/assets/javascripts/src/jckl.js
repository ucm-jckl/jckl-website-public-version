JCKL = (function(){
        
    var jckl = { 
        
        init: function(){
            return true;
        },
        
        
        UTILITIES: {
            
            production_server_hostname: "library.ucmo.edu",
            test_server_hostname: "library-test.ucmo.edu",
            development_server_hostname: "localhost:3000",
            local_hosts: [this.production_server_hostname, this.development_server_hostname, this.test_server_hostname],


            site_root: function(){
                return window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;
            },
            
            relative_url: function( relative_url ){
                return this.site_root() + "/" + relative_url;
            },
            
            
        }
        
    };
    
    return jckl;
}());

