JCKL.SVG_MAP_CONTROLLER = (function(){
    
    var svg_map_controller = {
        
        svg_file: JCKL.UTILITIES.relative_url("/svg/floorplan.svg"),
        base_zoom: 1,
        refresh_time: 60000,
        
        init: function( floor_id ){
            floor_id = typeof floor_id !== 'undefined' ? floor_id : "floor-1";
            
            this.snap = Snap(".map-wrapper .viewport");
            this.load_svg( floor_id );
        },

        
        //Load the SVG file containing the map and call various functions once the map is loaded.
        load_svg: function( floor_id ){  
            var this_class = this;
            Snap.load(this.svg_file, function(svg){
                this_class.append_layers_to_dom(svg);
                this_class.setup_zoom_and_pan_controls();
                this_class.set_active_floor( floor_id );
                this_class.resize_svg_height();
                this_class.set_initial_zoom();
                
            });
        },
        
        
         //Append layers from the SVG file into the page DOM
        append_layers_to_dom: function( svg ){
            var layers = svg.selectAll(".layer");
            this.snap.append(layers);
        },
        
        
        //Setup zoom and pan controls using svg-pan-zoom plugin
        setup_zoom_and_pan_controls: function(){
            this.map_controls = svgPanZoom(".map-wrapper", {
                //controlIconsEnabled: true,
                maxZoom: 6,
                minZoom: 0.3,
                fit: false,
                center: false
            }); 
        },
        
        
        //Resize the svg element's height to the minimum height of the tallest floor viewport
        resize_svg_height: function(){
            var tallest_viewport = 0;
            this.snap.selectAll( ".floor-viewport" ).forEach( function( element, i ){
                var height = element.getBBox().height;
                if( height > tallest_viewport ){
                    tallest_viewport = height;
                }
            });
            $( ".map-wrapper" ).height( tallest_viewport );
        },
        
        
        //Set the initial zoom level based on the size of the svg element
        set_initial_zoom: function(){
            //get the width of the svg wrapper and the width and height of the largest floor
            var wrapper_width = $( ".map-wrapper" ).width();
            var wrapper_height = $( ".map-wrapper" ).height();
            var image_width = 0;
            var image_height = 0;
            this.snap.selectAll( ".floor-viewport" ).forEach( function( element, i ){
                var height = element.getBBox().height;
                var width = element.getBBox().width;
                if( height > image_height ){
                    image_height = height;
                }
                if( width > image_width ){
                    image_width = width;
                }
            });
            
            //find the amount of zooming the page size can handle by increasing the zoom factor by increments of 0.1
            var zoom_factor = 0;
            while( zoom_factor * image_width < wrapper_width && zoom_factor * image_height < wrapper_height ){
                zoom_factor += 0.1;
            }
            
            //set the zoom
            this.base_zoom = zoom_factor - 0.1; //the -0.1 is corrective for web font size changes and other unknowables
        },
        
        
        //Set the active floor that is currently in the viewport
        set_active_floor: function( floor_id ){            
            this.snap.selectAll( ".floor-viewport" ).forEach( function( element, i ){
                element.removeClass( "active-floor" );
            });
            if( this.snap.select( "#" + floor_id + "-viewport" ) != null ){
                this.snap.select( "#" + floor_id + "-viewport" ).addClass( "active-floor" );
            }
            this.set_initial_zoom();
            this.jump_to_active_floor();
            this.lock_viewport_to_active_floor();
        },
        
        
        //Pan to the active floor and hide other floors
        jump_to_active_floor: function(){            
            //get the boundaries of the floor_viewport
            var box = this.snap.select(".floor-viewport.active-floor").getBBox();
            var x_min = box.x;
            var y_min = box.y;

            //center the viewport on the active floor
            //this.map_controls.resetZoom();
            this.map_controls.zoom( this.base_zoom );
            this.map_controls.pan({
                x: x_min * this.base_zoom,
                y: y_min * -1 * this.base_zoom
            });
        },
        
        
        //Prevent panning too far from the boundaries of the active floor
        lock_viewport_to_active_floor: function(){
            var this_class = this;
            
            this.map_controls.setOnPan( function(){
                
                var box = this_class.snap.select( ".floor-viewport.active-floor" ).getBBox();
                var x_min = box.x;
                var x_max = box.x2;
                var width = box.width;
                var y_min = box.y;
                var y_max = box.y2;
                var height = box.height;
                
                var gutter = 100 ;
                
                var left_bound = (x_min + gutter + width) * this.getZoom();
                var right_bound = ((x_max * -1) + gutter) * this.getZoom();
                var top_bound = ((y_min * -1 )+ gutter + height) * this.getZoom();
                var bottom_bound = ((y_max * -1) + gutter) * this.getZoom();
                
                if( this.getPan().x > left_bound  ){
                    this.pan({
                        x: left_bound,
                        y: this.getPan().y
                    });
                }
                
                if( this.getPan().x < right_bound  ){
                    this.pan({
                        x: right_bound,
                        y: this.getPan().y
                    });
                }
                
                if( this.getPan().y > top_bound  ){
                    this.pan({
                        x: this.getPan().x,
                        y: top_bound
                    });
                }
                
                if( this.getPan().y < bottom_bound  ){
                    this.pan({
                        x: this.getPan().x,
                        y: bottom_bound
                    }); 
                }
                
            });
        },
        
        
        //Highlight a specific location on the map
        highlight_location: function( location_id, floor_id ){
            this.set_active_floor( floor_id );
            
            var location_element = this.snap.select( "#" + location_id );
            this.snap.selectAll( ".map-wrapper .location" ).forEach( function( element, i ){
                element.removeClass( "animating" );
            });
            location_element.addClass( "animating" );
            this.fade_in_out( location_element, 8, 0 );
            
        },
        
        
        //Fade an element in and out a set number of times
        fade_in_out: function( element, repeat, i ){
            var this_class = this;
            var animation_duration = 750;
            element.animate({
                opacity: 1
            }, animation_duration, mina.linear, function(){
                element.animate({
                    opacity: 0
                }, animation_duration, mina.linear, function(){
                    i++;
                    if( i < repeat && element.hasClass( "animating" ) ){
                        this_class.fade_in_out( element, repeat, i );
                    }else{
                        element.removeClass( "animating" );
                    }
                });
            });
        },
        
        
        //Add or remove classes from study room elements
        add_study_room_class: function( room_id, class_name ){
            if( this.snap.select(  room_id ) !== null ){
                this.snap.select(  room_id ).addClass( class_name );
            }
        },
        
        remove_study_room_class: function( room_id, class_name ){
            if( this.snap.select( room_id ) !== null ){
                this.snap.select( room_id ).removeClass( class_name );
            }
        },
        
        //Add or remove classes from computer elements
        add_computer_class: function( computer_id, class_name ){
            if( this.snap.select( ".computer#" + computer_id ) !== null ){
                this.snap.select( ".computer#" + computer_id ).addClass( class_name );
            }
        },
        
        remove_computer_class: function( computer_id, class_name ){
            if( this.snap.select( ".computer#" + computer_id ) !== null ){
                this.snap.select( ".computer#" + computer_id ).removeClass( class_name );
            }
        },
        
        
        //Change the color of a computer
        change_computer_color: function( computer_id, fill_color_hex, stroke_color_hex ){
            if( this.snap.select( ".computer#" + computer_id ) !== null ){
                this.snap.select( ".computer#" + computer_id ).attr({
                    fill: fill_color_hex,
                    stroke: stroke_color_hex
                });
            }
        },
        
        //Add a popup to an element on hover
        add_popup_on_click: function( map_identifier, modal_identifier, modal_title, modal_content ){
            this.snap.selectAll( map_identifier ).forEach( function( element, i ){
                element.node.onclick = function(){ 
                    $( modal_identifier ).find( ".modal-title" ).empty().append( modal_title );
                    $( modal_identifier ).find( ".modal-body" ).empty().append( modal_content );
                    
                    $( modal_identifier ).modal();
                };
            });
        }
        
    };
    
    
    return svg_map_controller;
}());

