JCKL.MAP = (function(){
        
    var map = {
        
        availability_url:  JCKL.UTILITIES.relative_url("map/availability.json"),
        
        
        init: function(){
            if( $( ".map--show" ).length > 0 ){
                if( $( ".ie6, .ie7, .ie8" ).length == 0 ){
                    this.load_map();
                }
                this.set_availability();
            }
            return true;
        },
        
        
        load_map: function(){
            
            var active_floor = $( ".floors .floor.active" ).attr( "id" );
            this.map_controller = JCKL.SVG_MAP_CONTROLLER;
            this.map_controller.init( active_floor );
            
            //bind events
            this.change_floors();
            this.highlight_clicked_location();
        }, 
        
        
        //Change which floor is active by clicking a floor name
        change_floors: function(){
            var this_class = this;
            
            //Set an event to toggle the active floor when clicking a floor button
            $( ".floors .floor" ).click(function( e ){
                e.preventDefault();
                
                //add active state to the floor button
                $( this ).addClass( "active" );
                $( this ).siblings( ".floor" ).removeClass( "active" );
                
                this_class.map_controller.set_active_floor ( $( this ).attr( "id" ) );
            });
            
        },
        
        
        //Mark a clicked location in the nav list as active
        highlight_clicked_location: function(){
            var this_class = this;
            $( ".map-info .location" ).click( function( e ){
                e.preventDefault(); 
                
                var location_name = $( this ).attr( "data-location" );
                var location_floor = "floor-" + $( this ).attr( "data-floor" );
                
                //mark the floor and location as active and view them
                if( ! $( ".location.active" ).parents(".list-group").is( ".in" ) ){
                    $( ".location.active" ).parents(".list-group").collapse( "show" );
                }
                $( ".map-info .floor" ).removeClass( "active" );
                $( ".map-info #" + location_floor ).addClass( "active" );
                
                $( ".map-info .location" ).removeClass( "active" );
                $( this ).addClass( "active" );
                
                this_class.map_controller.highlight_location( 
                       location_name, 
                       location_floor
                );
        
            });
        },
        
        
        //Set the availability of computers and study rooms on the map
        set_availability: function(){
            var this_class = this;
            $.getJSON( this.availability_url, function( data ){
                //set computer availability
                if( $( ".ie6, .ie7, .ie8" ).length == 0 ){
                    $.each( data.computers, function( i, computer ){
                        //find the computer on the map matching the tag value of this JSON object
                        var tag = computer.tag;
                        var map_computer = "pc" + tag;

                        //set its availability
                        if( computer.is_available ){
                            this_class.map_controller.add_computer_class( map_computer, "available" );
                            this_class.map_controller.remove_computer_class( map_computer, "unavailable" );
                        }else{
                            this_class.map_controller.add_computer_class( map_computer, "unavailable" );
                            this_class.map_controller.remove_computer_class( map_computer, "available" );
                        }

                        if( computer.is_open ){
                            this_class.map_controller.add_computer_class( map_computer, "open" );
                            this_class.map_controller.remove_computer_class( map_computer, "closed" );
                        } else{
                            this_class.map_controller.add_computer_class( map_computer, "closed" );
                            this_class.map_controller.remove_computer_class( map_computer, "open" );
                        }
                    });

               
                }
                
                //Set the number of rooms and computers available in each location
                $.each( data.locations, function( i, location_type ){
                    for( var j = 0; j < location_type.length; j++ ){
                        
                        var location = location_type[j];
                        
                        //find the DOM element matching this location and insert the number of available units
                        var location_elements = $(".map-info").find( ".location" );
                        location_elements.each( function(){
                            if( $( this ).attr( "data-location" ) == "location-" + location.name ){
                                var badge = $( this ).find(".availability-count");
                                var badge_text;
                                if( location.is_open ){
                                    badge_text = location.available_units + " of " + location.total_units + " available";
                                }else{
                                    badge_text = "Currently Closed"; 
                                }
                                badge.empty().append( badge_text );
                            }
                        });
                        
                    }
                });
                
            });
        },
        
        
                
    };
    
    
    return map;
}());

