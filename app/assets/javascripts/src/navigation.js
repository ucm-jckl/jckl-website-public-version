JCKL.NAVIGATION = (function(){
        
    var navigation = {
        
        init: function(){
            this.add_dropdown_hover();
            
            return true;
        },
        
        //For full-size browsers, enable navigation dropdown menus working on hover
        add_dropdown_hover: function(){
            var width = $( window ).width();
            if( width > 960 ){
                $( "#jckl-header .header-navigation-menu .dropdown-toggle" ).dropdownHover({
                    delay: 600,
                    close_others: true
                });
            }
        }
                
    };
    
    
    return navigation;
}());

