JCKL.FORMS = (function(){
    
    var forms = {
        
        
        init: function(){
            this.apply_datetimepicker();
            this.apply_chosen();
            this.clear_form();
            
            return true;
        },
        
        
        //Apply the jQuery AnyTime plugin to datetime fields
        apply_datetimepicker: function(){
            $( "form .datetime" ).AnyTime_picker({
                askEra: false,
                askSecond: false,
                format: "%Y-%m-%d %h:%i %p",
                latest: new Date( new Date().getTime() + (1000*60*60*24*365*2) )
            }).removeAttr( "readonly" );
            
            $( "form .date" ).AnyTime_picker({
                askEra: false,
                askSecond: false,
                format: "%Y-%m-%d", 
                latest: new Date( new Date().getTime() + (1000*60*60*24*365*2) )
            }).removeAttr( "readonly" );
        },
        
        
        //Apply the Chosen plugin to .chosen select fields
        apply_chosen: function(){
            $( "form .chosen" ).chosen();
        },
        
        //Clear a form
        clear_form: function(){
            $( ".form-reset" ).click( function(){
                var form = $( this ).parents( "form" );
                form.find( "input, select" ).val("");
            });
        }
        
    };
    
    
    return forms;
}());

