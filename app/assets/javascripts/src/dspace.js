var $ = jQuery;
$( document ).ready( function(){
    
    var DSPACE = (function(){

        var dspace = {

            init: function(){
                this.fix_dropdowns();
                this.rewrite_request_copy_page();
                return true;
            },


            //Fix broken dropdown menus
            fix_dropdowns: function(){
                $( ".dropdown-toggle" ).dropdown();
            },
            
            //Rewrite DSpace's default Request a Copy restricted resource page so that it is just an LDAP login link
            rewrite_request_copy_page: function(){
                if( $( "#aspect_artifactbrowser_ItemRequestForm_div_itemRequest-form" ).length > 0 ){
                    var form = $( "#aspect_artifactbrowser_ItemRequestForm_div_itemRequest-form" );
                    
                    form.find("p").slice(2).remove();
                    form.find("fieldset").remove();
                    form.find("a").attr( "href", "/ldap-login" );
                   
                }
            }
        }


        return dspace;
    }());
    
    DSPACE.init();

});