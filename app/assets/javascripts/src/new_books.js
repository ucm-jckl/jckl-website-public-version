JCKL.NEW_BOOKS = (function(){
        
    var new_books = {
                
        init: function(){
            if( $( ".newbooks--index, .pages--home" ).length > 0 ){
                
                if( $( ".front-page-new-books" ).length > 0 ){
                    this.start_new_books_carousel();
                    this.add_new_books_popover();
                }

                if( $( ".new-books-navigation" ).length > 0 ){
                    this.set_navigation_option_click_event();
                }

                if( $( ".new-books-pagination" ).length > 0 ){
                    this.add_pagination();
                }        

                if( $( ".new-books-list, .new-books-details, .new-books-table" ).length > 0 ){
                    this.show_summary();
                }

                if( $( ".new-books-columns" ).length > 0 ){
                    this.create_book_popovers();
                }

                this.lazy_load_images();
            
            }
            return true;
        },
        
        //Carousel
        
        start_new_books_carousel: function(){
            //Start the carousel display of the new books feed on the home page.
            $( ".books-carousel" ).owlCarousel({
                items: 6,
                itemsDesktop: [1200, 6],
                itemsDesktopSmall: [960, 5],
                itemsTablet: [768, 3],
                itemsMobile: [320, 2],
                //stopOnHover: true,
                autoPlay: 8000,
                scrollPerPage: true,
                pagination: true,
                navigation: true,
                navigationText: ["",""],
            });
        },
        
        
        add_new_books_popover: function(){
            //Add a tooltip to the new books using Bootstrap popover.
            $( ".books-carousel" ).find(".new-book img").popover({
                html: true,
                trigger: "hover",
                placement: "top",
                container: ".books-carousel",
                delay: {
                    show: 350,
                    hide: 400
                }
            });
        },
                
                
        //New Books Page Navigation
                
        //Set an onclick event that changes the current URL and the displayed navigation parameter
        //based on the option that was clicked.
        set_navigation_option_click_event: function(){
            var this_class = this;

            $('.new-books-navigation .dropdown-menu a').click(function(){

                //Get info about what was clicked
                var clicked_parameter = $(this).attr("data-parameter");
                var clicked_value = $(this).attr("data-value");
                var clicked_label = $(this).text();
                
                
                //Change the button to show what was clicked
                var button = $(this).parents(".dropdown-menu").siblings(".new-books-navigation-current");
                button.empty().append( clicked_label );
                
                //Replace the URL querystring with the value of what was clicked
                var uri = new URI( window.location );
                uri.setSearch( clicked_parameter, clicked_value );
                uri.removeSearch( "page" );
                window.location = uri.toString();
            });
        },
        
        
        
        
        //New Books Pagination
        
        //add pagination using the twbsPagination plugin and set page-changing functionality
        add_pagination: function(){
            
            //show the start page if one is specified
            var start_page = Number( $( ".new-books-pagination" ).attr( "data-start-page" ) );
            if( start_page != 0 ){
                $( ".new-books-page-" + start_page ).show().siblings( ".new-books-page" ).hide();
            }
            
            //add pagination and state-changing
            $( ".new-books-pagination" ).each( function(){
                var page_count = Number( $( this ).attr( "data-page-count" ) );

                $( this ).twbsPagination({
                    totalPages: page_count,
                    visiblePages: 5,
                    startPage: start_page,
                    onPageClick: function( event, page ){
                        $( ".new-books-page-" + page ).show().siblings( ".new-books-page" ).hide();
                        
                        var uri = new URI( window.location );
                        uri.setSearch( "page", page );
                        History.pushState( null, null, uri.search() );
                    }
                });
            });
        },

        
        
        //Misc New Books Functions
        
        //Expand/contract book summaries
        show_summary: function(){
            if( $( ".new-books-list" ).length > 0 ){
                $( ".new-book-summary.loading" ).removeClass( "loading" );
                $( ".new-book-summary" ).more({
                    length: 300,
                    wordBreak: true,
                    leeway: 20
                });
                $( ".more-link" ).addClass( "btn btn-default btn-sm" );
                
            }
            
            if( $( ".new-books-details, .new-books-table" ).length > 0 ){
                $( ".new-book-summary" ).truncate( {maxLength: 250} );
                $( ".new-book-summary" ).removeClass( "loading" );
            }
        },
        
        //Add bootstrap popovers to book images
        create_book_popovers: function(){
            $( ".new-book-image img" ).popover({
                placement: "right",
                trigger: "hover",
                html: "true"
            });
        },
        
        //Load images as the user scrolls to them
        lazy_load_images: function(){
            
            $( ".new-book-column, .new-books-page" ).each( function(){
                //skip the first 5 books on each page/column set up lazyloading
                $( ".new-book-image", this ).slice( 5 ).find( ".lazy-load" ).show().lazyload({ 
                    threshold: 100,
                    effect: "fadeIn"
                });
                
                //load the first 5 on each page/column like normal
                $( ".new-book-image .lazy-load", this ).each( function(){
                    var image_url = $( this ).attr( "data-original" );
                    $( this ).attr( "src", image_url ).show();
                });
            });
            
            
        }
    };
    
    
    return new_books;
}());

