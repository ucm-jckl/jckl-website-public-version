JCKL.ILLIAD = (function(){
    
    var illiad = {
        
        
        init: function(){
            this.add_table_classes();
            
            return true;
        },
        
        
        //Add bootstrap table classes to tables
        add_table_classes: function(){
            if( $( ".illiad" ).length > 0 ){
                $( ".default-table > table" ).addClass( "table table-striped table-condensed" );
            }
        }      
    };
    
    
    return illiad;
}());

