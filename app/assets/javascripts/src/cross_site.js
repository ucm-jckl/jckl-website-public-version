//Helper functions for enabling cross-site communication between various web platforms
JCKL.CROSS_SITE = (function(){
        
    var cross_site = {
        
        init: function(){
            this.cross_site_iframe_fix();
            this.fetch_page_elements();
            
            return true;
        },
        
        //Resize iframes containing content from this site
        cross_site_iframe_fix: function(){
            
            if( $( ".jckl-iframe" ).length > 0 ){
                $( ".jckl-iframe" ).iFrameResize( { 
                    heightCalculationMethod: "lowestElement", 
                } );
            }
        },
        
        //Fetch header, footer, and common boxes from the main library.ucmo.edu site on different sites (eg LibGuides)
        fetch_page_elements: function(){
            //First check that the current domain is not library.ucmo.edu
            
            if( JCKL.UTILITIES.local_hosts.indexOf(window.location.host) == -1  ){
                
                //Determine whether to use HTTPS or HTTP with a special HTTP proxy (because IE requires same protocol for CORS)
                var base_url = window.location.protocol + "//library.ucmo.edu/elements/";
                if( window.location.protocol.toLowerCase() == "http:" ){
                    base_url = window.location.protocol + "//library.ucmo.edu/http/elements/";
                }
                
                //Get the header
                if( $( "#jckl-header" ).length > 0 ){
                    var custom_jquery = $;
                    $( "#jckl-header .header-navigation-menu .navbar-collapse" ).load( base_url + "header .header-navigation-menu .navbar-collapse > *", function(){
                        custom_jquery( "#jckl-header .header-navigation-menu .dropdown-toggle" ).dropdownHover();
                    });
                    $( "#jckl-header .ucm-links" ).load( base_url + "header .ucm-links > *", function(){
                        custom_jquery( "#jckl-header .ucm-links .dropdown-toggle" ).dropdownHover();
                    });
                } 

                //Get the search bar and then re-initialize the search bar behavior for the newly added DOM elements
                if( $( "#jckl-search" ).length > 0 ){
                    $( "#jckl-search" ).load( base_url + "search #jckl-search > *", function(){
                        SEARCH.init();
                    });
                }
                
                //Get the footer
                if( $( "#jckl-footer" ).length > 0 ){
                    $( "#jckl-footer .footer-navigation-menu" ).load( base_url + "footer .footer-navigation-menu > *" );
                }
            
                //Get the library hours
                if( $( "#jckl-sidebar .library-hours-block" ).length > 0 ){
                    $( "#jckl-sidebar .library-hours-block" ).load( base_url + "hours .library-hours-block > *" );
                }
            
                //Get the help links
                if( $( "#jckl-sidebar .help-block" ).length > 0 ){
                    $( "#jckl-sidebar .help-block" ).load( base_url + "help .help-block > *" );
                }
                
                //Get the computer information
                if( $( "#jckl-sidebar .computers-block" ).length > 0 ){
                    $( "#jckl-sidebar .computers-block" ).load( base_url + "rooms_computers .computers-block > *" );
                }
                
                //Get the chat popout widget
                if( $( "#jckl-chat-popout" ).length > 0 ){
                    $( "#jckl-chat-popout" ).hide().removeAttr( "style" ).removeClass( "chat-popout" ).load( base_url + "chat #jckl-chat-popout > *", function(){
                        $( this ).addClass( "chat-popout" );
                        CHAT.init();
                    });
                }
            }
        }
                
    };
    
    
    return cross_site;
}());

