JCKL.LIBRARY_STATS = (function(){
    
    
    var stats = {
        
        init: function(){
            
            this.get_report_data();
            this.download_csv();
            
            return true; 
        },
        
        
        get_report_data: function(){
            if( $( ".highcharts-chart" ).length > 0 ){
                var this_class = this;
                
                var base_url = JCKL.UTILITIES.site_root();
                if( JCKL.UTILITIES.local_hosts.indexOf(window.location.host) == -1
                && !window.location.host.startsWith("153.91.")){
                    var base_url = "https://library.ucmo.edu";
                }
                
                
                $( ".highcharts-chart" ).each( function(){
                    var chart_area = $(this)
                    chart_area.siblings( ".loading" ).show();
                    var url = base_url + "/stats/chart.json";
                    var params = $( ".chart-data-filters form" ).serialize();
                    var report_type = $( this ).attr( "data-report-type" );
                    params += "&report_type=" + report_type;
                    $.getJSON( url, params, function( data ){
                        var chart_element = $( ".highcharts-chart[data-report-type='"+report_type+"']" );
                        this_class.initialize_highcharts( data, chart_element );
                        chart_area.siblings( ".loading" ).hide();
                    });
                });
            }
        },
        
        
        //Set up HighCharts graphs on pages containing them 
        initialize_highcharts: function( data, chart_element ){
            Highcharts.setOptions({
                lang: {
                    thousandsSep: ","
                }
            });

            chart_element.highcharts( data );
        },
        
        
        download_csv: function(){
            if( $( ".highcharts-chart" ).length > 0 ){
                $( "#download-csv" ).click( function(){
                    var url = JCKL.UTILIES.relative_url("/stats/chart.csv");
                    var params = $( ".chart-data-filters form" ).serialize();
                    var report_type = $( ".highcharts-chart" ).first().attr( "data-report-type" );
                    params += "&report_type=" + report_type;
                    window.open( url + "?" + params );
                });
            }
        }
    };
    
    
    return stats;
}());

