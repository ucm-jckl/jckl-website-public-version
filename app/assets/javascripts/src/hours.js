JCKL.HOURS = (function(){
        
    var hours = {
        
        init: function(){
            if( $( ".library-hours-page" ).length > 0 ){
                this.add_hours_pagination();
            }
            return true;
        },
        
        
        add_hours_pagination: function(){
            //Hide all but the first 7 days of the calendar on the Library Hours page and add a "Show More" pagination button.
            $(".library-hours-page .library-hours-entry").slice(7).hide();

            //add a click event to show more days
            $(".library-hours-page .library-hours-show-more").click(function(){
                //test if there are more hidden .item-list elements
                if($(".library-hours-page .library-hours-entry").filter(":hidden").length > 0){
                    //get the next 7 items that are hidden and show them
                    $(".library-hours-page .library-hours-entry").filter(":hidden").slice(0, 7).slideDown();
                }else{
                    //hide the Show More button if there are no more items and show the end message
                    $(".library-hours-page .library-hours-show-more").hide();
                    $(".library-hours-page .library-hours-end-of-calendar").show();
                }
            });
        }
        
        
                
    };
    
    
    return hours;
}());

