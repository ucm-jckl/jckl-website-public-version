JCKL.GOOGLE_ANALYTICS_STATS = (function(){
        
    /*
     * Information on Google Analytics tracking events here: https://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide
     * ga(
     *      'send',
     *      'event', //This has to be in every event
     *      'Category', 
     *      'Action', 
     *      'Label',
     *      'Value'
     *  );
     */    
    
    
    var stats = {
        
        init: function(){
            this.record_header_events();
            this.record_footer_events();
            this.record_search_events();
            this.record_sidebar_events();
            this.record_chat_events();
            this.record_database_page_events();
            this.record_hours_page_events();
            this.record_new_books_events();
            this.record_news_events();
            this.record_front_page_three_column_events();
            
            return true;
        },
        
        record_header_events: function(){
            //record when the UCM links dropdown menu is expanded
            $( "#jckl-header .ucm-links" ).on( "show.bs.dropdown", function(){
                ga(
                    'send',
                    'event',
                    'Header Region',
                    'Dropdown Menu Expand', 
                    'UCM Links Menu',
                    1
                );
            });
            
            //record when one of the main navigation dropdowns is expanded
            $( "#jckl-header .header-navigation-menu .navbar-nav > .dropdown" ).on( "show.bs.dropdown", function(){
                var event_label = $( ".dropdown-toggle", this ).text()
                ga(
                    'send',
                    'event',
                    'Header Region',
                    'Dropdown Menu Expand', 
                    event_label,
                    1
                );
            });
            
            //record when a link in one of the dropdowns is clicked
            $( "#jckl-header .dropdown-menu a" ).click( function(){
                var event_label = $( this ).parents( ".dropdown" ).find( ".dropdown-toggle" ).text() + " - " + $( this ).text();
                ga(
                    'send',
                    'event',
                    'Header Region',
                    'Dropdown Menu Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when the logo is clicked
            $( "#jckl-header .logo" ).click( function(){
                ga(
                    'send',
                    'event',
                    'Header Region',
                    'Logo Home Link Click', 
                    '',
                    1
                );
            });
        },
        
        record_footer_events: function(){
            //record when a link in the footer is clicked
            $( "#jckl-footer .footer-columns a" ).click( function(){
                var event_label = $( this ).parents( ".footer-column" ).find( "h3" ).text() + " - " + $( this ).text();
                ga(
                    'send',
                    'event',
                    'Footer Region',
                    'Footer Link Click', 
                    event_label,
                    1
                );
            });
        },
        
        record_search_events: function(){
            //record when a search is submitted
            $( "#jckl-search .search-wrapper" ).submit( function(){
                var search_terms = $( ".search-text-field" ).val();
                var search_type = $( ".search-type-button" ).text();
                ga(
                    'send',
                    'event',
                    'Search Region',
                    search_type + ' Submit', 
                    search_terms,
                    1
                );
            });
            
            //record when someone opens the search type dropdown
            $( "#jckl-search .dropdown-button-wrapper" ).on( "show.bs.dropdown", function(){
                ga(
                    'send',
                    'event',
                    'Search Region',
                    'Search Type Menu Expand', 
                    '',
                    1
                );
            });
            
            //record when someone changes the search type using the dropdown
            $( "#jckl-search .search-type-list li" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Search Region',
                    'Search Type Change', 
                    event_label,
                    1
                );
            });
            
            //record when someone clicks a link in the Search Options area
            $( "#jckl-search .search-options .navbar-nav a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Search Region',
                    'Search Option Link Click', 
                    event_label,
                    1
                );
            });
        },
        
        record_sidebar_events: function(){
            //record when a link in the sidebar is clicked
            $( "#jckl-sidebar a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Sidebar Region',
                    'Sidebar Link Click', 
                    event_label,
                    1
                );
            });
            
        },
        
        record_chat_events: function(){
            //record when the big chat button on the home page is clicked
            $( ".hero-button .chat-link" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Chat Links',
                    'Home Page Giant Chat Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a chat link in the header menu is clicked
            $( "#jckl-header .chat-link" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Chat Links',
                    'Header Navigation Menu Chat Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a chat link in the footer menu is clicked
            $( "#jckl-footer .chat-link" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Chat Links',
                    'Footer Menu Chat Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a chat link in the sidebar menu is clicked
            $( "#jckl-sidebar .chat-link" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Chat Links',
                    'Sidebar Chat Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a popout chat link is clicked
            $( "#jckl-chat-popout a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Chat Links',
                    'Popout Chat Link Click', 
                    event_label,
                    1
                );
            });
        },
        
        record_database_page_events: function(){
            //record when text in the database search field is changed
            $( ".database-page .database-search-input" ).change( function(){
                var event_label = $( this ).val();
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Database Search Field Change', 
                    event_label,
                    1
                );
            });
            
            //record when a letter in the Browse By Letter list is clicked
            $( ".database-page .database-a-z a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Database Letter Click', 
                    event_label,
                    1
                );
            });
            
            //record when the Select a Subject button is clicked
            $( ".database-page .database-subjects-modal-button" ).click( function(){
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Select Subject Button Click', 
                    '',
                    1
                );
            });

            
            //record when a specific subject is clicked
            $( ".database-page .database-subjects-modal-wrapper .database-subject a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Subject Click', 
                    event_label,
                    1
                );
            });
            
            //record when the clear filter button is clicked
            $( ".database-page .database-applied-filters-clear" ).click( function(){
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Clear Filters Click', 
                    '',
                    1
                );
            });
            
            //record when a database title link is clicked
            $( ".database-page .database-title a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Database Title Click', 
                    event_label,
                    1
                );
            });
            
            //record when a database friendly URL link is clicked
            $( ".database-page .database-friendly-url a" ).click( function(){
                var event_label = $( this ).parents( ".database" ).find( ".database-title a" ).text();
                ga(
                    'send',
                    'event',
                    'Database Page',
                    'Database Friendly URL Click', 
                    event_label,
                    1
                );
            });
            
        },
        
        record_hours_page_events: function(){
            //record when the View Full Monthly Calendars menu is expanded
            $( ".library-hours-page .library-hours-google-calendars .btn-group" ).on( "show.bs.dropdown", function(){
                ga(
                    'send',
                    'event',
                    'Library Hours Page',
                    'Monthly Calendars Menu Expand', 
                    '',
                    1
                );
            });
            
            //record when a monthly calendar link is clicked
            $( ".library-hours-page .library-hours-google-calendars .dropdown-menu a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'Library Hours Page',
                    'Monthly Calendar Click', 
                    event_label,
                    1
                );
            });
            
            //record when the Show the Next Week button is clicked
            $( ".library-hours-page .library-hours-show-more" ).click( function(){
                ga(
                    'send',
                    'event',
                    'Library Hours Page',
                    'Show Next Week Click', 
                    '',
                    1
                );
            });
        },
        
        record_new_books_events: function(){
            //record when new book scroll arrows are clicked on the home page
            $( ".front-page-new-books .owl-prev, .front-page-new-books .owl-next" ).click( function(){
                var event_label = $( this ).attr( "class" );
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Homepage Scroll Arrow Click', 
                    event_label,
                    1
                );
            });
            
            //record when new books scroll dots are clicked on the home page
            $( ".front-page-new-books .owl-page" ).click( function(){
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Homepage Scroll Dot Click', 
                    '',
                    1
                );
            });
            
            //record when the View More New Arrivals link is clicked on the home page
            $( ".front-page-new-books" ).siblings( ".new-books-view-more" ).click( function(){
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Homepage View More Click', 
                    '',
                    1
                );
            });
            
            //record when a specific book image link is clicked
            $( ".new-book a img" ).click( function(){
                var event_label = $( this ).parent( "a" ).attr( "href" );
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Image Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a specific book popover appears
            $( ".new-book" ).on( "show.bs.popover", function(){
                var event_label = $( this ).find( "a" ).attr( "href" );
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Image Popover', 
                    event_label,
                    1
                );
            });
            
            //record when a specific book title link is clicked
            $( ".new-book .new-book-title a" ).click( function(){
                var event_label = $( this ).attr( "href" );
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Title Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a specific book call number link is clicked
            $( ".new-book .new-book-call-number a" ).click( function(){
                var event_label = $( this ).attr( "href" );
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Call Number Link Click', 
                    event_label,
                    1
                );
            });
            
            //record when a specific book show more/less button is clicked
            $( ".new-book .more-link" ).click( function(){
                var event_label = $( this ).text() + " - " + $( this ).parents( ".new-book" ).find( ".new-book-title a" ).attr( "href" );
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Show More/Less Click', 
                    event_label,
                    1
                );
            });
            
            //record when the new books page subject dropdown is expanded
            $( ".new-books-subject" ).on( "show.bs.dropdown", function(){
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Subject Dropdown Expand', 
                    '',
                    1
                );
            });
            
            //record when the new books page timeframe dropdown is expanded
            $( ".new-books-timeframe" ).on( "show.bs.dropdown", function(){
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Timeframe Dropdown Expand', 
                    '',
                    1
                );
            });
            
            //record when the new books page display dropdown is expanded
            $( ".new-books-display-type" ).on( "show.bs.dropdown", function(){
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Display Dropdown Expand', 
                    '',
                    1
                );
            });
            
            //record when a new books page subject is clicked
            $( ".new-books-subject .dropdown-menu a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Subject Click', 
                    event_label,
                    1
                );
            });
            
            //record when a new books page timeframe is clicked
            $( ".new-books-timeframe .dropdown-menu a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Timeframe Click', 
                    event_label,
                    1
                );
            });
            
            //record when a new books page display type is clicked
            $( ".new-books-display-type .dropdown-menu a" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Display Click', 
                    event_label,
                    1
                );
            });
            
            //record when the new books page RSS link is clicked
            $( ".new-books-rss-link" ).click( function(){
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books RSS Link Click', 
                    '',
                    1
                );
            });
            
            //record when a pagination button is clicked
            $( ".new-books-pagination a" ).not( ".disabled" ).click( function(){
                var event_label = $( this ).text();
                ga(
                    'send',
                    'event',
                    'New Books',
                    'New Books Pagination Click', 
                    event_label,
                    1
                );
            });
        },
        
        record_news_events: function(){
            //record when a news scroll arrow is clicked
            $( ".front-page-news .owl-prev, .front-page-new-books .owl-next" ).click( function(){
                var event_label = $( this ).attr( "class" );
                ga(
                    'send',
                    'event',
                    'News',
                    'News Homepage Scroll Arrow Click', 
                    event_label,
                    1
                );
            });
                        
            //record when a news scroll dot is clicked
            $( ".front-page-news .owl-page" ).click( function(){
                ga(
                    'send',
                    'event',
                    'News',
                    'News Homepage Scroll Dot Click', 
                    '',
                    1
                );
            });
                        
            //record when a story image link is clicked
            $( ".front-page-news .news-image a" ).click( function(){
                var event_label = $( this ).attr( "href");
                ga(
                    'send',
                    'event',
                    'News',
                    'News Homepage Story Image Click', 
                    event_label,
                    1
                );
            });
            
            //record when a story image Read More link is clicked
            $( ".front-page-news .news-story-read-more" ).click( function(){
                var event_label = $( this ).attr( "href");
                ga(
                    'send',
                    'event',
                    'News',
                    'News Homepage Story Read More Click', 
                    event_label,
                    1
                );
            });
            
            //record when a social media icon is clicked
            $( ".front-page-news .social-media-icons a" ).click( function(){
                var event_label = $( this ).attr( "href");
                ga(
                    'send',
                    'event',
                    'News',
                    'Homepage Social Media Icon Click', 
                    event_label,
                    1
                );
            });
        },
        
        record_front_page_three_column_events: function(){
            //record when a link is clicked
            $( "#three-col-display a" ).click( function(){
                
                if( $( this ).is( ".badge" ) ){
                    var event_label = $( this ).siblings( "a" ).text() + " (badge)";
                }else{
                    var event_label = $( this ).text();
                }
                
                ga(
                    'send',
                    'event',
                    'Homepage Three Columns Region',
                    'Link Click', 
                    event_label,
                    1
                );
            });
        }
                
    };
    
    
    return stats;
}());

