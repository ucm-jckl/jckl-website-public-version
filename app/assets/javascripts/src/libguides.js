var $ = jQuery;
$( document ).ready( function(){
    
    JCKL_LIBGUIDES = (function(){

        var libguides = {

            init: function(){

                this.identify_edit_mode();
                this.add_best_practices_link();
                this.fix_subpage_dropdowns();
                this.modify_email_link();
                this.fix_single_page_guide_names();
                
                return true;
            },

            //add a special class to the body tag on Edit Mode pages
            identify_edit_mode: function(){
                if( $( ".s-lib-cmd-bar" ).length > 0 ){
                    $( "body" ).addClass( "libguides-edit-mode" );
                }
            },
            
            //add best practices guide link to edit mode pages
            add_best_practices_link: function(){
                if ( $(".libguides-edit-mode").length > 0 ){
                    
                    var li = $( document.createElement( "li" ) );
                    
                    var a = $( document.createElement( "a" ) );
                    a.attr({
                        href: "http://guides.library.ucmo.edu/best-practices",
                        target: "_blank"
                    });
                    
                    var i = $( document.createElement( "i" ) );
                    i.addClass( "fa fa-star-o fa-lg" );
                    
                    $( "#s-lg-admin-command-bar .nav:first-child" ).append( li );
                    li.append( a );
                    a.append( i ).append( "JCKL Best Practices" );
                    
                }
            },

            //Add page navigation box listing all other boxes
            add_page_nav: function(){
                if( $( ".libguides-page-navigation" ).length > 0 ){
                    
                    var links = [];
                    $( ".content-column .s-lib-box" ).each( function(){
                        var a = document.createElement( "a" );
                        a.href = "#" + $( this ).attr( "id" );
                        var heading = $( ".s-lib-box-title", this ).clone();
                        $( a ).append( heading.contents() );
                        $( a ).children().remove();
                        var li = document.createElement( "li" );
                        $( li ).append( a );
                        links.push( li );
                    });
                    
                    for( var i = 0; i < links.length; i++ ){
                        $( ".libguides-page-navigation ul" ).append( links[i] );
                    }
                }
            },
            
            //Fix dropdown nav menus for subpages on enhanced tab layout
            fix_subpage_dropdowns: function(){
                if( $( ".libguides-template-enhanced-tabs").length > 0 ){ 

                    $( ".s-lg-tab-top-link" ).addClass( "dropdown-toggle" )
                        .attr( "data-toggle", "dropdown" );
                    $( ".s-lg-tab-drop" ).remove();
                    $( ".libguides-template-enhanced-tabs .s-lg-tab-top-link.dropdown-toggle" ).dropdownHover({
                        delay: 600,
                        close_others: true
                    });
                }
                
                
            },
            
            //Modify the Email Me profile link so that it points to the library contact form instead of mailto
            modify_email_link: function(){
                if( $( ".s-lib-profile-email" ).length > 0 ){
                    if( $( ".s-lib-profile-email a" ).attr( "href" ) !== undefined ){
                        //get the librarian's email address minus the @ucmo.edu part
                        var email_address = $( ".s-lib-profile-email a" ).attr( "href" ).replace( "mailto:", "" ).replace( "@ucmo.edu", "" );

                        //build a contact page URL and make the Email Me link point to it
                        var url = "https://library.ucmo.edu/contact/employee/" + email_address;
                        $( ".s-lib-profile-email a" ).attr( "href", url );
                    }
                }
            },
            
            //Remove page names from html title and breadcrumb for single page guides
            fix_single_page_guide_names: function(){
                if( $( ".libguides-single-page-no-nav" ).length > 0 ){
                    //breadcrumb
                    $( "#s-lib-bc-page" ).remove();
                    var guide = $( "#s-lib-bc-guide" );
                    guide.addClass( "active" );
                    var guide_title = guide.find( "a" ).contents()[0];
                    guide.empty().append( guide_title );
                    
                    //html title
                    var title_parts = $( "title" ).text().split( " - " );
                    var new_title = title_parts[1] + " - " + title_parts[2].replace( "Guides at ", "" );
                    $( "title" ).empty().append( new_title );
                }
            }

        };


        return libguides;
    }());
    
    LIBGUIDES.init();

});