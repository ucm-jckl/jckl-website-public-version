JCKL.DATABASES = (function(){
    
    
    var databases = {
                
        init: function(){
            
            if($(".database-page").length > 0){
                this.animate_database_subjects_menu();
                this.wire_database_filters();
                this.wire_clear_database_filters();
                this.filter_from_url();
            }
            
            return true;
        },
        
        
        //Add interactivity to the subject menu on the database page.
        animate_database_subjects_menu: function(){
            //expand/collapse the list group displaying the subjects
            $(".database-subjects .modal-body > ul > li").click(function(){
                $(this).toggleClass("open").find("ul").slideToggle();;
                $(this).siblings("li").removeClass("open").find("ul").slideUp();
            });
        },
        
        
        //Add filter functionality to the database page for clicking a letter or subject in the Browse By section.
        wire_database_filters: function(){            
            var this_class = this;
            
            //filter by letter
            $(".database-a-z li").click(function(){
                var letter = $("a", this).attr( "data-letter" ).toLowerCase();
                this_class.filter_by_letter( letter );
            });

            //filter by subject
            $(".database-subjects .modal-body > ul > li > ul > li").click(function(){
                var subject_name = $( "a", this ).text().trim();
                var subject_id = $( "a", this ).attr( "data-subject-id" );
                var exclude_multidisciplinary = false;
                if( $( "a", this ).attr( "data-exclude-multidisciplinary" ) == "true" ){
                    exclude_multidisciplinary = true;
                }
                this_class.filter_by_subject( subject_name, subject_id, exclude_multidisciplinary );                
            });
            
            //filter with search terms 
            $(".database-search-input").keyup(function(){
                var search_value = $(this).val().toLowerCase();
                this_class.filter_by_search( search_value );
            });
        },
        
        
        //Get filter values from the URL and apply them
        filter_from_url: function(){
            var uri = new URI( window.location );
            if( uri.hasQuery( "letter" ) ){
                var letter = uri.search( true ).letter;
                this.filter_by_letter( letter );
            }else if( uri.hasQuery( "subject" ) ){
                var subject_name = uri.search( true ).subject.trim();
                var subject_id = null;
                var exclude_multidisciplinary = false;
                $( ".database-subjects .database-subject" ).each( function(){
                    var subject_anchor = $( this ).find ( "a" );
                    if( subject_anchor.text().toLowerCase().trim() == subject_name.toLowerCase().trim() ){
                        subject_id = subject_anchor.attr( "data-subject-id" );
                        if( subject_anchor.attr( "data-exclude-multidisciplinary" ) == "true" ){
                            exclude_multidisciplinary = true;
                        }
                    }
                });
                this.filter_by_subject( subject_name, subject_id, exclude_multidisciplinary );
                
            }else if( uri.hasQuery( "search" ) ){
                var search_value = uri.search( true ).search;
                this.filter_by_search( search_value );
            }
        },
        
        
        filter_by_letter: function( letter ){
            this.clear_database_filters( true );
            
            $(".database-page .database-list > ul > li").hide().filter( function( index ){
                return $( ".database-title a", this ).text().toLowerCase().charAt(0) == letter;
            }).show();
            this.show_current_database_filter(letter, "letter");
            this.set_filter_history( "letter", letter );
            this.make_databases_even_odd();
        },
        
        
        filter_by_subject: function( subject_name, subject_id, exclude_multidisciplinary ){
            this.clear_database_filters(true);

            //Show databases related to this subject and hide all other databases
            var multidisciplinary_subject_id = $( ".database-subject-multidisciplinary-id" ).val(); 
            $( ".database-page .database-list > ul > li" ).hide();
            $( ".database-page .database-list > ul > li.database-subject-" + subject_id ).show();
            if( exclude_multidisciplinary == false){
                $( ".database-page .database-list > ul > li.database-subject-" + multidisciplinary_subject_id ).not( ".unimportant" ).show();
            }
            
            //If this is a parent subject, show all child subjects
            var subject_element = $( ".database-subject-" + subject_id );
            
            if( subject_element.is( ".database-parent-subject" ) ){
                subject_element.siblings( ".database-subject" ).each( function(){
                    var child_subject_id = $( "a", this ).attr( "data-subject-id" );
                    $( ".database-page .database-list > ul > li.database-subject-" + child_subject_id ).show();
                });
            }

            this.show_current_database_filter( subject_name, "subject" );
            this.set_filter_history( "subject", subject_name );
            this.make_databases_even_odd();

            //close the modal
            $( ".database-subjects-modal-wrapper" ).modal("hide");
        },
        
        
        filter_by_search: function( search_value ){
            this.clear_database_filters(false);
            if(search_value !== ""){
                $(".database-page .database-list > ul > li").hide().filter(function( index ){
                    return $( ".database-title a", this ).text().toLowerCase().indexOf( search_value ) != -1;
                }).show();
                this.show_current_database_filter( search_value, "search" );
                this.set_filter_history( "search", search_value );
                this.make_databases_even_odd();
            }
        },
        
        
        show_current_database_filter: function(filterText, filterType){
            //Display to the user the database filters currently in place, or hide the Current Filter section if there is no filter.
            var message = "";
            switch(filterType){
                case "letter":
                    message = "Name starts with \""+ filterText.toUpperCase() +"\""
                    break;
                case "subject":
                    message = "Subject - " + filterText;
                    break;
                case "search":
                    message = "Name contains \""+ filterText +"\"";
                    break;
            }

            $(".database-applied-filters-value").empty().append(message);
            $(".database-applied-filters").show();
        },
        
        
        clear_database_filters: function(empty_search_box){
            //Clear any active database filter: search, first letter, or subject.
            $( ".database-applied-filters" ).hide();
            $( ".database-page .database-list > ul > li" ).show();
            if( empty_search_box ){
                $( ".database-search-input" ).val( "" );
            }
            
            //Clear the filter history in the URL
            var uri = new URI( window.location );
            uri.removeSearch( "letter" ).removeSearch( "subject" ).removeSearch( "search" );
            History.pushState( null, null, uri.toString() );
        },
        
        
        wire_clear_database_filters: function(){
            var this_class = this;
            
            //Sets an event to clear the database filters when clicking the Clear button.
            $(".database-applied-filters-clear").click(function(){
                this_class.clear_database_filters(true);
            });
        },
        
        
        make_databases_even_odd: function(){
            //Assign even and odd classes to visible databases.
            $(".database-page .database-list > ul > li").removeClass("even odd");
            $(".database-page .database-list > ul > li").filter(":visible").filter(":odd").addClass("odd");
            $(".database-page .database-list > ul > li").filter(":visible").filter(":even").addClass("even");
        },
        
        
        set_filter_history: function( filter_type, filter_value ){
            var uri = new URI( window.location );
            uri.setSearch( filter_type, filter_value );
            History.pushState( null, null, uri.search() );
        },
                
    };
    
    
    return databases;
}());

