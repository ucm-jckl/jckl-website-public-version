JCKL.NEWS = (function(){
        
    var news = {
        
        init: function(){
            if( $( ".front-page-news" ).length ){
                this.create_news_slideshow();
            }
            
            return true;
        },
        
        //Create a news slideshow with Owl Carousel widget.
        create_news_slideshow: function(){
            if( $( ".news-slideshow" ).length > 0 ){
                $( ".news-slideshow" ).owlCarousel({
                    pagination: true,
                    navigation: true,
                    navigationText: ["",""],
                    singleItem: true,
                    autoPlay: 12000,
                    stopOnHover: true
                });
            }
            
        }
                        
    };
    
    
    return news;
}());

