JCKL.EJS = (function(){
    
    var ejs = {
        
        
        init: function(){
            this.add_panel_classes();
            
            return true;
        },
        
        
        //Add bootstrap panel classes to body
        add_panel_classes: function(){
            if( $( ".ejs" ).length > 0 ){
                $( ".ejs .content-body" ).addClass( "panel panel-default panel-body" );
            }
            
        }      
        
    };
    
    
    return ejs;
}());

