$( document ).ready( function(){
    
    var SUMMON = (function(){

        var summon = {

            init: function(){

                this.load_custom_css();
                
                return true;
            },

           //load a custom css file
           load_custom_css: function(){
               var css = $( document.createElement( "link" ) );
               css.attr({
                   href: "https://library.ucmo.edu/css/summon.css",
                   rel: "stylesheet",
                   type: "text/css",
                   media: "all"
               });
               $( "head" ).append( css );
           }

        };


        return summon; 
    }());
    
    SUMMON.init();

});