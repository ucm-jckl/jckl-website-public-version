
JCKL.CHAT = (function(){
        
    var chat = { 
        
        init: function(){
            if( $( ".chat-popout" ).length > 0 ){
                this.show_chat_popout();
            }
            
            if( $( ".chat-popout, .chat-link" ).length > 0 ){
                this.launch_chat_from_popout();
            }
            
            return true;
        },
        
        
        //Launch a LibraryH3lp chat window when the chat popout is clicked.
        launch_chat_from_popout: function(){
            $( ".chat-popout a, .chat-link" ).click(function( e ){
                var success = window.open( $(this).attr("href"), "libchat", "toolbar=no, location=no, directories=no, status=no, scrollbars=no, menubar=no, resizeable=yes, width=300, height=400");
                if( success ){
                    e.preventDefault();
                }
            });
        },
        
        
        //Reposition the chat popout, show it, and slide it in.
        show_chat_popout: function(){
            //Align the chat popout just off the right side of the screen. Due to it being rotated, 
            //CSS right:0 doesn't work. We will also calculate the finalOffset value that will determine
            //where the popout will end up after sliding.
            var chat = $("#jckl-chat-popout");
            var finalOffset = -1 * ((chat.width() / 2) - (chat.height() / 2) + 3); //the +3 is to account for borders and rounding
            var initialOffset = finalOffset - chat.height() - 1; //the -1 is to account for borders
            chat.css("right", initialOffset);

            //show the chat popout and slide it out to the finalOffset value so it is on screen
            if( $( ".ie8" ).length > 0 ){
                setTimeout( function(){
                    var ie_offset = -1 * ( chat.height() - chat.width() );
                    chat.show();
                    chat.css( "right", ie_offset );
                }, 3000);
            }else{
                chat.show().animate({right: finalOffset}, 500);
            }
        },
    };
    
    
    return chat;
}());

