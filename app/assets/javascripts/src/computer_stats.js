JCKL.COMPUTER_STATS = (function(){
        
    var computer_stats = {
        
        json_url: JCKL.UTILITIES.relative_url("/stats/computers.json"),
        json_data: {},
        current_index: 0,
        
        init: function(){
            if( $( ".computer-stats" ).length > 0 ){
                this.init_computer_data_table();
                this.load_map();
                this.download_csv_submit();
                this.get_json_data();
                this.set_pagination_events();
            }
            
            
            return true;
        },
        
        
        load_map: function(){
            this.map_controller = JCKL.SVG_MAP_CONTROLLER;
            this.map_controller.init();
        },
        
        
        init_computer_data_table: function(){
            $( ".computer-stats .data-table" ).DataTable({
                "paging": false,
                "searching": false,
                "order": [[ 3, "desc" ]]
            });
        },
        
        
        download_csv_submit: function(){
            $( "#download_csv" ).click( function(){
                var form_data = $( ".computer-stats-filters" ).serialize();
                var url = $( ".computer-stats-filters" ).attr( "action" ) + ".csv?" + form_data;
                window.open( url );
            });
        },
        
        
        get_json_data: function(){
            var this_class = this;
            $( ".computer-stats-filters" ).submit( function( e ){
                e.preventDefault(); 
                
                $( ".computer-stats .loading" ).show();

                
                var form_data = $( ".computer-stats-filters" ).serialize();
                $.getJSON( this_class.json_url + "?" + form_data, function( data ){
                    this_class.json_data = data;
                    this_class.json_data_array = Object.keys( data.data_sets ).map( function( k ) { return data[k] }); 
                    this_class.current_index = 0;
                    
                    this_class.set_os();
                    this_class.populate_tables();
                    this_class.modify_map();
                    this_class.update_display_headings();
                    
                    this_class.map_controller.resize_svg_height();
                    this_class.map_controller.set_initial_zoom();
                    
                    $( ".computer-stats .data-display" ).css({
                        "visibility": "visible",
                        "position": "relative",
                        "height": "100%"
                    });
                    
                    $( ".computer-stats .loading" ).hide();
                   
                });
            });
        },
          
        //determine which operating systems to include in the data
        set_os: function(){
            var show_windows = true;
            if( $( "#os_windows:checked" ).length == 0 ){
                show_windows = false;
            }
            
            var show_mac = true;
            if( $( "#os_mac:checked" ).length == 0 ){
                show_mac = false;
            }
            
            var show_linux = true;
            if( $( "#os_linux:checked" ).length == 0 ){
                show_linux = false;
            }
            
            this.os = {
                "show_windows": show_windows,
                "show_mac": show_mac,
                "show_linux": show_linux
            };
            
        },
        
        //determine whether to include a particular computer based on its os
        include_computer: function( computer ){
            var include = false;
            
            if( computer.is_windows == true && this.os.show_windows == true ){
                include = true;
            }
            
            if( computer.is_mac == true && this.os.show_mac == true ){
                include = true;
            }
            
            if( computer.is_linux == true && this.os.show_linux == true ){
                include = true;
            }
            
            return include;
        },
        
        
        populate_tables: function(){
            
            $( ".computer-stats .data-table" ).DataTable().clear();
            
            var total_use_time_all_computers = 0;
            var total_sessions_all_computers = 0;
            var number_of_computers = this.json_data.data_sets[ this.current_index ].computers.length;
            
            var dataset = this.json_data.data_sets[ this.current_index ];
            
            for( computer in dataset.computers ){
                if( this.include_computer( dataset.computers[computer] ) ){
                    $( ".computer-stats .data-table" ).DataTable().row.add([
                        dataset.computers[ computer ].computer_name,
                        dataset.computers[ computer ].location,
                        dataset.computers[ computer ].total_sessions,
                        dataset.computers[ computer ].total_use_time_s,
                        dataset.computers[ computer ].total_use_time,
                        dataset.computers[ computer ].average_use_time,
                        dataset.computers[ computer ].median_use_time
                    ]).draw();
                    
                    total_use_time_all_computers += dataset.computers[ computer ].total_use_time_s;
                    total_sessions_all_computers += dataset.computers[ computer ].total_sessions;
                }
            }
            
            $( ".total-usage-time-all-computers" ).empty().
                    append( this.seconds_to_time_string( total_use_time_all_computers) );
            $( ".total-average-usage-time-per-computer" ).empty()
                    .append( this.seconds_to_time_string( total_use_time_all_computers / number_of_computers ) );
            $( ".total-sessions-all-computers" ).empty().append( total_sessions_all_computers );
            $( ".total-number-of-computers" ).empty().append( number_of_computers );
        },
        
        //convert seconds to a time string
        seconds_to_time_string: function( s ){

            var dd = Math.floor( s / ( 60 * 60 * 24 ) );
            s = s - ( dd * 60 * 60 * 24 );
            var hh = Math.floor( s / ( 60 * 60 ) );
            s = s - ( hh * 60 * 60 );
            var mm = Math.floor( s / 60 );
            s = s - ( mm * 60 );
            var ss = s;

            
            var time_parts = [];
            if( dd >= 1 ){
                time_parts.push( dd + "d" );
            }
            if( hh >= 1 ){
                time_parts.push( Math.floor( hh ) + "h" );
            }
            if( mm >= 1){
                time_parts.push( Math.floor( mm ) + "m" );
            }
            if( ss >= 1){
                time_parts.push( Math.floor( ss ) + "s" );
            }
            
            
            return time_parts.join( " " );
        },
        
        
        update_display_headings: function(){
            
            var timeframe = $( document.createElement( "h2" ) )
                    .append( this.json_data.start_time + " to " + this.json_data.end_time );
            var report_type = $( document.createElement( "p" ) )
                    .append( "Report Type: " + $( ".computer-stats-filters #report-type option[value='"+ this.json_data.report_type +"']" ).text() );
            var location = $( document.createElement( "p" ) )
                    .append( $( ".computer-stats-filters #location option:selected" ).text() );

            var data_set_heading = $( document.createElement( "div" ) )
                    .append( timeframe )
                    .append( report_type )
                    .append( location )
                    .addClass( "report-heading" );
            $( ".report-heading" ).replaceWith( data_set_heading );
            
            var page_heading = this.json_data.data_sets[ this.current_index ].timeframe;
            $( ".computer-stats .data-display .page-timeframe" ).empty().append( page_heading );
            
            var total_pages = this.json_data.data_sets.length;
            $( ".computer-stats .data-display .page-counter-total" ).empty().append( total_pages );
            $( ".computer-stats .data-display .page-counter-current" ).empty().append( this.current_index + 1 );
            
            if( this.current_index == 0 ){
                $( ".data-display .page-back" ).addClass( "disabled" );
            }else{
                $( ".data-display .page-back" ).removeClass( "disabled" );
            }
            
            if( this.current_index == total_pages - 1 ){
                $( ".data-display .page-forward" ).addClass( "disabled" );
            }else{
                $( ".data-display .page-forward" ).removeClass( "disabled" );
            }
            
        },
        
        
        modify_map: function(){
            
            //Build a color scale
            switch( this.json_data.report_type ){
                case "hourly":
                    var max_range = 60 * 30; //max brightness at 30 minutes
                    break;
                case "hourly-aggregate":
                    var max_range = 60 * 30; //max brightness at 30 minutes
                    break;
                case "daily":
                    var max_range = 60 * 60 * 12; //max brightness at 12 hours
                    break;
                case "daily-aggregate":
                    var max_range = 60 * 60 * 12; //max brightness at 12 hours 
                    break;
                case "total": 
                    var max_range = 1;
                    break;
            }
            
            var scale = chroma.scale([ "#222", "#55f", "red" ]).domain([0, max_range]); 
            var usage_times = [];
            for(var i = 0; i < this.json_data.data_sets[ this.current_index ].computers.length; i++){
                var computer = this.json_data.data_sets[ this.current_index ].computers[i];
                if( this.include_computer( computer ) ){
                    usage_times.push( computer.total_use_time_s );
                }
            };
            var highest_usage_time = Math.max.apply( null, usage_times );
            
            //Loop through computers and assign them a color and a modal popup
            for( var i = 0; i < this.json_data.data_sets[ this.current_index ].computers.length; i++ ){
                
                var computer = this.json_data.data_sets[ this.current_index ].computers[i]
                var computer_id = "pc" + computer.map_tag;
                
                //add colors
                if( this.json_data.report_type == "total" ){
                    var color = scale( computer.total_use_time_s / highest_usage_time );
                }else{
                    var color = scale( computer.total_use_time_s ); 
                }
                
                //add visual key values
                if( this.json_data.report_type == "total" ){
                    $( ".color-key .start" ).empty().append( "0m" );
                    $( ".color-key .end" ).empty().append( this.seconds_to_time_string( highest_usage_time ) );
                }else{
                     $( ".color-key .start" ).empty().append( "0m" );
                    $( ".color-key .end" ).empty().append( this.seconds_to_time_string( max_range ) );
                }
                
                //change the color
                if( this.include_computer( computer ) ){
                    this.map_controller.change_computer_color( 
                        computer_id, color.hex(), color.darken().hex() 
                    );
                    this.map_controller.remove_computer_class( "pc" + computer.map_tag, "hidden" );
                }else{
                    this.map_controller.add_computer_class( "pc" + computer.map_tag, "hidden" );
                }
                
                
                //add modal popup to each computer
                this.map_controller.add_popup_on_click( 
                    "#" + computer_id, ".data-display .modal", 
                    "Computer: " + computer.computer_name, this.generate_modal_content( computer ) 
                );
            }
            
            //show the floor
            var floor_id = "floor-" + this.json_data.active_floor;
            this.map_controller.set_active_floor( floor_id );    
            
        },
        
        
        //generate the content to add to popup modals
        generate_modal_content: function( computer ){
            var modal_content = $( document.createElement( "table" ) );
            modal_content.addClass( "table table-striped table-condensed" );
            
            var row1 = $( "<tr><td>Location</td><td>"+ computer.location +"</td></tr>" );
            var row2 = $( "<tr><td>Total Sessions</td><td>"+ computer.total_sessions +"</td></tr>" );
            var row3 = $( "<tr><td>Total Use Time</td><td>"+ computer.total_use_time +"</td></tr>" );
            var row4 = $( "<tr><td>Average Use Time</td><td>"+ computer.average_use_time +"</td></tr>" );
            var row5 = $( "<tr><td>Median Use Time</td><td>"+ computer.median_use_time +"</td></tr>" );
            
            modal_content.append( row1 ).append( row2 ).append( row3 ).append( row4 ).append( row5 );
            modal_content.addClass( "table table-striped table-condensed" );
            
            return modal_content;
        },
        
        set_pagination_events: function(){
            var this_class = this;
            $( ".page-back" ).not( ".disabled" ).click( function(){
                this_class.cycle_data_back();
            });
            
            $( ".page-forward" ).not( ".disabled" ).click( function(){
                this_class.cycle_data_forward();
            });
        },
        
        
        cycle_data_back: function(){
            if( this.current_index != 0 ){
                this.current_index -= 1;
                
                this.populate_tables();
                this.modify_map();
                this.update_display_headings();
            }
        },
        
        
        cycle_data_forward: function(){
            if( this.current_index != this.json_data.data_sets.length - 1 ){
                this.current_index += 1;
            
                this.populate_tables();
                this.modify_map();
                this.update_display_headings();
            }
        }
                
    };
    
    
    return computer_stats;
}());

