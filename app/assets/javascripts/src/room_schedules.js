JCKL.ROOM_SCHEDULES = (function(){
    
    var room_schedules = {
        
        
        init: function(){
            if( $( ".room-schedules" ).length > 0 ){
                this.change_calendar();

                this.toggle_daily_view_calendars();
                this.activate_daily_view_chosen();
            }
            
            return true;
        },
        
        
        
        //Change the displayed calendar when a View Calendar link is clicked.
        change_calendar: function(){
            $( ".room-calendar-link" ).click( function( e ){
                var room = $( this ).parents( ".room" );
                var room_name = room.attr( "data-room-name" );
                var iframe_src = room.attr( "data-iframe-src" );
                var calendar_link = room.attr( "data-calendar-link" );
                
                var modal = $( "#room-schedules-calendar-modal" );
                modal.find( ".modal-title" ).empty().append( room_name );
                modal.find( ".room-schedule-iframe" ).attr( "src", iframe_src );
                modal.find( ".google-calendar-link a" ).attr( "href", calendar_link );
            });
        },
        
        
        //On the daily view page, display selected calendars and resize them to fit in a single row
        toggle_daily_view_calendars: function(){
            
            if( $( ".room-schedules-daily-select-rooms" ).length > 0 ){
                $( ".room-schedules-daily-select-rooms" ).change( function(){
                    //show the selected calendars
                    $( ".room-schedules-daily-iframe" ).removeClass( "visible" );
                    $( "option:selected", this ).each(function(){
                        var target_iframe = $( this ).val();
                        $( "#" + target_iframe ).addClass( "visible" );
                    });
                    
                    //resize iframes based on the number visible
                    var room_count = $( "option:selected", this ).length;
                    $( ".room-schedules-daily-iframe" ).css( "width", "0" );
                    $( ".room-schedules-daily-iframe:visible" ).css( "width", Math.floor( 100 / room_count - 1 ) + "%" );
                });
            }
        },
        
        
        //Activate the Chosen javascript plugin to prettify the select box on the daily view page
        activate_daily_view_chosen: function(){
            $( ".room-schedules-daily-select-rooms" ).chosen();
        }
        
    };
    
    
    return room_schedules;
}());

