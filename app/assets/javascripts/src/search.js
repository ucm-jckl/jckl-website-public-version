JCKL.SEARCH = (function(){
        
    var search = {
        
        summon_base_url: "http://ucmo.summon.serialssolutions.com/search",
        catalog_base_url: "https://quest.searchmobius.org/search/",
        journals_base_url: "http://WD8CD4TK5M.search.serialssolutions.com/",
        website_search_base_url: "https://www.google.com/",
        
        init: function(){
            this.give_search_box_focus();
            this.create_search_box_dropdown();
            this.toggle_search_options();

            this.do_search();
                        
            return true;
        },
        
        
        give_search_box_focus: function(){
            //Add a special CSS class to the search box form when it has focus.
            //Also gives the search box focus on page load.
            
            //$(".search-text-field").focus(); //don't start with search box focused because it causes accessibility issues for screen readers
            $(".search-wrapper").addClass("search-has-focus");

            $(".search-wrapper").focusin(function(){
                $(this).addClass("search-has-focus");
            });

            $(".search-wrapper").focusout(function(){
                $(this).removeClass("search-has-focus");
            });
        },
        
         //Create a Bootstrap dropdown menu on the main search bar and change the contents of the button based on the clicked dropdown item.
        create_search_box_dropdown: function(){
            var this_class = this;
            
            //set the click event
            $( ".search-type-list li" ).click(function(){
                //First check if it is a plain link. Those should just behave like normal links. 
                if( $("a", this).length == 0 ){
                    this_class.select_search_box_dropdown_item( $( this ) );
                }
            });

            //populate initial values based on the first item in the list
            this.select_search_box_dropdown_item( $(".search-type-list li:first-child") );
        },
        
        
        //make the contents and value of the button equal to those of the dropdown item clicked
        select_search_box_dropdown_item: function(selectedItem){
            var clickedValue = selectedItem.attr("data-value");
            var clickedText = selectedItem.text();
            selectedItem.parents(".search-type-list").siblings(".search-type-button").val(clickedValue).text(clickedText);

            //make the search field placeholder text equal to that specified on the dropdown item
            var clickedPlaceholder = selectedItem.attr("data-placeholder");
            selectedItem.parents(".search-wrapper").children(".search-text-field").attr("placeholder", clickedPlaceholder);
        },
        

        //Create an onclick event to show/hide search options on small screens.
        toggle_search_options: function(){
            $(".search-options h3").click(function(){
                $(this).toggleClass("open");
                $(".search-options .menu-block-wrapper").slideToggle();
            });
        },
                
        
        //Execute a search and open up a results page
        do_search: function(){
            var this_class = this;
            
            $( ".search-wrapper" ).submit( function( e ){

                var search_terms = $( ".search-text-field" ).val().replace( " ", "+" );
                var search_type = $( ".search-type-button" ).val();
                var search_params = {};
                var url = "";

                switch( search_type ){
                    case "central-search":
                        url = this_class.summon_base_url;
                        search_params["s.q"] = search_terms;
                        break;
                    case "catalog":
                        url = this_class.catalog_base_url;
                        search_params.searchtype = "X";
                        search_params.SORT = "D";
                        search_params.searcharg = search_terms;
                        search_params.searchscope = "2";
                        break;
                    case "articles":
                        url = this_class.summon_base_url;
                        search_params["s.q"] = search_terms;
                        search_params["s.cmd"] = "addFacetValueFilters(IsScholarly,true)";
                        search_params["s.light"] = "t";
                        break;
                    case "journals":
                        url = this_class.journals_base_url;
                        search_params.tab = "JOURNALS";
                        search_params.N = "100";
                        search_params.V = "1.0";
                        search_params.L = "WD8CD4TK5M";
                        search_params.S = "AC_T_B";
                        search_params.C = search_terms
                        break;
                    case "website":
                        url = this_class.website_search_base_url;
                        //search_params.gws_rd = "ssl";
                        search_params.q = "site:library.ucmo.edu+" + search_terms;

                        break;
                }

                //Update the form and add data dynamically before the form data is sent
                $( ".dynamic-form-data", this ).remove();
                $( this ).attr( "action", url );
                for( var param in search_params ){
                    var input = $( document.createElement( "input" ) ).attr( "type", "hidden" ).attr( "name", param ).val( search_params[param] ).addClass( "dynamic-form-data" );
                    input.appendTo( $( this ) );
                }
                
                var whole_url = url + "?" + $( this ).serialize();
                //For Google website searches, replace the query string ? with #
                if( search_type == "website" ){
                    whole_url = whole_url.replace( "?", "#" );
                }

                //Use window.open instead of default form submit action to avoid insecure form warnings
                e.preventDefault();
                window.location.href = decodeURIComponent( whole_url );
                
            });
            
            //Fix for iOS form not submitting when Enter key is pressed
            var is_ios = navigator.userAgent.toLowerCase().match(/(iphone|ipod|ipad)/);
            if( is_ios ){
                $( ".search-wrapper .search-text-field" ).keypress( function(e){
                    //if Enter key was pressed, trigger a Click on the submit button
                    if( e.which == 13 ){
                        $( ".search-wrapper .search-submit-button" ).trigger( "click" );
                    }
                });
            }
        },

    };
    
    
    
    return search;
}());

