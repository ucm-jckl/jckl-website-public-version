JCKL.CLF = (function(){
        
    var clf = {
        
        minimum_quantity: 10,
        
        init: function(){
                        
            if( $( ".clf-book-summary" ).length > 0 ){
                this.show_summary();
            }
            
            if( $( ".clf-book-image" ).length > 0 ){
                this.lazy_load_images();
            }
            
            if( $( ".clf-book-add-to-cart" ).length > 0 ){
                this.add_to_order();
                this.remove_from_order();
            }
            
            return true;
        },
        
        
        
        //Expand/contract book summaries
        show_summary: function(){
            $( ".clf-book-summary" ).more({
                length: 60,
                wordBreak: true,
                leeway: 20
            });
            $( ".more-link" ).addClass( "btn btn-default btn-sm" );
        },
        
        
        //Load images as the user scrolls to them
        lazy_load_images: function(){
            //skip the first 5 books and set up lazyloading for the rest
            $( ".clf-book-image" ).slice( 5 ).find( ".lazy-load" ).show().lazyload({ 
                threshold: 100,
                effect: "fadeIn"
            });
                
            //load the first 5 on each page/column like normal
            $( ".clf-book-image img.lazy-load" ).slice(0, 4).each( function(){
                var image_url = $( this ).attr( "data-original" );
                $( this ).attr( "src", image_url ).show();
            });            
            
        },
        
        
        //Add books to an order 
        add_to_order: function(){
            var this_class = this;
            var form = $( ".clf-book-add-to-cart form" );
                            
            //Make an AJAX POST request when the form is submitted
            form.submit( function( e ){
                e.preventDefault();

                //Check if the quantity is set to a number greater than 0
                var quantity = Number( $( this ).find( ".clf-book-add-to-cart-quantity" ).val() );

                if( quantity < 1 || isNaN( quantity ) ){
                    
                    //Display an error message
                    $( this ).find( ".special-message" ).remove();
                    var error_message = $( document.createElement( "span" ) );
                    error_message.text( "Quantity must be greater than zero." );
                    error_message.addClass( "error-message special-message" );
                    $( this ).find( ".form-actions" ).after( error_message );
                    
                }else{
                    
                    //Hide any messages
                    $( this ).find( ".special-message" ).remove();
                    
                    //Make the POST request
                    var this_form = $( this );
                    $.post( $( this ).attr( "action" ), $( this ).serialize() )
                    .done( function( response_data ){

                        //Display success/error message
                        var alert_type = "success";
                        var alert_message = quantity + " added to cart.";
                        if( response_data.is_new == false ){
                            alert_message = "Quantity changed to " + quantity + ".";
                        }
                        if( response_data.success == false ){
                             alert_type = "error";
                             alert_message = "There was an error adding this to your cart.";
                        }
                        var alert = $( document.createElement( "div" ) );
                        alert.addClass( "special-message " + alert_type + "-message" );
                        alert.text( alert_message );
                        this_form.find( ".form-actions" ).after( alert );
                        
                        //Show/hide the Remove button and toggle the add/update text
                        if( response_data.success ){
                            this_form.find( ".remove-from-cart" ).removeClass( "hidden" );
                            this_form.find( "button[type=submit]" ).text( "Update Cart" );
                        }else{
                            this_form.find( ".remove-from-cart" ).addClass( "hidden" );
                        }
                        
                        if( "cart" in response_data ){
                            //Update the cart display
                            this_class.update_cart_display( response_data.cart );
                            
                            //Check the number of books
                            this_class.check_number_of_books( response_data.cart );
                        }
                    });
                                        
                }
            });
        },
        
        //Remove book from an order
        remove_from_order: function(){
            var this_class = this;
            
            //Make an AJAX POST request when the remove button is clicked
            $( ".clf-book-add-to-cart form .remove-from-cart" ).click( function(){
                var form = $( this ).parents( "form" );
                
                //Hide any messages
                form.find( ".special-message" ).remove();

                //Make the POST request
                var data = form.serialize() + "&remove=true";
                $.post( form.attr( "action" ), data )
                .done( function( response_data ){

                    //Display success/error message
                    var alert_type = "success";
                    var alert_message = "Item removed from cart.";
                    if( response_data.success == false ){
                         alert_type = "error";
                         alert_message = "Error removing from cart.";
                    }
                    var alert = $( document.createElement( "div" ) );
                    alert.addClass( "special-message " + alert_type + "-message" );
                    alert.text( alert_message );
                    form.find( ".form-actions" ).after( alert );
                    
                    //Update the value of the Quantity field and hide the Remove button
                    if( response_data.success = true ){
                        form.find( ".clf-book-add-to-cart-quantity" ).val( "" );
                        form.find( ".remove-from-cart" ).addClass( "hidden" );
                        form.find( "button[type=submit]" ).text( "Add to Cart" );
                    }
                    
                    if( "cart" in response_data ){
                        //Update the cart display
                        this_class.update_cart_display( response_data.cart );
                        
                        //Check the number of books
                        this_class.check_number_of_books( response_data.cart );
                    }
                    
                });
            });
        },
        
        //Update the shopping cart displayed on the page
        update_cart_display: function( cart ){
            $( ".clf-shopping-cart .clf-book" ).addClass( "hidden" );
            
            for( var i = 0; i < cart.items.length; i++ ){
                var item = cart.items[i];
                var display = $( "#book-" + item.id );
                display.find( ".clf-book-quantity" ).empty().append( "Quantity: " + item.quantity );
                display.find( ".clf-book-total" ).empty().append( "Total: $" + ( item.quantity * item.price ).toFixed( 2 ) );
                if( item.quantity > 0 ){
                    display.removeClass( "hidden" );
                }
            }
            
            $( ".clf-shopping-cart .clf-cart-number-of-books" ).empty().append( cart.book_count );
            $( ".clf-shopping-cart .clf-cart-subtotal" ).empty().append( "$" + cart.subtotal.toFixed( 2 ) );
            $( ".clf-shopping-cart .clf-cart-tax" ).empty().append( "$" + cart.tax.toFixed( 2 ) );
            $( ".clf-shopping-cart .clf-cart-total" ).empty().append( "$" + cart.total.toFixed( 2 ) );
            
        },
        
        
        //Check that the number of books meets the minimum quantity and show/hide buttons and messages accordingly
        check_number_of_books: function( cart ){
            if( cart.book_count < this.minimum_quantity ){
                $( ".clf-minimum-books-alert" ).show();
                $( ".clf-check-out" ).addClass( "hidden" );
            }else{
                $( ".clf-minimum-books-alert" ).hide();
                $( ".clf-check-out" ).removeClass( "hidden" );
            }
            
        }
    };
    
    return clf;
}());

