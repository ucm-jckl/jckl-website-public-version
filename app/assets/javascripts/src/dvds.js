JCKL.DVDS = (function(){
        
    var dvds = { 
        
        init: function(){
            if( $( ".dvd-browse" ).length > 0 ){
                this.add_pagination();
                this.show_summary();
                this.lazy_load_images();
            }
            
            return true;
        },
        
        //add pagination using the twbsPagination plugin and set page-changing functionality
        add_pagination: function(){
            
            //show the start page if one is specified or else show page one
            var start_page = Number( $( ".dvd-pagination" ).attr( "data-start-page" ) );
            if( $( ".dvd-page" ).length == 1 ){
                $( ".dvd-page" ).show();
            }else if( start_page != 0 ){
                $( ".dvd-page-" + start_page ).show().siblings( ".dvd-page" ).hide();
            }
            
            //add pagination and state-changing
            var page_count = Number( $( ".dvd-pagination" ).first().attr( "data-page-count" ) );
            $( ".dvd-pagination" ).twbsPagination({
                totalPages: page_count,
                visiblePages: 5,
                startPage: start_page,
                onPageClick: function( event, page ){
                    $( ".dvd-page-" + page ).show().siblings( ".dvd-page" ).hide();
                    
                    var uri = new URI( window.location );
                    uri.setSearch( "page", page );
                    History.pushState( null, null, uri.search() );
                }
            });
            
            $( "#dvd-pagination-bottom" ).click(function(){
               document.getElementById( "dvd-pagination-top" ).scrollIntoView(); 
            });
        },

        
        //Expand/contract dvd summaries
        show_summary: function(){
            $( ".dvd-summary" ).more({
                length: 300,
                wordBreak: true,
                leeway: 20
            });
        },
        
        
        //Load images as the user scrolls to them
        lazy_load_images: function(){
            
            $( ".dvd-page" ).each( function(){ 
                //skip the first 5 books on each page/column set up lazyloading
                $( ".dvd-image", this ).slice( 5 ).find( ".lazy-load" ).show().lazyload({ 
                    threshold: 100,
                    effect: "fadeIn"
                });
                
                //load the first 5 on each page like normal
                $( ".dvd-image .lazy-load", this ).each( function(){
                    var image_url = $( this ).attr( "data-original" );
                    $( this ).attr( "src", image_url ).show(); 
                });
            });
            
            
        }
        
        
    };
    
    
    return dvds;
}());

