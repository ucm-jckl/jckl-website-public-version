class AbsencesController < ApplicationController
  def by_date
    date = Chronic.parse(params[:date]).andand.to_date
    date ||= Time.zone.now.to_date
    @absences = Absence.includes(:employee).where('? >= start_date and ? <= end_date', date, date)

    @page_title = "Library Absences: #{date}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", @page_title]
    render
  end

  def index
    @employee = Employee.includes(:supervisor).find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absences = Absence.includes(:absence_type, :employee).where(employee_id: @employee.id).where('created_at >= ?', Time.zone.now - 2.years).order(updated_at: :desc)
    @page_title = "Absence Requests: #{@employee.first_last}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", @page_title]
    render
  end

  def show
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:submitter, :employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @page_title = "Absence Request: #{@absence.employee.first_last} - #{@absence.date_range}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
    render
  end

  def new
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.new
    @absence.employee = @employee

    @submit_for = @employee.subordinates.order(:last_name, :first_name)
    if current_user.employee.library_administrator?
      @submit_for = Employee.all.order(:last_name, :first_name)
    end

    @page_title = 'Submit an Absence Request'
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
    render
  end

  def create
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.new(params.require(:absence).permit!)
    @absence.employee = @employee
    @absence.submitter = current_user.employee

    if @absence.save
      AbsencesMailer.send_email(@absence, 'Submitted').deliver_later
      flash[:notice] = 'Your absence request was submitted. You should receive confirmation via email.'
      return redirect_to employee_absences_url(employee_name: @absence.employee.url_slug)
    else
      @page_title = 'Submit an Absence Request'
      @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
      render :new
    end
  end

  def edit
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @page_title = "Edit Absence Request: #{@absence.employee.first_last} - #{@absence.date_range}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
    render
  end

  def update
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    if @absence.update_attributes(params.require(:absence).permit!)
      AbsencesMailer.send_email(@absence, 'Amended').deliver_later
      flash[:notice] = 'Your absence request was amended. You should receive confirmation via email.'
      return redirect_to employee_absences_url(employee_name: @absence.employee.url_slug)
    else
      @page_title = "Edit Absence Request: #{@absence.employee.first_last} - #{@absence.date_range}"
      @breadcrumb = ["<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
      render
    end
  end

  def destroy
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @absence.destroy
    if @absence.destroyed?
      clone = @absence.dup
      clone.created_at = @absence.created_at
      clone.updated_at = @absence.updated_at
      AbsencesMailer.send_email(clone, 'Deleted').deliver_now
      flash[:notice] = 'This absence request was deleted. You should receive confirmation via email.'
      redirect_to employee_absences_url(employee_name: @absence.employee.url_slug)
    else
      flash[:alert] = 'There was an error deleting this absence request.'
      redirect_to request.original_url
    end
  end

  def approve
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @absence.approved = true
    @absence.background_information = '[redacted]'
    if @absence.save
      flash[:notice] = 'This absence request was approved. You should receive confirmation via email.'
      AbsencesMailer.send_email(@absence, 'Approved').deliver_later
    else
      flash[:alert] = 'There was an error approving this absence request.'
    end
    redirect_to employee_absence_url(employee_name: @absence.employee.url_slug, id: @absence.id)
  end

  def reject
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @absence.approved = false
    @absence.background_information = '[redacted]'
    if @absence.save
      flash[:notice] = 'This absence request was rejected. You should receive confirmation via email.'
      AbsencesMailer.send_email(@absence, 'Rejected').deliver_later
    else
      flash[:alert] = 'There was an error rejecting this absence request.'
    end
    redirect_to employee_absence_url(employee_name: @absence.employee.url_slug, id: @absence.id)
  end

  def receive_note
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @absence.note_received = true
    if @absence.save
      flash[:notice] = 'This absence request received a doctor\'s note. You should receive confirmation via email.'
      AbsencesMailer.send_email(@absence, ' - Doctor\'s Note Received').deliver_later
    else
      flash[:alert] = 'There was an error receiving a doctor\'s note this absence request.'
    end
    redirect_to employee_absence_url(employee_name: @absence.employee.url_slug, id: @absence.id)
  end

  private

  def check_if_employee
    authenticate_user!
    access_denied unless current_user.andand.employee?
  end
  before_action :check_if_employee
end
