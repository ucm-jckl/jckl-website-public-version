class NewsItemsController < ApplicationController
  def index
    # determine whether to show all stories or just the unexpired ones
    @show_all = params.key?(:showall)

    # get stories
    @news_items = NewsItem.all.order(updated_at: :desc)
    unless @showall
      @news_items = @news_items.where('NOW() < expiration_date OR expiration_date is null')
    end

    @page_title = 'Library News'
    render
  end

  def show
    @news_item = NewsItem.find_by(friendly_url: params[:date])

    return not_found unless @news_item.present?

    @page_title = @news_item.title
    @breadcrumb = ["<a href='#{news_items_url}'>News</a>", @page_title]
    render
  end
end
