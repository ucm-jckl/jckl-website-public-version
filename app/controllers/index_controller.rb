class IndexController < ApplicationController
  def index
    @page_title = 'Home'
    set_layout_option(:fixed_width_container, false)
    set_layout_option(:include_search, true)
    set_layout_option(:include_search_options, true)
    set_layout_option(:include_breadcrumb, false)
    render
  end

  def sitemap
    @page_title = 'Sitemap'
    respond_to do |format|
      format.html { render }
      format.xml { render layout: false }
    end
  end
end
