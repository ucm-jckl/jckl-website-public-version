class BarcodeFilesController < ApplicationController
  def new
    return access_denied unless current_user.present?
    @barcode_file = BarcodeFile.new
    @page_title = 'Upload Barcode File'
    @breadcrumb = ["<a href='#{barcode_files_url}'>Inventory</a>", @page_title]
  end

  def create
    return access_denied unless current_user.present?

    @barcode_file = BarcodeFile.new
    @barcode_file.user_id = current_user.id
    @barcode_file.notes = params[:notes]
    @barcode_file.barcodes = params[:barcodes].andand.read.andand.split("\r\n").andand.join(',')

    similar_files = BarcodeFile.where(barcodes: @barcode_file.barcodes).where('updated_at > ?', Time.now - 1.day)

    if similar_files.present?
      flash[:alert] = "The file #{@barcode_file.to_array[0]} through #{@barcode_file.to_array[@barcode_file.to_array.length - 1]} has already been uploaded today."
      @page_title = 'Upload Barcode File'
      @breadcrumb = ["<a href='#{barcode_files_url}'>Inventory</a>", @page_title]
      render :new
    elsif @barcode_file.save
      flash[:notice] = "#{@barcode_file.to_array.length} barcodes uploaded successfully."
      return redirect_to new_barcode_file_url
    else
      @page_title = 'Upload Barcode File'
      @breadcrumb = ["<a href='#{barcode_files_url}'>Inventory</a>", @page_title]
      render :new
    end
  end

  def index
    return access_denied unless current_user.employee?

    @barcode_files = BarcodeFile.where('created_at >= ?', Time.now - 1.year).order(updated_at: :desc)
    @page_title = 'Inventory'
  end
end
