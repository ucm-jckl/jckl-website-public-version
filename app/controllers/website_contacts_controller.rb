class WebsiteContactsController < ApplicationController
  CONFIRMATION_MESSAGE = 'Your message was submitted. You should receive confirmation via email.'.freeze

  def general
    @employees = Employee.where('hide_from_public_views = false or hide_from_public_views is null').order(:last_name, :first_name)
    @employee = nil
    @offices = Office.all.order(:display_name)
    @office = nil
    @contact_form = WebsiteContact.new

    @page_title = 'Contact Us'
    render
  end

  def employee
    @employee = Employee.find_by_url_slug(params[:name])
    @office = nil
    return not_found unless @employee.present?
    @contact_form = WebsiteContact.new

    @page_title = "Contact #{@employee.first_last}"
    render
  end

  def office
    @employee = nil
    @office = Office.find_by(office_name: params[:name])
    return not_found unless @office.present?
    @contact_form = WebsiteContact.new

    @page_title = "Contact the #{@office.display_name}"
    render
  end

  def create
    # redirect if the honeypot "content" field was filled by a spambot
    return redirect_to root_path unless params[:content].blank?

    employee = Employee.find_by(id: params[:employee])
    office = Office.find_by(id: params[:office])

    @contact_form = WebsiteContact.new(params.require(:website_contact).permit(:contact_name, :contact_email, :topic, :subject, :message))
    if @contact_form.save
      flash[:notice] = CONFIRMATION_MESSAGE
      WebsiteContactsMailer.send_email(@contact_form, employee, office).deliver_later
      if employee.present?
        return redirect_to employee_website_contacts_url
      elsif office.present?
        return redirect_to office_website_contacts_url
      else
        return redirect_to general_website_contacts_url
      end
    else
      @page_title = "Contact the #{@office.display_name}"
      render
    end
  end

  def info
    @page_title = 'Contact Information'
    render
  end
end
