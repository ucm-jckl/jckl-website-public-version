class MeetingRoomsController < ApplicationController
  CONFIRMATION_MESSAGE = 'Your booking request was submitted. You should receive confirmation via email.'.freeze

  def index
    @rooms = MeetingRoom.all.order(:library_only, :floor, :room_name)
    @page_title = 'Room Booking Schedules'
    render
  end

  def available_rooms
    start_time = Chronic.parse(params[:start])
    end_time = Chronic.parse(params[:end])

    if start_time.present? && end_time.present? && start_time < end_time
      render json: MeetingRoom.check_availability(start_time, end_time)
    else
      render json: { errors: ['A start time and end time must be specified and in the correct order.'] }, status: 422
    end
  end

  def new
    @booking = MeetingRoomBooking.new
    @booking.meeting_room = MeetingRoom.find_by(room_name: params[:room_name])

    if @booking.meeting_room.andand.library_only && !current_user.andand.employee?
      if !user_signed_in?
        flash[:alert] = "Notice: Only library employees can book room #{@booking.meeting_room.display_name}. If you are a library employee, please <a href='#{new_user_session_url}'>log in</a>."
        return redirect_to meeting_rooms_url
      else
        flash[:alert] = "Notice: Only library employees can book room #{@booking.meeting_room.display_name}."
        return redirect_to meeting_rooms_url
      end
    end

    @page_title = "Book #{@booking.meeting_room.display_name}"
    @breadcrumb = ["<a href='#{meeting_rooms_url}'>Room Booking Schedules</a>", @page_title]
    render
  end

  def create
    @booking = MeetingRoomBooking.new(params.require(:meeting_room_booking).permit!)
    @booking.meeting_room = MeetingRoom.find_by(room_name: params[:room_name])
    @booking.requestor_name = current_user.andand.name || @booking.contact_name
    @booking.requestor_email = current_user.andand.email || @booking.contact_email

    if @booking.meeting_room.andand.library_only && !current_user.andand.employee?
      if !user_signed_in?
        flash[:alert] = "Notice: Only library employees can book room #{@booking.meeting_room.display_name}. If you are a library employee, please <a href='#{new_user_session_url}'>log in</a>."
        return redirect_to meeting_rooms_url
      else
        flash[:alert] = "Notice: Only library employees can book room #{@booking.meeting_room.display_name}."
        return redirect_to meeting_rooms_url
      end
    end

    if @booking.save
      flash[:notice] = CONFIRMATION_MESSAGE
      MeetingRoomsMailer.send_email(@booking).deliver_later
      redirect_to meeting_rooms_url
    else
      @page_title = "Book #{@booking.meeting_room.display_name}"
      @breadcrumb = ["<a href='#{meeting_rooms_url}'>Room Booking Schedules</a>", @page_title]
      render :new
    end
  end
end
