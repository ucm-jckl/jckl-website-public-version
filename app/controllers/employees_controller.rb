class EmployeesController < ApplicationController
  # List all employees
  def index
    @current_department = Department.find_by(department_name: params[:list])
    @current_department ||= Department.new
    @offices = Office.joins(:department).order('display_name asc')
    @employees = Employee.joins(:department).where('hide_from_public_views is null or hide_from_public_views = false')
                         .order('last_name asc, first_name asc')

    if params[:list].present?
      @offices = @offices.where(department_id: @current_department.id)
      @employees = @employees.where(department_id: @current_department.id)
    end

    @breadcrumb = ["<a href='#{employees_url}'>Employee Directory</a>"]
    @page_title = 'Employee Directory'
    if params[:list].present?
      @breadcrumb << @current_department.display_name
      @page_title = "#{@page_title}: #{@current_department.display_name}"
    end
    render
  end

  # List all subject specialists
  def subject_specialists
    @departments = Department.all.order(:display_name)
    @subjects = SubjectArea.includes(:subject_librarian).where.not(subject_librarian_id: nil).order('subject_name asc')

    @breadcrumb = ["<a href='#{employees_url}'>Employee Directory</a>", 'Subject Specialists']
    @page_title = 'Employee Directory: Subject Specialists'

    render
  end

  # Show the employee portal with links to various employee functions
  def portal
    authenticate_user!
    return access_denied unless current_user.andand.employee?

    @employee = current_user.employee
    @subordinates = @employee.subordinates.order(:last_name)
    if @employee.library_administrator?
      @subordinates = LibraryEmployee.all.order(:last_name)
    end

    @page_title = 'Employee Portal'
    render
  end

  # Edit a specific employee's info
  def edit
    @employee = Employee.find_by_url_slug(params[:name])
    return not_found unless @employee.present?

    authenticate_user!
    return access_denied unless current_user.employee == @employee

    @page_title = "Edit Information: #{@employee.first_last}"
  end

  def update
    @employee = Employee.find_by_url_slug(params[:name])
    return not_found unless @employee.present?

    authenticate_user!
    return access_denied unless current_user.employee == @employee

    if @employee.update(params.require(:employee).permit!)
      flash[:notice] = 'Your information was updated.'
      return redirect_to portal_employees_url
    else
      @page_title = "Edit Information: #{@employee.first_last}"
      render 'edit'
    end
  end

  # Show a page of common links for student workers
  def student_workers
    @page_title = 'Student Worker Links'
    render
  end
end
