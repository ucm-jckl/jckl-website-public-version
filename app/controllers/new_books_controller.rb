class NewBooksController < ApplicationController
  def index
    @active_subject = SubjectArea.find_by(subject_name: params[:subject])
    @active_call_number_range = CallNumberRange.find_by(id: params[:range])
    @current_page = params[:page].to_i - 1 || 0
    @books_per_page = params[:per_page] || 25
    @timeframe = params[:timeframe]

    @subjects = SubjectArea.subjects_with_call_number_ranges.order(:display_name)
    @call_number_ranges = CallNumberRange.all.order(:range_start, :range_end)

    if @active_subject.present?
      @books = NewBook.within_subject_area(@active_subject.id)
    elsif @active_call_number_range.present?
      @books = NewBook.within_call_number_range(@active_call_number_range.id)
    else
      @books = NewBook.all
    end

    if @timeframe.present?
      @books = @books.where('cdate >= ? and cdate <= ?', Chronic.parse(@timeframe).andand.beginning_of_month, Chronic.parse(@timeframe).andand.end_of_month)
    end

    @total_pages = (@books.length / @books_per_page.to_i).ceil
    @current_page = @total_pages - 1 if @current_page > @total_pages - 1
    @current_page = 0 if @current_page < 0
    pagination_padding = 4
    @pages_to_show = if @current_page < pagination_padding
                       [*0..(pagination_padding * 2)]
                     elsif @current_page > @total_pages - (pagination_padding + 1)
                       [*@total_pages - ((pagination_padding * 2) + 2)..@total_pages - 1]
                     else
                       [*@current_page - pagination_padding..@current_page + pagination_padding]
                     end

    @paginated_books = @books.order(cdate: :desc)
                             .in_groups_of(@books_per_page)[@current_page]
                             .andand.reject(&:blank?)

    @page_title = 'New Arrivals'
    @page_title += ": #{@active_subject.display_name}" if @active_subject.present?
    @page_title += ": #{@active_call_number_range.range_summary}" if @active_call_number_range.present?
    render
  end

  def mappings
    @subjects = SubjectArea.subjects_with_call_number_ranges.order(:display_name)
    @page_title = 'Call Number Subject Mappings'
    @breadcrumb = ["<a href='#{new_books_url}'>New Arrivals</a>", @page_title]
    render
  end
end
