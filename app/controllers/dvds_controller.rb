class DvdsController < ApplicationController
  def index
    @genre = DvdGenre.find_by(display_name: params[:genre])
    @title = params[:title]
    @sort = params[:sort].andand.capitalize

    @current_page = params[:page].to_i - 1
    @current_page = 0 if @current_page < 0
    @dvds_per_page = params[:per_page].andand.to_i || 25

    if @genre.present?
      @dvds = Dvd.find_by_genre(@genre.id)
    else
      @dvds = Dvd.all
    end

    order = case @sort
               when 'Title'
                 'title_sort asc'
               when 'Newest by Release Date'
                 'copyright_date desc'
               when 'Newest by Date Acquired by JCKL'
                 'cdate desc'
               else
                 'copyright_date desc'
               end

    @dvds = @dvds.order(order).includes(:location_code)
    if @title.present?
     @dvds = @dvds.where('title_sort like ?', '%' + @title + '%')
    end

    @total_pages = (@dvds.length / @dvds_per_page.to_i).ceil
    @current_page = @total_pages - 1 if @current_page > @total_pages - 1
    @current_page = 0 if @current_page < 0
    pagination_padding = 4
    @pages_to_show = if @current_page < pagination_padding
                       [*0..(pagination_padding * 2)]
                     elsif @current_page > @total_pages - (pagination_padding + 1)
                       [*@total_pages - ((pagination_padding * 2) + 2)..@total_pages - 1]
                     else
                       [*@current_page - pagination_padding..@current_page + pagination_padding]
                     end

    @paginated_dvds = @dvds.in_groups_of(@dvds_per_page)[@current_page].andand.reject(&:blank?)

    @page_title = 'Browse DVDs'
    render
  end
end
