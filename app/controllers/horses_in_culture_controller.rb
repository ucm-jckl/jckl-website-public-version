class HorsesInCultureController < ApplicationController
  def data
    @titles = HorsesInCultureTitle.all.order(:author, :title)

    respond_to do |format|
      format.json { render json: { data: @titles } }
    end
  end
end
