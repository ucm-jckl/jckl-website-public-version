class ErrorsController < ApplicationController
  def not_found
    @page_title = 'Page Not Found'
    render(status: 404)
  end

  def unprocessable
    @page_title = 'Could Not Process Request'
    render(status: 422)
  end

  def internal_server_error
    @page_title = 'Internal Server Error'
    render(status: 500)
  end

  def unauthorized
    @page_title = 'Unauthorized'
    render(status: 401)
  end
end
