class LibraryHoursController < ApplicationController
  def index
    # determine whether to show all hours or just the first 4 weeks
    @show_all = params.key?(:showall)

    # get the hours
    start_date = Time.zone.now.beginning_of_week(:sunday).to_date
    end_date = Time.zone.now.end_of_week.to_date + 3.weeks
    end_date = start_date + 1.year if @show_all

    @weeks = LibraryHour.where('calendar_date >= ? and calendar_date <= ?', start_date, end_date)
                        .order(:calendar_date)
                        .in_groups_of(7)

    @page_title = 'Library Hours'
    render
  end
end
