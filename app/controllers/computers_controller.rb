class ComputersController < ApplicationController
  def index
    @computers = Computer.includes(:computer_sessions).where(active: true).order(:floor, :computer_name)
    @page_title = 'JCKL Computers'
    render
  end

  def ping
    require 'resolv'
    ip = params[:ip] || request.remote_ip
    hostname = Resolv.getname(ip)

    return render plain: 'Invalid address', status: 404 unless hostname.present?

    computer = Computer.find_by(computer_name: hostname)
    return render plain: 'Computer not found', status: 404 unless computer.present?

    session = computer.current_session
    session.ping_count += 1
    if session.save
      render plain: 'Session updated', status: 200
    else
      render plain: 'Error updating session', status: 500
    end
  end

  def availability
    render json: Computer.where(active: true).as_json(methods: :available?)
  end
end
