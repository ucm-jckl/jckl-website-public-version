class NewBookRequestsController < ApplicationController
  CONFIRMATION_MESSAGE = 'Your purchase request was submitted. You should receive confirmation via email.'.freeze

  def new
    return access_denied unless user_signed_in?

    @request = NewBookRequest.new
    @request.requestor_name = current_user.name
    @request.requestor_email = current_user.email
    @request.copies_ordered = 1
    @request.format = 'Book'
    if current_user.employee?
      @request.recommender = current_user.employee.first_last
      @request.approved_by = current_user.employee.first_last
    end

    @page_title = 'Purchase Request Form'
    render
  end

  def create
    return access_denied unless user_signed_in?

    @request = NewBookRequest.new(params.require(:new_book_request).permit!)
    @request.submitted_by = current_user.name

    if @request.save
      flash[:notice] = CONFIRMATION_MESSAGE
      NewBookRequestsMailer.send_email(current_user, @request).deliver_later
      redirect_to new_new_book_request_url
    else
      @page_title = 'Purchase Request Form'
      render :new
    end
  end
end
