class MapController < ApplicationController
  def index
    @initial_floor = (params[:floor] || 1).to_i
    @highlight = params[:highlight]
    @computers = Computer.where(active: true)
    @page_title = 'Library Map'
    render
  end
end
