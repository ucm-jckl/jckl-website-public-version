class SessionsController < Devise::SessionsController
  def new
    @page_title = 'Log In'
    super
  end
end
