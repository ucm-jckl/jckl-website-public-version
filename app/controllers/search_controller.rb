class SearchController < ApplicationController
  def index
    @my_variable = 'Hello World'

    @service = Summon::Service.new(access_id: Rails.application.secrets.summon_access_id, secret_key: Rails.application.secrets.summon_api_key)
    @search = @service.search('s.q' => params[:q], 's.fvf' => 'ContentType,Book')
    @article_search = @service.search('s.q' => params[:q], 's.fvf' => 'ContentType,Article')
    #  @journals_search = @service.search('s.q' => params[:q], 's.fvf' => 'ContentType, Journals')

    @journals_search = []
    url = 'http://wd8cd4tk5m.search.serialssolutions.com/?tab=JOURNALS&N=100&V=1.0&L=WD8CD4TK5M&S=AC_T_B&C=mysearchterm'
    journal_page = Nokogiri::HTML(open(url))
    journal_page.css('.SS_Holding').each do |result|
      @journals_search << {
        'title' => result.css('.result__title .result__a').first.andand.text,
        'url' => result.css('h2 > a').first.andand.get_attribute('href')
      }

      @duckduckgo_results = []
      url = 'https://duckduckgo.com/html/?q=site:library.ucmo.edu+mysearchterms'
      duckduckgo_page = Nokogiri::HTML(open(url))
      duckduckgo_page.css('.result').each do |result|
        @duckduckgo_results << {
          'title' => result.css('.result__title .result__a').first.andand.text,
          'url' => result.css('h2 > a').first.andand.get_attribute('href')
        }
      end

      render
    end
end
end
