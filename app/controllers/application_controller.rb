class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # Return whether the current url is the login page
  def login_page?
    request.fullpath.start_with?('/admin/login', '/admin/auth', '/users')
  end

  protected

  # Make "request" object a helper method so it can be called from views
  helper_method :request

  # Make "cookies" object a helper method so it can be called from views
  helper_method :cookies

  # get/set page title
  def page_title
    @page_title ||= action_name
  end
  helper_method :page_title
  attr_writer :page_title

  # get/set breadcrumb
  def breadcrumb
    @breadcrumb ||= [@page_title]
  end
  helper_method :breadcrumb
  attr_writer :page_title

  # get/set layout options hash
  def layout_options
    @layout_options ||= {
      include_sidebar: false,
      include_special_messages: true,
      include_header: true,
      include_footer: true,
      include_search: false,
      include_search_options: false,
      include_inner_container: true,
      include_breadcrumb: true,
      include_google_analytics: true,
      include_crazy_egg: true,
      include_browser_update: true
    }
  end
  helper_method :layout_options

  def set_layout_option(option, value)
    layout_options[option] = value
  end

  def authenticate_active_admin_user!
    authenticate_user!
    unless current_user.has_role?(:administrator)
      flash[:alert] = 'You are not authorized to access this resource.'
      redirect_to root_path
    end
  end

  # Redirect to a given page with a message that a specified object does not exist.
  def not_found
    raise ActionController::RoutingError, 'Not Found'
  end

  # Redirect to a given location or to the root page because the user does not have sufficient access
  def access_denied(url = nil)
    if !user_signed_in?
      return redirect_to new_user_session_path
    else
      if url
        return redirect_to url, alert: 'You are not authorized to view the requested page or perform the requested action.'
      else
        return redirect_to error_unauthorized_path
      end
    end
  end

  before_action :store_location
  def store_location
    session[:before_login_url] = request.original_url unless login_page?
  end

  # Redirect user to previous URL after sign in, or Employee Portal by default
  def after_sign_in_path_for(_resource)
    # set default url to employee portal for employees, root for everyone else
    default_url = root_url

    default_url = portal_employees_url if current_user.employee?

    if session[:before_login_url] == root_url
      default_url
    else
      session[:before_login_url] || default_url
    end
  end

  # Assign users to group A or B for testing, and assign an IP group
  before_action :set_testing_cookie
  def set_testing_cookie
    if cookies[:testing_group].blank?
      # Generate a random number 0-99 to determine which a/b group to assign
      random = rand(100)
      percentage_group_b = 90 # change this to change the %
      testing_group = if random < percentage_group_b
                        'a'
                      else
                        'b'
                      end

      cookies[:testing_group] = {
        value: testing_group,
        expires: Time.now.in_time_zone + 1.month
      }
    end

    if cookies[:ip_group].blank?
      # Determine the user's IP group
      ip = request.env['REMOTE_ADDR']
      ip_group = 'OFFCAMPUS'
      if ip.start_with?('153.91.64') || ip.start_with?('153.91.65') \
        || ip.start_with?('153.91.66') || ip.start_with?('153.91.67')

        ip_group = 'JCKL'

      elsif ip.start_with?('153.91.28') || ip.start_with?('153.91.29')

        ip_group = 'SUMMIT'

      elsif ip.start_with? '153.91.'

        ip_group = 'UCM'

      elsif ip == '127.0.0.1'

        ip_group = 'LOCALHOST'

      end

      cookies[:ip_group] = {
        value: ip_group,
        expires: Time.now.in_time_zone + 1.hour
      }
    end
  end
end
