source 'https://rubygems.org'

# Rails core
gem 'rails', '~> 5.0.0'

# Database connections
gem 'mysql2' # local db
gem 'pg' # for Sierra connections

# Asset pipeline
gem 'react-rails'
gem 'sass-rails'
gem 'autoprefixer-rails'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'

# Assets
gem 'jquery-rails'
gem 'font-awesome-rails'
gem 'jquery-slick-rails'
gem 'lazy_high_charts'

# JS/CSS assets from Bower using rails-assets
gem 'rails-assets-bootstrap', '4.0.0.alpha.3', source: 'https://rails-assets.org'
gem 'rails-assets-tether', '>= 1.1.0', source: 'https://rails-assets.org'
gem 'rails-assets-react-slick', source: 'https://rails-assets.org'
gem 'rails-assets-readmore', source: 'https://rails-assets.org'
gem 'rails-assets-snap.svg', source: 'https://rails-assets.org'
gem 'rails-assets-svg-pan-zoom', source: 'https://rails-assets.org'

# Administration and authentication tools
gem 'activeadmin', github: 'activeadmin/activeadmin'
gem 'inherited_resources', github: 'activeadmin/inherited_resources'
gem 'ransack', github: 'activerecord-hackery/ransack'
gem 'draper', '> 3.x'
gem 'formtastic'
gem 'formtastic-bootstrap'
gem 'ckeditor'
gem 'paperclip'
gem 'active_admin-sortable_tree'
gem 'devise', '>= 4.2.0'
gem 'omniauth-google-oauth2'

# Screen scraping
gem 'nokogiri' # screen scraping
gem 'fastimage' # checking image size

# Development tools
group :development, :test do
  gem 'capybara'
  gem 'poltergeist'
  gem 'byebug'
  gem 'timecop'
end

group :development do
  gem 'web-console'
  gem 'guard'
  gem 'guard-livereload'
  gem 'rack-livereload'
  gem 'bullet'
  gem 'active_record_query_trace'
  gem 'rb-fsevent'
  gem 'wdm', '>= 0.1.0' if Gem.win_platform?
  gem 'certified'
  gem 'fixtures_dumper'
end

# Miscellaneous
gem 'chronic' # time functions
gem 'chronic_duration' # time duration functions
gem 'activerecord-import' # bulk insert SQL
gem 'rack-cors' # allow cross-domain requests
gem 'lcsort' # working with Library of Congress numbers
gem 'andand' # nil exception helper
gem 'exception_notification' # email notifications for errors
gem 'json'
gem 'rest-client' # working with remote APIs
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'summon'
gem 'google-api-client'
